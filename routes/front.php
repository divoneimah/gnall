<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('guest')->post('/login_json', ['as'=>'front.login.login_json'                  , 'uses'=>'App\Http\Controllers\Auth\LoginUsersController@login_json']);
Route::group(['middleware' => 'auth:front'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::apiResource('admin_users'                            , 'App\Http\Controllers\Front\AdminUsersController'                 , ['as'=>'front.admin_users'        , 'parameters' => [ 'admin_users'              => 'admin_users']]);
    Route::apiResource('control_points'                         , 'App\Http\Controllers\Front\ControlPointsController'              , ['as'=>'front.control_points'     ,'parameters' => [ 'control_points'           => 'control_points']]);
    Route::apiResource('cost_centers'                           , 'App\Http\Controllers\Front\CostCentersController'                , ['as'=>'front.cost_centers'       ,'parameters' => [ 'cost_centers'             => 'cost_centers']]);
    Route::apiResource('events'                                 , 'App\Http\Controllers\Front\EventsController'                     , ['as'=>'front.events'             ,'parameters' => [ 'events'                   => 'events']]);
    Route::apiResource('event_expense_natures'                  , 'App\Http\Controllers\Front\EventExpenseNaturesController'        , ['as'=>'front.event_expense_natures','parameters' => [ 'event_expense_natures'    => 'event_expense_natures']]);
    Route::apiResource('event_receipts'                         , 'App\Http\Controllers\Front\EventReceiptsController'              , ['as'=>'front.event_receipts'     ,'parameters' => [ 'event_receipts'           => 'event_receipts']]);
    Route::apiResource('expenses'                               , 'App\Http\Controllers\Front\ExpensesController'                   , ['as'=>'front.expenses'           ,'parameters' => [ 'expenses'                 => 'expenses']]);
    Route::apiResource('transfer'                               , 'App\Http\Controllers\Front\TransferController'                   , ['as'=>'front.transfer'           ,'parameters' => [ 'transfer'                 => 'transfer']]);
    Route::apiResource('expense_natures'                        , 'App\Http\Controllers\Front\ExpenseNaturesController'             , ['as'=>'front.expense_natures'    ,'parameters' => [ 'expense_natures'          => 'expense_natures']]);
    Route::apiResource('expense_receipts'                       , 'App\Http\Controllers\Front\ExpenseReceiptsController'            , ['as'=>'front.expense_receipts'   ,'parameters' => [ 'expense_receipts'         => 'expense_receipts']]);
    Route::apiResource('payment_forms'                          , 'App\Http\Controllers\Front\PaymentFormsController'               , ['as'=>'front.payment_forms'      ,'parameters' => [ 'payment_forms'            => 'payment_forms']]);
    Route::apiResource('subsidiaries'                           , 'App\Http\Controllers\Front\SubsidiariesController'               , ['as'=>'front.subsidiaries'       ,'parameters' => [ 'subsidiaries'             => 'subsidiaries']]);
    Route::apiResource('unit_measurements'                      , 'App\Http\Controllers\Front\UnitMeasurementsController'           , ['as'=>'front.unit_measurements'  ,'parameters' => [ 'unit_measurements'        => 'unit_measurements']]);
    Route::apiResource('users'                                  , 'App\Http\Controllers\Front\UsersController'                      , ['as'=>'front.users'              ,'parameters' => [ 'users'                    => 'users']]);
    Route::apiResource('user_cost_centers'                      , 'App\Http\Controllers\Front\UserCostCentersController'            , ['as'=>'front.user_cost_centers'  ,'parameters' => [ 'user_cost_centers'        => 'user_cost_centers']]);
    Route::apiResource('user_payment_forms'                     , 'App\Http\Controllers\Front\UserPaymentFormsController'           , ['as'=>'front.user_payment_forms','parameters' => [ 'user_payment_forms'        => 'user_payment_forms']]);
    Route::apiResource('user_subsidiaries'                      , 'App\Http\Controllers\Front\UserSubsidiariesController'           , ['as'=>'front.user_subsidiaries'  ,'parameters' => [ 'user_subsidiaries'        => 'user_subsidiaries']]);
});
