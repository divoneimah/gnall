<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/'                      , ['as'=>'admin.main_index'              , 'uses'=>'App\Http\Controllers\Admin\IndexController@index']);
Route::get('/termo_privacidade'     , ['as'=>'front.main_termo_privacidade'  , 'uses'=>'App\Http\Controllers\Front\PageController@termo_privacidade']);
