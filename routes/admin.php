<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
This route respond for api/admin*
|
*/

    
Route::middleware('guest')->get('/'             , ['as'=>'admin.index'                              , 'uses'=>'App\Http\Controllers\Auth\LoginAdminController@index']);
Route::middleware('guest')->post('/login_json'  , ['as'=>'admin.login.login_json'                   , 'uses'=>'App\Http\Controllers\Auth\LoginAdminController@login_json']);
Route::middleware('guest')->post('/login'       , ['as'=>'admin.login.login'                        , 'uses'=>'App\Http\Controllers\Auth\LoginAdminController@login']);
Route::middleware('guest')->get('/logout'       , ['as'=>'admin.login.logout'                      , 'uses'=>'App\Http\Controllers\Auth\LoginAdminController@logout']);
Route::middleware('guest')->get('/form_login'   , ['as'=>'admin.login.form_login'                   , 'uses'=>'App\Http\Controllers\Auth\LoginAdminController@index']);
Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/admin_users/edit/{admin_users}'                        , ['as'=>'admin.admin_users.edit'           , 'uses'=>'App\Http\Controllers\Admin\AdminUsersController@edit']);
    Route::get('/admin_users/create'                                    , ['as'=>'admin.admin_users.create'         , 'uses'=>'App\Http\Controllers\Admin\AdminUsersController@create']);
    Route::apiResource('admin_users'                                    , 'App\Http\Controllers\Admin\AdminUsersController'                 , ['as' => 'admin', 'parameters' => [ 'admin_users'              => 'admin_users']]);
    
    Route::get('/control_points/edit/{control_points}'                  , ['as'=>'admin.control_points.edit'           , 'uses'=>'App\Http\Controllers\Admin\ControlPointsController@edit']);
    Route::get('/control_points/create'                                 , ['as'=>'admin.control_points.create'         , 'uses'=>'App\Http\Controllers\Admin\ControlPointsController@create']);
    Route::apiResource('control_points'                                 , 'App\Http\Controllers\Admin\ControlPointsController'              , ['as' => 'admin', 'parameters' => [ 'control_points'           => 'control_points']]);
    
    Route::get('/cost_centers/edit/{cost_centers}'                      , ['as'=>'admin.cost_centers.edit'           , 'uses'=>'App\Http\Controllers\Admin\CostCentersController@edit']);
    Route::get('/cost_centers/create'                                   , ['as'=>'admin.cost_centers.create'         , 'uses'=>'App\Http\Controllers\Admin\CostCentersController@create']);
    Route::apiResource('cost_centers'                                   , 'App\Http\Controllers\Admin\CostCentersController'                , ['as' => 'admin', 'parameters' => [ 'cost_centers'             => 'cost_centers']]);
    
    Route::get('/departaments/edit/{departaments}'                      , ['as'=>'admin.departaments.edit'           , 'uses'=>'App\Http\Controllers\Admin\DepartamentsController@edit']);
    Route::get('/departaments/create'                                   , ['as'=>'admin.departaments.create'         , 'uses'=>'App\Http\Controllers\Admin\DepartamentsController@create']);
    Route::apiResource('departaments'                                   , 'App\Http\Controllers\Admin\DepartamentsController'                , ['as' => 'admin', 'parameters' => [ 'departaments'             => 'departaments']]);

    Route::get('/events/edit/{events}'                                  , ['as'=>'admin.events.edit'           , 'uses'=>'App\Http\Controllers\Admin\EventsController@edit']);
    Route::get('/events/create'                                         , ['as'=>'admin.events.create'         , 'uses'=>'App\Http\Controllers\Admin\EventsController@create']);
    Route::apiResource('events'                                         , 'App\Http\Controllers\Admin\EventsController'                     , ['as' => 'admin', 'parameters' => [ 'events'                   => 'events']]);
    
    Route::get('/event_expense_natures/edit/{event_expense_natures}'    , ['as'=>'admin.event_expense_natures.edit'           , 'uses'=>'App\Http\Controllers\Admin\EventExpenseNaturesController@edit']);
    Route::get('/event_expense_natures/create'                          , ['as'=>'admin.event_expense_natures.create'         , 'uses'=>'App\Http\Controllers\Admin\EventExpenseNaturesController@create']);
    Route::apiResource('event_expense_natures'                          , 'App\Http\Controllers\Admin\EventExpenseNaturesController'        , ['as' => 'admin', 'parameters' => [ 'event_expense_natures'    => 'event_expense_natures']]);
    
    Route::get('/event_receipts/edit/{event_receipts}'                  , ['as'=>'admin.event_receipts.edit'           , 'uses'=>'App\Http\Controllers\Admin\EventReceiptsController@edit']);
    Route::get('/event_receipts/create'                                 , ['as'=>'admin.event_receipts.create'         , 'uses'=>'App\Http\Controllers\Admin\EventReceiptsController@create']);
    Route::apiResource('event_receipts'                                 , 'App\Http\Controllers\Admin\EventReceiptsController'              , ['as' => 'admin', 'parameters' => [ 'event_receipts'           => 'event_receipts']]);
    
    Route::get('/expenses/edit/{expenses}'                              , ['as'=>'admin.expenses.edit'           , 'uses'=>'App\Http\Controllers\Admin\ExpensesController@edit']);
    Route::get('/expenses/create'                                       , ['as'=>'admin.expenses.create'         , 'uses'=>'App\Http\Controllers\Admin\ExpensesController@create']);
    Route::apiResource('expenses'                                       , 'App\Http\Controllers\Admin\ExpensesController'                   , ['as' => 'admin', 'parameters' => [ 'expenses'                 => 'expenses']]);
    
    Route::get('/expense_natures/edit/{expense_natures}'                , ['as'=>'admin.expense_natures.edit'           , 'uses'=>'App\Http\Controllers\Admin\ExpenseNaturesController@edit']);
    Route::get('/expense_natures/create'                                , ['as'=>'admin.expense_natures.create'         , 'uses'=>'App\Http\Controllers\Admin\ExpenseNaturesController@create']);
    Route::apiResource('expense_natures'                                , 'App\Http\Controllers\Admin\ExpenseNaturesController'             , ['as' => 'admin', 'parameters' => [ 'expense_natures'          => 'expense_natures']]);
    
    Route::get('/expense_receipts/edit/{expense_receipts}'              , ['as'=>'admin.expense_receipts.edit'           , 'uses'=>'App\Http\Controllers\Admin\ExpenseReceiptsController@edit']);
    Route::get('/expense_receipts/create'                               , ['as'=>'admin.expense_receipts.create'         , 'uses'=>'App\Http\Controllers\Admin\ExpenseReceiptsController@create']);
    Route::apiResource('expense_receipts'                               , 'App\Http\Controllers\Admin\ExpenseReceiptsController'            , ['as' => 'admin', 'parameters' => [ 'expense_receipts'         => 'expense_receipts']]);
    
    Route::get('/payment_forms/edit/{payment_forms}'                    , ['as'=>'admin.payment_forms.edit'           , 'uses'=>'App\Http\Controllers\Admin\PaymentFormsController@edit']);
    Route::get('/payment_forms/create'                                  , ['as'=>'admin.payment_forms.create'         , 'uses'=>'App\Http\Controllers\Admin\PaymentFormsController@create']);
    Route::post('/payment_forms/index_manager'                          , ['as'=>'admin.payment_forms.index_manager'  , 'uses'=>'App\Http\Controllers\Admin\PaymentFormsController@index_manager']);
    Route::apiResource('payment_forms'                                  , 'App\Http\Controllers\Admin\PaymentFormsController'               , ['as' => 'admin', 'parameters' => [ 'payment_forms'            => 'payment_forms']]);
    
    Route::get('/subsidiaries/edit/{subsidiaries}'                      , ['as'=>'admin.subsidiaries.edit'           , 'uses'=>'App\Http\Controllers\Admin\SubsidiariesController@edit']);
    Route::get('/subsidiaries/create'                                   , ['as'=>'admin.subsidiaries.create'         , 'uses'=>'App\Http\Controllers\Admin\SubsidiariesController@create']);
    Route::apiResource('subsidiaries'                                   , 'App\Http\Controllers\Admin\SubsidiariesController'               , ['as' => 'admin', 'parameters' => [ 'subsidiaries'             => 'subsidiaries']]);
    
    Route::get('/unit_measurements/edit/{unit_measurements}'            , ['as'=>'admin.unit_measurements.edit'           , 'uses'=>'App\Http\Controllers\Admin\UnitMeasurementsController@edit']);
    Route::get('/unit_measurements/create'                              , ['as'=>'admin.unit_measurements.create'         , 'uses'=>'App\Http\Controllers\Admin\UnitMeasurementsController@create']);
    Route::apiResource('unit_measurements'                              , 'App\Http\Controllers\Admin\UnitMeasurementsController'           , ['as' => 'admin', 'parameters' => [ 'unit_measurements'        => 'unit_measurements']]);
    
    Route::get('/users/edit/{users}'                            , ['as'=>'admin.users.edit'           , 'uses'=>'App\Http\Controllers\Admin\UsersController@edit']);
    Route::get('/users/create'                                  , ['as'=>'admin.users.create'         , 'uses'=>'App\Http\Controllers\Admin\UsersController@create']);
    Route::apiResource('users'                                  , 'App\Http\Controllers\Admin\UsersController'                      , ['as' => 'admin', 'parameters' => [ 'users'                    => 'users']]);
    Route::apiResource('user_cost_centers'                      , 'App\Http\Controllers\Admin\UserCostCentersController'            , ['parameters' => [ 'user_cost_centers'        => 'user_cost_centers']]);
    Route::apiResource('user_payment_forms'                     , 'App\Http\Controllers\Admin\UserPaymentFormsController'           , ['as'=>'admin.user_payment_forms','parameters' => [ 'user_payment_forms'        => 'user_payment_forms']]);
    Route::apiResource('user_subsidiaries'                      , 'App\Http\Controllers\Admin\UserSubsidiariesController'           , ['parameters' => [ 'user_subsidiaries'        => 'user_subsidiaries']]);
    Route::get('/dashboard', ['as'=>'admin.dashboard'           , 'uses'=>'App\Http\Controllers\Admin\DashboardController@index']);
});

