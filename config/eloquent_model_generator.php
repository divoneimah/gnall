<?php

return [
    'model_defaults' => [
        'namespace'       => 'App\\Models\\General',
        'output_path'     => 'Models/General',
        //'no_timestamps'   => true,
        //'date_format'     => 'U',
        //'connection'      => 'other-connection',
        //'backup'          => true,
    ],
    'db_types' => [
        'enum' => 'string',
    ],
];