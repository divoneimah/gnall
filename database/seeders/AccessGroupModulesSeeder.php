<?php

namespace Database\Seeders;

use App\Models\General\AccessGroupModules;
use Illuminate\Database\Seeder;

class AccessGroupModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccessGroupModules::create(
            ['name' => 'Admin', 'system_modules' => '{"data":["control_points", "cost_centers", "events",
                "unit_measurements", "expense_natures", "payment_forms", "subsidiaries", "expenses", "users"
                , "departaments"]}'],
        );
        AccessGroupModules::create(['name' => 'Nenhum', 'system_modules' => ''],
        );
        AccessGroupModules::create(['name' => 'Gestor', 'system_modules' => '{"data":[ "expenses"]}'],
        );
        
    }
}
