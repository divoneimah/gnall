<?php

namespace Database\Seeders;

use App\Models\Admin\AdminUsers;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminUsers::create(['name' => 'Admin', 'email' => 'admin@admin.com', 
        'password' => Hash::make("admin")]);
    }
}
