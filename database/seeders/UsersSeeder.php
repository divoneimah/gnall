<?php

namespace Database\Seeders;

use App\Models\General\Users;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::create(['name' => 'User', 'email' => 'user@user.com', 
        'password' => Hash::make("user")]);
    }
}
