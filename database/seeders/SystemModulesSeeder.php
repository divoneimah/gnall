<?php

namespace Database\Seeders;

use App\Models\General\SystemModules;
use Illuminate\Database\Seeder;

class SystemModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemModules::create(
            ['code' => 'admin_users']
        );
        SystemModules::create(
            ['code' => 'control_points']
        );
        SystemModules::create(
            ['code' => 'cost_centers']
        );
        SystemModules::create(
            ['code' => 'events']
        );
        SystemModules::create(
            ['code' => 'unit_measurements']
        );
        SystemModules::create(
            ['code' => 'expense_natures']
        );
        SystemModules::create(
            ['code' => 'payment_forms']
        );
        SystemModules::create(
            ['code' => 'subsidiaries']
        );
        SystemModules::create(
            ['code' => 'expenses']
        );
        SystemModules::create(
            ['code' => 'users']
        );
    }
}
