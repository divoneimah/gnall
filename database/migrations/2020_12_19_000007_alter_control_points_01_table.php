<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterControlPoints01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('control_points', function (Blueprint $table) {
            $table->dropColumn(['start_date', 'end_date']);
            $table->string('longitude', 50)->nullable();
            $table->string('latitude', 50)->nullable();
            $table->text("note")->nullable();
            $table->timestamps();
            $table->integer('created_user_id')->unsigned()->nullable();
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->integer('updated_user_id')->unsigned()->nullable();
            $table->foreign('updated_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('control_points', function (Blueprint $table) {
            $table->dropForeign('control_points_created_user_id_foreign');
            $table->dropForeign('control_points_updated_user_id_foreign');
            $table->dropColumn(['longitude', 'latitude', 'note', 'created_at','created_user_id', 'updated_at', 'updated_user_id']);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
        });
    }
}
