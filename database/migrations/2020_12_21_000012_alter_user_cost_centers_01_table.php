<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserCostCenters01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_cost_centers', function (Blueprint $table) {
            $table->integer('created_user_id')->unsigned()->nullable();
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->integer('updated_user_id')->unsigned()->nullable();
            $table->foreign('updated_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_cost_centers', function (Blueprint $table) {
            $table->dropForeign('user_cost_centers_created_user_id_foreign');
            $table->dropForeign('user_cost_centers_updated_user_id_foreign');
            $table->dropColumn(['created_user_id', 'updated_user_id', 'created_at', 'updated_at']);
        });
    }
}
