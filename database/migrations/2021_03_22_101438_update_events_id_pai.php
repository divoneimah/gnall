<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventsIdPai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //id pai movimentacao
        Schema::table('expenses', function (Blueprint $table) {
            $table->integer("expense_id_from")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn(['expense_id_from']);
        });
    }
}
