<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenses06Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropColumn(['is_removed']);
            $table->decimal("quantity", 9,3)->nullable()->default(0)->change();
            $table->decimal("unitary", 9,2)->nullable()->default(0)->change();
            $table->decimal("addition", 9,2)->nullable()->default(0)->change();
            $table->decimal("discount", 9,2)->nullable()->default(0)->change();
            $table->decimal("total", 9,2)->nullable()->default(0)->change();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->integer('is_removed')->unsigned()->default(0);
            $table->decimal("quantity", 9,3)->nullable()->change();
            $table->decimal("unitary", 9,2)->nullable()->change();
            $table->decimal("addition", 9,2)->nullable()->change();
            $table->decimal("discount", 9,2)->nullable()->change();
            $table->decimal("total", 9,2)->nullable()->change();
            $table->dropColumn(['created_at','updated_at','deleted_at']);
        });
    }
}
