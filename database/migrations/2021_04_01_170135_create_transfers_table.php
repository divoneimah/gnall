<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transfers');
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('id_debit_account')->unsigned();
            $table->integer('id_credit_account')->unsigned();
            $table->integer('subsidiary_id')->unsigned();
            $table->double('valor');
            $table->bigInteger('internal_app_id');
            $table->unique('internal_app_id');
            $table->timestamps();
            $table->integer('created_user_id')->unsigned();
            $table->integer('updated_user_id')->unsigned();
            $table->integer('deleted_user_id')->unsigned()->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
