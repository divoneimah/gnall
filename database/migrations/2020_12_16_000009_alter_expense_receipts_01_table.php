<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenseReceipts01Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_receipts', function (Blueprint $table) {
            $table->dropForeign('expense_receipts_expense_id_foreign');
            $table->dropForeign('expense_receipts_event_id_foreign');
            $table->dropForeign('expense_receipts_cost_center_id_foreign');
            $table->dropColumn(['cost_center_id', 'expense_date', 'sequencial_receipt']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_receipts', function (Blueprint $table) {
            $table->string('cost_center_id', 10)->nullable();
            $table->foreign('cost_center_id')->references('cost_center_id')->on('cost_centers');
            $table->date("expense_date")->nullable();
            $table->integer("sequencial_receipt")->nullable();
        });
    }
}
