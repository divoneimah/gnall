<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenses02Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->string('longitude', 50)->nullable();
            $table->string('latitude', 50)->nullable();
            $table->integer('user_from_id')->unsigned()->nullable();
            $table->foreign('user_from_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses', function (Blueprint $table) {
            $table->dropForeign('expenses_user_from_id_foreign');
            $table->dropColumn(['longitude', 'latitude', 'user_from_id']);
        });
    }
}
