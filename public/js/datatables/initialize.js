function carregatDatatable(ajax_call){
    $('#table').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": ajax_call,
        "pageLength": 10,
        "lengthMenu": [ 10 ],
        dom: 'Bfrtip',
        buttons: [
            {
                extend:'csv',
                className:'btn btn-primary'
            },{
                extend:'excel',
                className:'btn btn-primary'
            },{
                extend:'pdf',
                className:'btn btn-primary'
            }
        ],
        language:
            {
                "decimal":        "",
                "emptyTable":     "Nenhum registro disponível",
                "info":           "Mostrando _START_ até _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 para 0 de 0 registros",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrando _MENU_ registros",
                "loadingRecords": "Carregando...",
                "processing":     "Processing...",
                "search":         "Buscar:",
                "zeroRecords":    "Nenhum registro encontrado",
                "paginate": {
                    "first":      "Primeira",
                    "last":       "Última",
                    "next":       "Próxima",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }

    } ).on( 'order.dt',  function () {
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Deseja remover o registro?',
            btnOkLabel:"Sim",
            btnCancelLabel:"Não"
        });
     } );
}
