@extends('admin.dashboard.page')

@inject('layoutHelper', '\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Check-in')

@section('content_header')
    <h1>Check-in</h1>
@stop

@section('content')
<div class="col-md-12">
    <div class="box">

    @if ($message = Session::get('message'))
        <div class="alert alert-{{ $message['class'] }} alert-block">
            <button type="button" class="close" data-dismiss="{{ $message['class'] }}">×</button>
            <strong>{{ $message['message'] }}</strong>
        </div>
    @endif
        <!-- /.box-header -->
        <div class="box-body">
            <table id="table" class="table table-striped table-bordered table-hover">
                <thead><tr>
                    <th style="width: 10px">#</th>
                    <th>Usuário</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Observação</th>
                    <th>Data Criação</th>
                    <th>Data Atualização</th>
                    <th style="width: 40px">Ações</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <a class="btn btn-primary pull-right" href="{{route('admin.control_points.create')}}">Novo Registro</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/datatables/datatables.min.css">
    <link rel="stylesheet" href="/js/datatables/datatables.min.css">
@stop

@section('js')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.print.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="/js/datatables/initialize.js"></script>

<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.print.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="/js/datatables/initialize.js"></script>
<script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
<script>
		var CSRF_TOKEN;
		var dataTable;
		$(document).ready(function() {
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var ajax_call = "{{ route('admin.control_points.index', ['return_json'=>1])}}";
            carregatDatatable(ajax_call);
		});
		function confirmDelete(id)
		{
            $.ajax({
                url: "{{ route('admin.control_points.destroy','') }}/"+id,
                type: 'DELETE',
                data: {'_token':CSRF_TOKEN},
                success: function(response) {
                    if(response.success == true) {
                        var url = "{{ route('admin.control_points.index')}}";
                        window.location.href = url;
                    }else {
                        alert("Falha ao remover o Registro")
                    }
                },
                error: function(response){
                    alert("Falha ao remover o Registro")
                }
            });
		}


	</script>
@stop
