@extends('admin.dashboard.page')

@inject('layoutHelper', '\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Bem vindo ao painel administrativo.</p>
@stop

@section('css')
    <link rel="stylesheet" href="{{url('')}}/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
