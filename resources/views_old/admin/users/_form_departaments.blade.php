<div class="form-group col-md-6 {{ $errors->has('users.departament_id') ? 'has-error' : '' }}">
    <label for="departament_id">Departamento</label>
    <select 
        class="form-control " 
        name="users[departament_id]" 
        
        >
        <option value=''>Selecione</option>
        
        @php
        if(isset($departaments) && $departaments){
            foreach($departaments as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity) && $e->id ==  $entity->departament_id) 
                    echo "selected='selected'";
                echo ">". $e->description ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('users.departament_id'))
        <span class="help-block">{{ $errors->first('users.departament_id') }}</span>
    @endif
</div>