<div class="form-group {{ $errors->has('users.user_payment_forms_id') ? 'has-error' : '' }}">
    <div class="card-body p-0">
        <table  id="table" class="table">
            <thead>
                <tr>
                    <th style="width: 250px">Forma de Pagamento</th>
                    <th style="width: 50px">Número Cartão</th>
                    <th style="width: 50px">Limite Cartão</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width: 250px">
                    <select 
                    class="form-control js-data-ajax-provider select2"  
                    name="user_payment_forms[payment_form_id]" 
                    >
                    @php
                    if(isset($payment_forms) && $payment_forms){
                        foreach($payment_forms as $e){
                            echo "<option value='". $e->id."'";
                            echo ">". $e->description ." </option>";        
                        }
                    }
                    @endphp 
                        
                </select></td>
                    <td><input type='text' name='user_payment_forms[card_number]' value='' />
                    @if($errors->has('user_payment_forms.card_number'))
                        <span class="help-block">{{ $errors->first('user_payment_forms.card_number') }}</span>
                    @endif
                    </td>
                    <td><input type='text' style='width:100px' class="card_limit" id='card_limit' name='user_payment_forms[card_limit]' value='' /></td>
                    <td>
                    <div class="box-footer">
                        <button class="btn btn-success" type="submit">Adicionar</button>
                    </div>
                    </td>
                </tr> 
                @php
                if(isset($entity->userPaymentForms) && $entity->userPaymentForms){
                    foreach($entity->userPaymentForms as $e){
                        $path_parts = pathinfo($e->document_path);
                @endphp
                        <tr>
                            <td>{{$e->paymentForms->description }}
                            </td>
                            <td>{{  $e->card_number }}</td>
                            <td>{{ numbr($e->card_limit) }}</td>
                            <td>
                            <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('{{ $e->id }}')"><i class="fa fa-fw fa-trash"></i>{{ __('Remover') }}</a>
                            </td>
                        </tr>
                @php
                    }
                }
                @endphp
                
            </tbody>
        </table>
    </div>
    
</div>