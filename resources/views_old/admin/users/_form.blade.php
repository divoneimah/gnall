
<div class="form-group col-md-6 {{ $errors->has('users.name') ? 'has-error' : '' }}">
    <label for="nome">Nome</label>
    <input type="text" 
            id="name" 
            class="form-control" 
            name="users[name]" 
            value="{{ old('users.name', isset($entity->name) ? $entity->name : '') }}">

    @if($errors->has('users.name'))
        <span class="help-block">{{ $errors->first('users.name') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('users.email') ? 'has-error' : '' }}">
    <label for="email">Email</label>
    <input type="text" 
            id="email" 
            class="form-control" 
            name="users[email]" 
            value="{{ old('users.email', isset($entity->email) ? $entity->email : '') }}">

    @if($errors->has('users.email'))
        <span class="help-block">{{ $errors->first('users.email') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('users.password') ? 'has-error' : '' }}">
    <label for="password">Password</label>
    <input type="text" 
            id="password" 
            class="form-control" 
            name="users[password]" 
            value="{{ old('users.password', '') }}">

    @if($errors->has('users.password'))
        <span class="help-block">{{ $errors->first('users.password') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('users.password_confirmation') ? 'has-error' : '' }}">
    <label for="password_confirmation">Password Confirmation</label>
    <input type="text" 
            id="password_confirmation" 
            class="form-control" 
            name="users[password_confirmation]" 
            value="{{ old('users.password_confirmation', '') }}">

    @if($errors->has('users.password_confirmation'))
        <span class="help-block">{{ $errors->first('users.password_confirmation') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('users.access_group_module_id') ? 'has-error' : '' }}">
    <label for="access_group_module_id">Permissão</label>
    <select 
        class="form-control js-data-ajax-provider select2" 
        name="users[access_group_module_id]" 
        >
        @php
        if(isset($access_group_module) && $access_group_module){
            foreach($access_group_module as $e){
                echo "<option value='". $e->id."'";
                $_access_group_module_selected = old('users.access_group_module_id', isset($entity->userAccessGroups->access_group_module_id)?$entity->userAccessGroups->access_group_module_id:null);
                if(isset($_access_group_module_selected) && $e->id ==  $_access_group_module_selected) 
                    echo "selected='selected'";
                echo ">". $e->name ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('users.access_group_module_id'))
        <span class="help-block">{{ $errors->first('users.access_group_module_id') }}</span>
    @endif
</div>