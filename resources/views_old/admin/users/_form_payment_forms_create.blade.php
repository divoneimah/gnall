<div class="form-group {{ $errors->has('users.user_payment_forms_id') ? 'has-error' : '' }}">
    <div class="card-body p-0">
        <table  id="table" class="table">
            <thead>
                <tr>
                    <th style="width: 250px">Forma de Pagamento</th>
                    <th style="width: 50px">Número Cartão</th>
                    <th style="width: 50px">Limite Cartão</th>
                    <th>
                    <div class="box-footer">
                        <button class="btn btn-success add-pf" type="button">Adicionar</button>
                    </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr id='pf_header'>
                    <td style="width: 250px">
                    <select 
                    class="form-control js-data-ajax-provider "  
                    name="user_payment_forms[0][payment_form_id]" 
                    >
                    @php
                    if(isset($payment_forms) && $payment_forms){
                        foreach($payment_forms as $e){
                            echo "<option value='". $e->id."'";
                            echo ">". $e->description ." </option>";        
                        }
                    }
                    @endphp 
                        
                </select></td>
                    <td><input type='text' name='user_payment_forms[0][card_number]' value='' />
                    @if($errors->has('user_payment_forms.card_number'))
                        <span class="help-block">{{ $errors->first('user_payment_forms.card_number') }}</span>
                    @endif
                    </td>
                    <td><input type='text' style='width:100px' class="card_limit" id='card_limit' name='user_payment_forms[0][card_limit]' value='' /></td>
                    <td>
                    
                    </td>
                </tr> 
            </tbody>
        </table>
    </div>
    
</div>
