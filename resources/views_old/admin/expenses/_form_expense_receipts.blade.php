<div class="form-group col-md-6 {{ $errors->has('expenses.event_id') ? 'has-error' : '' }}">
    <label for="nome">Arquivos</label>
    <div class="card-body p-0">
        <table  id="table" class="table">
            <thead>
                <tr>
                    <th>Arquivo</th>
                    <th>Id Interno do App</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @php
                if(isset($entity->expenseReceipts) && $entity->expenseReceipts){
                    foreach($entity->expenseReceipts as $e){
                        $path_parts = pathinfo($e->server_path);
                @endphp
                        <tr>
                            <td><a href='{{ $e->server_path }}' target='_blank'>
                                <img src='{{ $e->server_path }}' width='100' height='100'/>
                                </a>
                            </td>
                            <td>{{ $e->internal_app_id }}</td>
                            <td>
                            <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('{{ $e->id }}')"><i class="fa fa-fw fa-trash"></i>{{ __('Remover') }}</a>
                            </td>
                        </tr>
                @php
                    }
                }
                @endphp 
            </tbody>
        </table>
    </div>
</div>

