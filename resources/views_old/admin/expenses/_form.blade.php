<div class="form-group col-md-6 {{ $errors->has('expenses.event_id') ? 'has-error' : '' }}">
    <label for="nome">Evento</label>
    <select
        class="form-control js-data-ajax-provider"
        name="expenses[event_id]"
        value="{{ old('expenses.event_id', isset($entity->event_id) ? $entity->event_id : '') }}">
        @php
        if(isset($events) && $events){
            foreach($events as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity->event_id) && $e->id == $entity->event_id)
                    echo "selected='selected'";
                echo ">". $e->id ." - ". $e->description ."</option>";
            }
        }
        @endphp

    </select>
    @if($errors->has('expenses.event_id'))
        <span class="help-block">{{ $errors->first('expenses.event_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.cost_center_id') ? 'has-error' : '' }}">
    <label for="cost_center_id">Centro de Custo</label>
    <select
        class="form-control js-data-ajax-provider"
        name="expenses[cost_center_id]"
        value="{{ old('expenses.cost_center_id', isset($entity->cost_center_id) ? $entity->cost_center_id : '') }}">
        @php
        if(isset($cost_centers) && $cost_centers){
            foreach($cost_centers as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity->cost_center_id) && $e->id == $entity->cost_center_id)
                    echo "selected='selected'";
                echo ">". $e->cost_center_id ." - ".  $e->description ."</option>";
            }
        }
        @endphp

    </select>
    @if($errors->has('expenses.cost_center_id'))
        <span class="help-block">{{ $errors->first('expenses.cost_center_id') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('expenses.subsidiary_id') ? 'has-error' : '' }}">
    <label for="nome">Filial</label>
    <select
        class="form-control js-data-ajax-provider"
        name="expenses[subsidiary_id]"
        value="{{ old('expenses.subsidiary_id', isset($entity->subsidiary_id) ? $entity->subsidiary_id : '') }}">
        @php
        if(isset($subsidiaries) && $subsidiaries){
            foreach($subsidiaries as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity->subsidiary_id) && $e->id == $entity->subsidiary_id)
                    echo "selected='selected'";
                echo ">". $e->name ."</option>";
            }
        }
        @endphp

    </select>
    @if($errors->has('expenses.subsidiary_id'))
        <span class="help-block">{{ $errors->first('expenses.subsidiary_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.user_id') ? 'has-error' : '' }}">
    <label for="user_id">Gestor</label>
    <select
        class="form-control js-data-ajax-provider"
        name="expenses[user_id]"
        id="user_id"
        value="{{ old('expenses.user_id', isset($entity->user_id) ? $entity->user_id : '') }}">
        <option value=''> Selecione uma opção</option>
        @php
        if(isset($users) && $users){
            foreach($users as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity->user_id) && $e->id == $entity->user_id)
                    echo "selected='selected'";
                echo ">". $e->name ."</option>";
            }
        }
        @endphp

    </select>
    @if($errors->has('expenses.user_id'))
        <span class="help-block">{{ $errors->first('expenses.user_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.user_payment_form_id') ? 'has-error' : '' }}">
    <label for="nome">Conta</label>
    <select
        id="user_payment_form_id"
        class="form-control js-data-ajax-provider"
        name="expenses[user_payment_form_id]"
        value="{{ old('expenses.user_payment_form_id', isset($entity->user_payment_form_id) ? $entity->user_payment_form_id : '') }}">
        <option value=''>Selecione um gestor</option>
    </select>
    @if($errors->has('expenses.user_payment_form_id'))
        <span class="help-block">{{ $errors->first('expenses.user_payment_form_id') }}</span>
    @endif
</div>



<div class="form-group col-md-6 {{ $errors->has('expenses.user_from_id') ? 'has-error' : '' }}">
    <label for="user_from_id">Usuário Geração</label>
    <select
        readonly
        class="form-control js-data-ajax-provider"
        name="expenses[user_from_id]"
        value="{{ old('expenses.user_from_id', isset($entity->user_from_id) ? $entity->user_from_id : '') }}">
        @php
        if(isset($user_from) && $user_from){
            foreach($user_from as $e){
                if(isset($user_id) && $e->id != $user_id) {
                    if(isset($create)){
                        continue;
                    }
                }
                if(isset($edit) &&
                    isset($entity->user_from_id) && $e->id != $entity->user_from_id){
                    continue;
                }
                echo "<option value='". $e->id."'";
                if(isset($entity->user_from_id) && $e->id == $entity->user_from_id)
                    echo "selected='selected'";
                echo ">". $e->name ."</option>";
            }
        }
        @endphp

    </select>
    @if($errors->has('expenses.user_from_id'))
        <span class="help-block">{{ $errors->first('expenses.user_from_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.expense_date') ? 'has-error' : '' }}">
    <label for="expense_date">Data da Despesa</label>
    <input type="text"
            id="expense_date"
            class="form-control"
            name="expenses[expense_date]"
            value="{{ old('expenses.expense_date', isset($entity->expense_date) ? \Carbon\Carbon::parse($entity->expense_date)->format('d/m/Y') : '') }}">

    @if($errors->has('expenses.expense_date'))
        <span class="help-block">{{ $errors->first('expenses.expense_date') }}</span>
    @endif
</div>
@if(!isset($edit))
<div class="form-group col-md-6 {{ $errors->has('expenses.quantity') ? 'has-error' : '' }}" style="display: none">
    <label for="quantity">Quantidade</label>
    <input type="text"
            id="quantity"
            class="form-control"
            name="expenses[quantity]"
            value="{{ old('expenses.quantity', isset($entity->quantity) ? $entity->quantity : '') }}">

    @if($errors->has('expenses.quantity'))
        <span class="help-block">{{ $errors->first('expenses.quantity') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.unitary') ? 'has-error' : '' }}" style="display: none">
    <label for="unitary">Unitário</label>
    <input type="text"
            id="unitary"
            class="form-control"
            name="expenses[unitary]"
            value="{{ old('expenses.unitary', isset($entity->unitary) ? $entity->unitary : '') }}">

    @if($errors->has('expenses.unitary'))
        <span class="help-block">{{ $errors->first('expenses.unitary') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.addition') ? 'has-error' : '' }}" style="display: none">
    <label for="addition">Acréscimo</label>
    <input type="text"
            id="addition"
            class="form-control"
            name="expenses[addition]"
            value="{{ old('expenses.addition', isset($entity->addition) ? $entity->addition : '') }}">

    @if($errors->has('expenses.addition'))
        <span class="help-block">{{ $errors->first('expenses.addition') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.discount') ? 'has-error' : '' }}" style="display: none">
    <label for="discount">Desconto</label>
    <input type="text"
            id="discount"
            class="form-control"
            name="expenses[discount]"
            value="{{ old('expenses.discount', isset($entity->discount) ? $entity->discount : '') }}">

    @if($errors->has('expenses.discount'))
        <span class="help-block">{{ $errors->first('expenses.discount') }}</span>
    @endif
</div>
@endif
<div class="form-group col-md-6 {{ $errors->has('expenses.total') ? 'has-error' : '' }}">
    <label for="total">Total</label>
    <input type="text"
            id="total"
            class="form-control"
            name="expenses[total]"
            value="{{ old('expenses.total', isset($entity->total) ? $entity->total : '') }}">

    @if($errors->has('expenses.total'))
        <span class="help-block">{{ $errors->first('expenses.total') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expenses.note') ? 'has-error' : '' }}">
    <label for="note">Observação</label>
    <input type="text"
            id="note"
            class="form-control"
            name="expenses[note]"
            value="{{ old('expenses.note', isset($entity->note) ? $entity->note : '') }}">

    @if($errors->has('expenses.note'))
        <span class="help-block">{{ $errors->first('expenses.note') }}</span>
    @endif
</div>

@if($activate_location ==2)
    <div class="form-group col-md-6 {{ $errors->has('expenses.latitude') ? 'has-error' : '' }}">
        <label for="latitude">Latitude</label>
        <input type="text"
                id="latitude"
                class="form-control"
                name="expenses[latitude]"
                value="{{ old('expenses.latitude', isset($entity->latitude) ? $entity->latitude : '') }}">

        @if($errors->has('expenses.latitude'))
            <span class="help-block">{{ $errors->first('expenses.latitude') }}</span>
        @endif
    </div>

    <div class="form-group col-md-6 {{ $errors->has('expenses.longitude') ? 'has-error' : '' }}">
        <label for="longitude">Longitude</label>
        <input type="text"
                id="longitude"
                class="form-control"
                name="expenses[longitude]"
                value="{{ old('expenses.longitude', isset($entity->longitude) ? $entity->longitude : '') }}">

        @if($errors->has('expenses.longitude'))
            <span class="help-block">{{ $errors->first('expenses.longitude') }}</span>
        @endif
    </div>
@endif

<div id="mapid"></div>
@section('edit_js')
<style>
    #mapid { height: 360px; }
</style>
<script>
    var CSRF_TOKEN;
      $(document).ready(function() {
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function searchManager(){
            $.ajax( {
                dataType: 'json',
                method: "POST",
                data:{"user_id":$('select[id="user_id"]').val(), "_token": CSRF_TOKEN},
                url: "{{route('admin.payment_forms.index_manager')}}",
                success: function ( json ) {
                    var user_payment_form_id_edit="";
                    $('#user_payment_form_id').empty();
                    // Use jQuery's each to iterate over the opts value
                    $.each(json.data.entities, function(i, d) {
                        var selected = "";
                        @if(isset($edit))
                            if("{{ $entity->user_payment_form_id }} " == d.id)
                                selected = "selected=selected";
                        @endif
                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#user_payment_form_id').append('<option '+selected+' value="' + d.id + '">' + d.payment_form_name + ' - '+ d.card_number_last + '</option>');
                    });
                },
                error: function () {
                    $('#user_payment_form_id').empty();
                }
            } );

        }
        $("#user_id").on("change", function(){
            searchManager();
        });
        @if(isset($edit))
            searchManager();
        @endif
      });

    var mymap = L.map('mapid').setView([{{ old('expenses.latitude', isset($entity->latitude) ? $entity->latitude : '') }}, {{ old('expenses.longitude', isset($entity->longitude) ? $entity->longitude : '') }}], 13);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);
    var marker = L.marker([{{ old('expenses.latitude', isset($entity->latitude) ? $entity->latitude : '') }}, {{ old('expenses.longitude', isset($entity->longitude) ? $entity->longitude : '') }}]).addTo(mymap);
    marker.dragging.disable();
    var draggable = new L.Draggable(mymap);
    draggable.disable();

</script>
@stop
