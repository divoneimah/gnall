<div class="form-group col-md-6 {{ $errors->has('payment_forms.expense_nature_id') ? 'has-error' : '' }}">
    <label for="nome">Tipo da Conta</label>
    <select 
            class="form-control js-data-ajax-provider" 
            name="payment_forms[account_type]" 
            value="{{ old('payment_forms.account_type', isset($entity->account_type) ? $entity->account_type : '') }}">
            @php
            if(isset($_account_types) && $_account_types){
                foreach($_account_types as $key => $e){
                    echo "<option value='". $key."'"; 
                    if(isset($entity->account_type) && $key == $entity->account_type) 
                    echo "selected='selected'";
                    echo ">". $e ."</option>";        
                }
            }
            @endphp 
    </select>
    @if($errors->has('payment_forms.expense_nature_id'))
        <span class="help-block">{{ $errors->first('payment_forms.expense_nature_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('payment_forms.description') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="payment_forms[description]" 
            value="{{ old('payment_forms.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('payment_forms.description'))
        <span class="help-block">{{ $errors->first('payment_forms.description') }}</span>
    @endif
</div>