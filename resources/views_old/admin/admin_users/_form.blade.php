<div class="form-group col-md-6 {{ $errors->has('admin_users.name') ? 'has-error' : '' }}">
    <label for="nome">Nome</label>
    <input type="text" 
            id="name" 
            class="form-control" 
            name="admin_users[name]" 
            value="{{ old('admin_users.name', isset($entity->name) ? $entity->name : '') }}">

    @if($errors->has('admin_users.name'))
        <span class="help-block">{{ $errors->first('admin_users.name') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('admin_users.email') ? 'has-error' : '' }}">
    <label for="email">Email</label>
    <input type="text" 
            id="email" 
            class="form-control" 
            name="admin_users[email]" 
            value="{{ old('admin_users.email', isset($entity->email) ? $entity->email : '') }}">

    @if($errors->has('admin_users.email'))
        <span class="help-block">{{ $errors->first('admin_users.email') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('admin_users.password') ? 'has-error' : '' }}">
    <label for="password">Password</label>
    <input type="text" 
            id="password" 
            class="form-control" 
            name="admin_users[password]" 
            value="{{ old('admin_users.password', '') }}">

    @if($errors->has('admin_users.password'))
        <span class="help-block">{{ $errors->first('admin_users.password') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('admin_users.password_confirmation') ? 'has-error' : '' }}">
    <label for="password_confirmation">Password Confirmation</label>
    <input type="text" 
            id="password_confirmation" 
            class="form-control" 
            name="admin_users[password_confirmation]" 
            value="{{ old('admin_users.password_confirmation', '') }}">

    @if($errors->has('admin_users.password_confirmation'))
        <span class="help-block">{{ $errors->first('admin_users.password_confirmation') }}</span>
    @endif
</div>