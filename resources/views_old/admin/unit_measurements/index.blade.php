@extends('admin.dashboard.page')

@inject('layoutHelper', '\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Unidade de Medida')

@section('content_header')
    <h1>Unidade de Medida</h1>
@stop

@section('content')
<div class="col-md-12">
    <div class="box">

    @if ($message = Session::get('message'))
        <div class="alert alert-{{ $message['class'] }} alert-block">
            <button type="button" class="close" data-dismiss="{{ $message['class'] }}">×</button>
            <strong>{{ $message['message'] }}</strong>
        </div>
    @endif
        <!-- /.box-header -->
        <div class="box-body">
            <table  id="table" class="table table-striped table-bordered table-hover">
                <thead><tr>
                    <th style="width: 10px">#</th>
                    <th>Id Unidade de Medida</th>
                    <th>Descrição</th>
                    <th style="width: 40px">Ações</th>
                </tr></thead>
                <tbody>
                @foreach($entities as $entity)
                    <tr>
                        <td>#{{ $entity->id }}</td>
                        <td>{{ $entity->unit_measure_id }}</td>
                        <td>{{ $entity->description }}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-default" href="{{ route('admin.unit_measurements.edit',$entity->id) }}"><i class="fa fa-fw fa-edit"> </i>{{ __('Editar') }}</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('{{ $entity->id }}')"><i class="fa fa-fw fa-trash"></i>{{ __('Remover') }}</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary pull-right" href="{{route('admin.unit_measurements.create')}}">Novo Registro</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/datatables/datatables.min.css">
@stop

@section('js')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.print.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="/js/datatables/initialize.js"></script>
<script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
<script>
		var CSRF_TOKEN;
		var dataTable;
		$(document).ready(function() {
            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var ajax_call = "{{ route('admin.unit_measurements.index', ['return_json'=>1])}}";
            carregatDatatable(ajax_call);
		});
		function confirmDelete(id)
		{
            $.ajax({
                url: "{{ route('admin.unit_measurements.destroy','') }}/"+id,
                type: 'DELETE',
                data: {'_token':CSRF_TOKEN},
                success: function(response) {
                    if(response.success == true) {
                        var url = "{{ route('admin.unit_measurements.index')}}";
                        window.location.href = url;
                    }else {
                        alert("Falha ao remover o Registro")
                    }
                },
                error: function(response){
                    alert("Falha ao remover o Registro")
                }
            });
		}

	</script>
@stop
