<div class="form-group col-md-6 {{ $errors->has('cost_centers.cost_center_id') ? 'has-error' : '' }}">
    <label for="nome">Centro de Custo ERP</label>
    <input type="text" 
            id="cost_center_id" 
            class="form-control" 
            name="cost_centers[cost_center_id]" 
            value="{{ old('cost_centers.cost_center_id', isset($entity->cost_center_id) ? $entity->cost_center_id : '') }}">

    @if($errors->has('cost_centers.cost_center_id'))
        <span class="help-block">{{ $errors->first('cost_centers.cost_center_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('cost_centers.description') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="cost_centers[description]" 
            value="{{ old('cost_centers.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('cost_centers.description'))
        <span class="help-block">{{ $errors->first('cost_centers.description') }}</span>
    @endif
</div>