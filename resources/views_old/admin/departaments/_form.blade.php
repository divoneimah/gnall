<div class="form-group col-md-6 {{ $errors->has('departaments.expense_nature_id') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="departaments[description]" 
            value="{{ old('departaments.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('departaments.description'))
        <span class="help-block">{{ $errors->first('departaments.description') }}</span>
    @endif
</div>