<div class="form-group col-md-6 {{ $errors->has('expense_natures.expense_nature_id') ? 'has-error' : '' }}">
    <label for="nome">Natureza gasto ERP</label>
    <input type="text" 
            id="expense_nature_id" 
            class="form-control"
            maxlength='9' 
            name="expense_natures[expense_nature_id]" 
            value="{{ old('expense_natures.expense_nature_id', isset($entity->expense_nature_id) ? $entity->expense_nature_id : '') }}">

    @if($errors->has('expense_natures.expense_nature_id'))
        <span class="help-block">{{ $errors->first('expense_natures.expense_nature_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('expense_natures.description') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="expense_natures[description]" 
            value="{{ old('expense_natures.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('expense_natures.description'))
        <span class="help-block">{{ $errors->first('expense_natures.description') }}</span>
    @endif
</div>