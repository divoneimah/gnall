<div class="form-group col-md-6 {{ $errors->has('subsidiaries.expense_nature_id') ? 'has-error' : '' }}">
    <label for="name">Nome</label>
    <input type="text" 
            id="name" 
            class="form-control" 
            name="subsidiaries[name]" 
            value="{{ old('subsidiaries.name', isset($entity->name) ? $entity->name : '') }}">

    @if($errors->has('subsidiaries.name'))
        <span class="help-block">{{ $errors->first('subsidiaries.name') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('subsidiaries.cnpj') ? 'has-error' : '' }}">
    <label for="cnpj">CNPJ</label>
    <input type="text" 
            id="cnpj" 
            class="form-control" 
            name="subsidiaries[cnpj]" 
            value="{{ old('subsidiaries.cnpj', isset($entity->cnpj) ? $entity->cnpj : '') }}">

    @if($errors->has('subsidiaries.cnpj'))
        <span class="help-block">{{ $errors->first('subsidiaries.cnpj') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('subsidiaries.internal_app_id') ? 'has-error' : '' }}">
    <label for="internal_app_id">Filial ERP</label>
    <input type="text" 
            id="internal_app_id" 
            class="form-control" 
            name="subsidiaries[internal_app_id]" 
            value="{{ old('subsidiaries.internal_app_id', isset($entity->internal_app_id) ? $entity->internal_app_id : '') }}">

    @if($errors->has('subsidiaries.internal_app_id'))
        <span class="help-block">{{ $errors->first('subsidiaries.internal_app_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('subsidiaries.active') ? 'has-error' : '' }}">
    <input type="checkbox" 
            id="active" 
            class="" 
            name="subsidiaries[active]" 
            value="1"
            @if(isset($entity->active) && $entity->active == 1)
            checked='checked'
            @endif
            >
            <label for="active">Ativo</label>

    @if($errors->has('subsidiaries.active'))
        <span class="help-block">{{ $errors->first('subsidiaries.active') }}</span>
    @endif
</div>