@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Filiais')

@section('content_header')
    <h1>Novo Registro</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="box box-success">
      <form action="{{ route('admin.subsidiaries.store') }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          @include('admin.subsidiaries._form')
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Criar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/daterangepicker/daterangepicker.css">
@stop

@section('js')
  <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>

  <script>
    $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
  </script>
@stop
