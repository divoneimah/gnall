<div class="form-group col-md-6 {{ $errors->has('users.subsidiary_id') ? 'has-error' : '' }}">
    <label for="subsidiary_id">Filiais</label>
    <select 
        class="form-control js-data-ajax-provider select2" 
        name="users[subsidiary_id][]" 
        multiple
        >
        @php
        if(isset($subsidiaries) && $subsidiaries){
            $_user_subsidiaries = [];
            if(isset($entity->userSubsidiaries)){
                foreach($entity->userSubsidiaries as $u){
                    $_user_subsidiaries[] = $u->subsidiary_id;
                }
            }
            foreach($subsidiaries as $e){
                echo "<option value='". $e->id."'";
                $_subsidiaries_selected = old('users.subsidiary_id', isset($entity->userSubsidiaries)?$_user_subsidiaries:null);
                if(isset($_subsidiaries_selected) && in_array($e->id, $_subsidiaries_selected)) 
                    echo "selected='selected'";
                echo ">". $e->name ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('users.subsidiary_id'))
        <span class="help-block">{{ $errors->first('users.subsidiary_id') }}</span>
    @endif
</div>