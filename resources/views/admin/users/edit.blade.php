@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Usuários')

@section('content_header')
    <h1>Editar</h1>
@stop

@section('content')
  <div class="col-md-12">
    @if ($message = Session::get('message'))
        <div class="alert alert-{{ $message['class'] }} alert-block">
            <button type="button" class="close" data-dismiss="{{ $message['class'] }}">×</button>
            @if(is_array($message['message']))
              @foreach($message['message'] as $m)
              <strong>{{ $m }}</strong>

              @endforeach
            @else
            <strong>{{ $message['message'] }}</strong>
            @endif
        </div>
    @endif
    <div class="box box-success">
      <form action="{{ route('admin.users.update', $entity->id) }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="put">
          <input type="hidden" name="users[id]" value="{{$entity->id}}">
        </div>
        <div class="card-header">
            <h3 class="card-title">Dados Gerais</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form')
        </div>
        <div class="card-header">
            <h3 class="card-title">Departamento</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_departaments')
        </div>
        <div class="card-header">
            <h3 class="card-title">Centro de Custos</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_cost_centers')
        </div>
        <div class="card-header">
            <h3 class="card-title">Filiais</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_subsidiaries')
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Atualizar</button>
        </div>
      </form>
      <div class="card-header">
          <h3 class="card-title">Formas de Pagamentos</h3>
      </div>
      <div class="card-body">
        <form action="{{ route('admin.user_payment_forms.user_payment_forms.store') }}" method="post">
          {{csrf_field()}}
          <input type="hidden" name="user_payment_forms[user_id]" value="{{$entity->id}}">
          @include('admin.users._form_payment_forms')
        </form>
      </div>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="/js/select2/css/select2.min.css" rel="stylesheet" />
@stop

@section('js')
  <script src="/js/select2/js/select2.min.js"></script>
  <script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
  <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>
  <script>
  var CSRF_TOKEN;
		var dataTable;
    function confirmDelete(id)
    {
          $.ajax({
              url: "{{ route('admin.user_payment_forms.user_payment_forms.destroy','') }}/"+id,
              type: 'DELETE',
              data: {'_token':CSRF_TOKEN},
              success: function(response) {
                  if(response.success == true) {
                      var url = "{{ route('admin.users.edit', $entity->id)}}";
                      window.location.href = url;
                  }else {
                      alert("Falha ao remover o Registro")
                  }
              },
              error: function(response){
                alert("Falha ao remover o Registro")
              }
          });
    }
    $(document).ready(function() {
      $('.select2').select2();
			CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $('[data-toggle=confirmation]').confirmation({
          rootSelector: '[data-toggle=confirmation]',
          title: 'Deseja remover o registro?',
          btnOkLabel:"Sim",
          btnCancelLabel:"Não"
          });
      });
      $('#card_limit').mask('000000000.00', {reverse: true});
  </script>
@stop
