@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Usuários')

@section('content_header')
    <h1>Novo</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="card card-primary">
      <form action="{{ route('admin.users.store') }}" method="post">
        {{csrf_field()}}
        <div class="card-header">
            <h3 class="card-title">Dados Gerais</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form')
        </div>
        <div class="card-header">
            <h3 class="card-title">Departamento</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_departaments')
        </div>
        <div class="card-header">
            <h3 class="card-title">Centro de Custos</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_cost_centers')
        </div>

        <div class="card-header">
            <h3 class="card-title">Filiais</h3>
        </div>
        <div class="card-body">
          @include('admin.users._form_subsidiaries')
        </div>
        <div class="card-header">
          <h3 class="card-title">Formas de Pagamentos</h3>
        </div>
        <div class="card-body">
            @include('admin.users._form_payment_forms_create')
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Criar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="/js/select2/css/select2.min.css" rel="stylesheet" />
@stop

@section('js')
  <script src="/js/select2/js/select2.min.js"></script>
  <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>

  <script>
    $(document).ready(function() {
      $('.select2').select2();
      var count_row = 1;
      $(".add-pf").on("click", function(){
            var pf_header = $("#pf_header").clone().html();
            var er = /\[[0-9]*\]/gi;
            pf_header = pf_header.replace(er, "["+ count_row +"]");
            $("#pf_header").after("<tr>"+pf_header +"</tr>");
            count_row++;
            $('.card_limit').unmask();
            $('.card_limit').mask('000000000.00', {reverse: true});

      });
      $('.card_limit').unmask();
      $('.card_limit').mask('000000000.00', {reverse: true});
    });
</script>
  </script>
@stop
