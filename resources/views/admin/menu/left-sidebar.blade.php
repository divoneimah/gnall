<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    <a href="{{ route('admin.dashboard')}}"
        @if($layoutHelper->isLayoutTopnavEnabled())
            class="navbar-brand {{ config('adminlte.classes_brand') }}"
        @else
            class="brand-link {{ config('adminlte.classes_brand') }}"
        @endif>

        {{-- Small brand logo --}}
        <img src="{{ asset(config('adminlte.logo_img', 'vendor/adminlte/dist/img/AdminLTELogo.png')) }}"
            alt="{{ config('adminlte.logo_img_alt', 'AdminLTE') }}"
            class="{{ config('adminlte.logo_img_class', 'brand-image img-circle elevation-3') }}"
            style="opacity:.8">

        {{-- Brand text --}}
        <span class="brand-text font-weight-light {{ config('adminlte.classes_brand_text') }}">
            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
        </span>

    </a>

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('adminlte.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if(config('adminlte.sidebar_nav_animation_speed') != 300)
                    data-animation-speed="{{ config('adminlte.sidebar_nav_animation_speed') }}"
                @endif
                @if(!config('adminlte.sidebar_nav_accordion'))
                    data-accordion="false"
                @endif>

                <li class="nav-header ">
                    Geral
                </li>
                @if(securityAccess(App\Models\General\SystemModules::$SM_USERS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.users.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Gestor</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_DEPARTAMENTS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.departaments.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Departamento</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_CONTROL_POINTS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.control_points.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Check-in</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_COST_CENTERS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.cost_centers.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Centro de Custo</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_UNIT_MEASUREMENTS_INDEX))
                <li class="nav-item" style="display:none;">
                    <a class="nav-link  " href="{{ route('admin.unit_measurements.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Unidade de Medida</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_EXPENSE_NATURES_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.expense_natures.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Natureza Gasto</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_PAYMENT_FORMS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.payment_forms.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Contas</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_EVENTS_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.events.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Eventos</p>
                    </a>
                </li>
                @endif
                @if(securityAccess(App\Models\General\SystemModules::$SM_SUBSIDIARIES_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.subsidiaries.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Filiais</p>
                    </a>
                </li>
                @endif
                <li class="nav-header ">
                    Financeiro
                </li>
                @if(securityAccess(App\Models\General\SystemModules::$SM_EXPENSES_INDEX))
                <li class="nav-item">
                    <a class="nav-link  " href="{{ route('admin.expenses.index')}}">
                        <i class="fas fa-fw fa-user "></i>
                        <p>Extratos</p>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
    </div>

</aside>
