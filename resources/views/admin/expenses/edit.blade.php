@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Despesas')

@section('content_header')
    <h1>Editar - Despesa</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="card card-primary">
      <form action="{{ route('admin.expenses.update', $entity->id) }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          <div class="card-header">
            <h3 class="card-title">Dados Gerais</h3>
          </div>
          <input type="hidden" name="_method" value="put">
          <input type="hidden" name="expenses[id]" value="{{$entity->id}}">
          <div class="card-body">
            @include('admin.expenses._form', array('activate_location'=>0, 'edit' => 1))
          </div>
          <div class="card-header">
            <h3 class="card-title">Anexos</h3>
          </div>
          <div class="card-body">
            @include('admin.expenses._form_expense_receipts')
          </div>
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Atualizar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>
@stop

@section('js')
    <script src="/js/daterangepicker/moment.min.js"></script>
    <script src="/js/daterangepicker/daterangepicker.js"></script>
    <script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
    <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
      integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
      crossorigin=""></script>

    <script>
      var CSRF_TOKEN;
      var dataTable;
      $(document).ready(function() {
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      });
      function confirmDelete(id)
      {
              $.ajax({
                  url: "{{ route('admin.expense_receipts.destroy','') }}/"+id,
                  type: 'DELETE',
                  data: {'_token':CSRF_TOKEN},
                  success: function(response) {
                      if(response.success == true) {
                          var url = "{{ route('admin.expenses.edit', $entity->id)}}";
                          window.location.href = url;
                      }else {
                          alert("Falha ao remover o Registro")
                      }
                  },
                  error: function(response){
                      alert("Falha ao remover o Registro")
                  }
              });
      }
      $('[data-toggle=confirmation]').confirmation({
      rootSelector: '[data-toggle=confirmation]',
      title: 'Deseja remover o registro?',
      btnOkLabel:"Sim",
      btnCancelLabel:"Não"
      });
      $('#expense_date').daterangepicker({
          singleDatePicker: true,
          timePicker: false,
          locale: {
              format: 'DD/MM/YYYY'
          }
      });
    $('#unitary').mask('000000000.00', {reverse: true});
    $('#addition').mask('000000000.00', {reverse: true});
    $('#discount').mask('000000000.00', {reverse: true});
    $('#total').mask('000000000.00', {reverse: true});
    $('#expense_date').mask('00/00/0000');
    //$('#latitude').mask('000.00000000', {reverse: true});
    //$('#longitude').mask('000.00000000', {reverse: true});
	  </script>
@stop
