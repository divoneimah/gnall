@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Despesas')

@section('content_header')
    <h1>Novo</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="box box-success">
      <form action="{{ route('admin.expenses.store') }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          @include('admin.expenses._form', array('activate_location'=>1, 'create'=>1))
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Criar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/daterangepicker/daterangepicker.css">
@stop

@section('js')
    <script src="/js/daterangepicker/moment.min.js"></script>
    <script src="/js/daterangepicker/daterangepicker.js"></script>
    <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>

    <script>
    $('#expense_date').daterangepicker({
        singleDatePicker: true,
        timePicker: false,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('#unitary').mask('000000000.00', {reverse: true});
    $('#addition').mask('000000000.00', {reverse: true});
    $('#discount').mask('000000000.00', {reverse: true});
    $('#total').mask('000000000.00', {reverse: true});
    $('#expense_date').mask('00/00/0000');
    //$('#latitude').mask('000.00000000', {reverse: true});
    //$('#longitude').mask('000.00000000', {reverse: true});
    </script>
@stop
