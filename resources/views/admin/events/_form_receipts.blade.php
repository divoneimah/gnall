<div class="form-group col-md-6 {{ $errors->has('event_receipts.cost_center_id') ? 'has-error' : '' }}">
    <table  id="table" class="table table-striped table-bordered table-hover">
                <tbody>
                <form action="{{ route('admin.event_receipts.store') }}" method="post">
                {{csrf_field()}}
                <tr>
                    <td>
                    <input type='hidden' name='event_receipts[event_id]' value='{{ $entity->id }}' />
                    <input type='text' name='event_receipts[description]' value='' />
                    @if($errors->has('event_receipts.description'))
                        <span class="help-block">{{ $errors->first('event_receipts.description') }}</span>
                    @endif
                    </td>
                    <td><div class="box-footer">
                        <button class="btn btn-success" type="submit">Adicionar</button>
                    </div>
                    </td>
                </tr> 
                </form>

                @php
                if(isset($entity->eventReceipts) && $entity->eventReceipts){
                    foreach($entity->eventReceipts as $e){
                        @endphp
                        <form action="{{ route('admin.event_receipts.update', $e->id) }}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        
                        @php
                        echo "<tr><th><input type='text' name='event_receipts[description]' value='". $e->description."' /></th>";
                        @endphp 
                        <th>
                        <input type='hidden' name='event_receipts[event_id]' value='{{ $e->event_id }}' />
                        <button class="btn btn-success"><i class="fa fa-fw fa-edit"></i>Atualizar</button>
                        <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('{{ $e->id }}')"><i class="fa fa-fw fa-trash"></i>{{ __('Remover') }}</a>
                        </th>
                        @php
                        echo "</tr>";
                        @endphp
                        </form>
                        @php
                    }
                }
                @endphp 
    </tbody>
    </table>
</div>
