<div class="form-group col-md-6 {{ $errors->has('events.unit_measure_id') ? 'has-error' : '' }}">
    <label for="nome">Id Unidade de medida</label>
    <select 
        class="form-control js-data-ajax-provider" 
        name="events[unit_measure_id]" 
        value="{{ old('events.unit_measure_id', isset($entity->unit_measure_id) ? $entity->unit_measure_id : '') }}">
        @php
        if(isset($unit_measurements) && $unit_measurements){
            foreach($unit_measurements as $e){
                echo "<option value='". $e->id."'";
                if(isset($entity->unit_measure_id) && $e->id == $entity->unit_measure_id) 
                    echo "selected='selected'";
                echo ">". $e->unit_measure_id ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('events.unit_measure_id'))
        <span class="help-block">{{ $errors->first('events.unit_measure_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('events.expense_nature_id') ? 'has-error' : '' }}">
    <label for="expense_nature_id">Natureza do Gasto</label>
    <select 
        class="form-control js-data-ajax-provider" 
        name="events[expense_nature_id]" 
        value="{{ old('events.expense_nature_id', isset($entity->expense_nature_id) ? $entity->expense_nature_id : '') }}">
        @php
        if(isset($expense_natures) && $expense_natures){
            foreach($expense_natures as $e){
                echo "<option value='". $e->expense_nature_id."'";
                if(isset($entity->expense_nature_id) && $e->id == $entity->expense_nature_id) 
                    echo "selected='selected'";
                echo ">". $e->description ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('events.expense_nature_id'))
        <span class="help-block">{{ $errors->first('events.expense_nature_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('events.movement_type') ? 'has-error' : '' }}">
    <label for="movement_type">Tipo de Movimento</label>
    <select 
        class="form-control js-data-ajax-provider" 
        name="events[movement_type]" 
        value="{{ old('events.movement_type', isset($entity->movement_type) ? $entity->movement_type : '') }}">
        @php
        if(isset($movement_types) && $movement_types){
            foreach($movement_types as $key => $e){
                echo "<option value='". $key."'";
                if(isset($entity->movement_type) && $key == $entity->movement_type) 
                    echo "selected='selected'";
                echo ">". $e ."</option>";        
            }
        }
        @endphp 
            
    </select>
    @if($errors->has('events.movement_type'))
        <span class="help-block">{{ $errors->first('events.movement_type') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('events.description') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="events[description]" 
            value="{{ old('events.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('events.description'))
        <span class="help-block">{{ $errors->first('events.description') }}</span>
    @endif
</div>

