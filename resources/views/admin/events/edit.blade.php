@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Eventos')

@section('content_header')
    <h1>Editar</h1>
@stop

@section('content')
  <div class="col-md-12">
    @if ($message = Session::get('message'))
        <div class="alert alert-{{ $message['class'] }} alert-block">
            <button type="button" class="close" data-dismiss="{{ $message['class'] }}">×</button>
            <strong>{{ $message['message'] }}</strong>
        </div>
    @endif
    <div class="box box-success">
      <form action="{{ route('admin.events.update', $entity->id) }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="put">
          <input type="hidden" name="events[id]" value="{{$entity->id}}">
          <div class="card-body">
            @include('admin.events._form')
          </div>
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Atualizar</button>
        </div>
      </form>
      <div class="card-header">
          <h3 class="card-title">Comprovantes</h3>
      </div>
      <div class="card-body">
        <div class="box-body">
          @include('admin.events._form_receipts')
        </div>
      </div>

    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/daterangepicker/daterangepicker.css">
@stop

@section('js')
  <script src="/js/select2/js/select2.min.js"></script>
  <script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
  <script src="/js/jquery-mask-plugin/jquery.mask.min.js"></script>
  <script>
  var CSRF_TOKEN;
		var dataTable;
    function confirmDelete(id)
    {
          $.ajax({
              url: "{{ route('admin.event_receipts.destroy','') }}/"+id,
              type: 'DELETE',
              data: {'_token':CSRF_TOKEN},
              success: function(response) {
                  if(response.success == true) {
                      var url = "{{ route('admin.events.edit', $entity->id)}}";
                      window.location.href = url;
                  }else {
                      alert("Falha ao remover o Registro")
                  }
              },
              error: function(response){
                alert("Falha ao remover o Registro")
              }
          });
    }
    $(document).ready(function() {
      $('.select2').select2();
			CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $('[data-toggle=confirmation]').confirmation({
          rootSelector: '[data-toggle=confirmation]',
          title: 'Deseja remover o registro?',
          btnOkLabel:"Sim",
          btnCancelLabel:"Não"
          });
    });
  </script>
@stop
