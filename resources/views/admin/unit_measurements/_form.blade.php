<div class="form-group col-md-6 {{ $errors->has('unit_measurements.unit_measure_id') ? 'has-error' : '' }}">
    <label for="nome">Id Unidade de medida</label>
    <input type="text" 
            id="unit_measure_id" 
            class="form-control" 
            name="unit_measurements[unit_measure_id]" 
            value="{{ old('unit_measurements.unit_measure_id', isset($entity->unit_measure_id) ? $entity->unit_measure_id : '') }}">

    @if($errors->has('unit_measurements.unit_measure_id'))
        <span class="help-block">{{ $errors->first('unit_measurements.unit_measure_id') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('unit_measurements.description') ? 'has-error' : '' }}">
    <label for="description">Descrição</label>
    <input type="text" 
            id="description" 
            class="form-control" 
            name="unit_measurements[description]" 
            value="{{ old('unit_measurements.description', isset($entity->description) ? $entity->description : '') }}">

    @if($errors->has('unit_measurements.description'))
        <span class="help-block">{{ $errors->first('unit_measurements.description') }}</span>
    @endif
</div>