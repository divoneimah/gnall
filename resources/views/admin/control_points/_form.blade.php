<div class="form-group col-md-6 {{ $errors->has('control_points.name') ? 'has-error' : '' }}">
    <label for="nome">Usuário</label>
    <select 
            class="form-control js-data-ajax-provider" 
            name="control_points[user_id]" 
            value="{{ old('control_points.user_id', isset($entity->user_id) ? $entity->user_id : '') }}">
            @php
            if(isset($users) && $users){
                foreach($users as $p){
                    echo "<option value='". $p->id."' selected='selected'>". $p->name ."</option>";        
                }
            }
            @endphp 
            
    </select>

    @if($errors->has('control_points.name'))
        <span class="help-block">{{ $errors->first('control_points.name') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('control_points.latitude') ? 'has-error' : '' }}">
    <label for="latitude">Latitude</label>
    <input type="text" 
            id="latitude" 
            class="form-control" 
            name="control_points[latitude]" 
            value="{{ old('control_points.latitude', isset($entity->latitude) ? $entity->latitude : '') }}">

    @if($errors->has('control_points.latitude'))
        <span class="help-block">{{ $errors->first('control_points.latitude') }}</span>
    @endif
</div>

<div class="form-group col-md-6 {{ $errors->has('control_points.longitude') ? 'has-error' : '' }}">
    <label for="longitude">Longitude</label>
    <input type="text" 
            id="longitude" 
            class="form-control" 
            name="control_points[longitude]" 
            value="{{ old('control_points.longitude', isset($entity->longitude) ? $entity->longitude : '') }}">

    @if($errors->has('control_points.longitude'))
        <span class="help-block">{{ $errors->first('control_points.longitude') }}</span>
    @endif
</div>
<div class="form-group col-md-6 {{ $errors->has('control_points.note') ? 'has-error' : '' }}">
    <label for="note">Observação</label>
    <input type="text" 
            id="note" 
            class="form-control" 
            name="control_points[note]" 
            value="{{ old('control_points.note', isset($entity->note) ? $entity->note : '') }}">

    @if($errors->has('control_points.note'))
        <span class="help-block">{{ $errors->first('control_points.note') }}</span>
    @endif
</div>