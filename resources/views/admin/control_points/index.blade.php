@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Check-in')

@section('content_header')
    <h1>Check-in</h1>
@stop



@section('content')
<div class="col-md-12">
    <div class="box">

    @if ($message = Session::get('message'))
        <div class="alert alert-{{ $message['class'] }} alert-block">
            <button type="button" class="close" data-dismiss="{{ $message['class'] }}">×</button>
            <strong>{{ $message['message'] }}</strong>
        </div>
    @endif
        <!-- /.box-header -->
        <div class="box-body">
            <table cellspacing="5" cellpadding="5" border="0">
                <tbody><tr>
                    <td>Data inicial:</td>
                    <td><input type="text" class="form-control" id="initial_date" name="initial_date" placeholder="DD/MM/AAAA" value='{{\Carbon\Carbon::parse(strtotime("today"))->format('d/m/Y')}}'>
                        <span class="date-range help-block hide d-none">Data Inicial deve menor que a final</span></td>
                </tr>
                <tr>
                    <td>Data Final:</td>
                    <td><input type="text" class="form-control" id="final_date" name="final_date" placeholder="DD/MM/AAAA"  value='{{\Carbon\Carbon::parse(strtotime("today"))->format('d/m/Y')}}'>
                        <span class="date-range help-block hide d-none">Data Final deve maior que a inicial</span></td>
                    </td>
                </tr>
                <tr>
                    <td>Gestor:</td>
                    <td>
                        <select
                            class="form-control js-data-ajax-provider"
                            id="user_id"
                            name="user_id"
                        >
                            <option value=''>Selecione</option>
                            @if(isset($users) && $users)
                                @foreach($users as $e){
                                <option value='{{$e->id}}'>{{ $e->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </td>
                </tr>


                <tr>
                    <td><button type='button' id="filtrar" class="btn btn-primary">Filtrar</button></td>
                </tr>
                </tbody></table>
            <table id="table" class="table table-striped table-bordered table-hover">
                <thead><tr>
                    <th style="width: 10px">#</th>
                    <th>Gestor</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Observação</th>
                    <th>Data Criação</th>
                    <th>Data Atualização</th>
                    <th style="width: 40px">Ações</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <a class="btn btn-primary pull-right" href="{{route('admin.control_points.create')}}">Novo Registro</a>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/datatables/datatables.min.css">
    <link rel="stylesheet" href="/js/datatables/datatables.min.css">
    <link rel="stylesheet" href="{{url('')}}/js/bootstrap-datepicker/css/bootstrap-datepicker.standalone.css">
@stop

@section('js')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.print.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="/js/datatables/initialize.js"></script>

<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/dataTables.buttons.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.html5.min.js"></script>
<script src="/js/datatables/Buttons-1.6.5/js/buttons.print.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
<script src="/js/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
<script src="/js/datatables/JSZip-2.5.0/jszip.min.js"></script>
<script src="/js/datatables/initialize.js"></script>
<script src="/js/bootstrap-confirmation/bootstrap-confirmation.js"></script>
<script src="{{url('')}}/js/daterangepicker/moment.min.js"></script>
<script src="{{url('')}}/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script>
		var CSRF_TOKEN;
		var dataTable;
		$(document).ready(function() {

            CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $("#initial_date").datepicker({
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                monthNamesShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
                dayNames:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],
                dayNamesShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                dayNamesMin:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                format: "dd/mm/yyyy"
            });
            $("#final_date").datepicker({
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
                monthNamesShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
                dayNames:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],
                dayNamesShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                dayNamesMin:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
                format: "dd/mm/yyyy"
            })
            $("#filtrar").on('click', function(ev){
                validade_data();
            });
            var ajax_call = "{{ route('admin.control_points.index', ['return_json'=>1])}}";
            dataTable = $('#table').DataTable( {
                "columnDefs": [
                    {
                        targets: -4,
                        className: 'dt-body-right'
                    }
                ],
                "processing": true,
                "serverSide": true,
                "deferLoading": 0, // here
                "ajax": {
                    "url": ajax_call,
                    "data": function(d) {
                        d.initial_date  = $('#initial_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
                        d.final_date    = $('#final_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
                        d.user_id       = $('select[name="user_id"]').val();
                        return d;
                    },
                    dataSrc: function(d){
                        if(d.additional.totalSum !== undefined){
                            $("th.sumTotal").html(d.additional.totalSum);
                        }
                        return d.data;
                    }
                },
                dom: 'Bfrtip',
                buttons: [

                ],
                "pageLength": 10,
                "lengthMenu": [ 10 ],
                language:
                    {
                        "decimal":        "",
                        "emptyTable":     "Nenhum registro disponível",
                        "info":           "Mostrando _START_ até _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 para 0 de 0 registros",
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     "Mostrando _MENU_ registros",
                        "loadingRecords": "Carregando...",
                        "processing":     "Processing...",
                        "search":         "Buscar:",
                        "zeroRecords":    "Nenhum registro encontrado",
                        "paginate": {
                            "first":      "Primeira",
                            "last":       "Última",
                            "next":       "Próxima",
                            "previous":   "Anterior"
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
                    let total = 0.0;
                    for(var i=0;i<data.length;i++){
                        var value = data[i][8];
                        if(isNaN(value)){
                            total += parseFloat(value.replace(",","."));
                        }else{
                            total += value;
                        }
                    }
                    //$( api.column( 8 ).footer() ).html(total.toFixed(2));
                }

            } ).on( 'order.dt',  function () {
                $('[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    title: 'Deseja remover o registro?',
                    btnOkLabel:"Sim",
                    btnCancelLabel:"Não"
                });
            } );
		});

        function validade_data(){
            if($('#final_date').data('datepicker').getDate() == null){
                $('#final_date').data('datepicker').setDate(new Date());
            }
            if($('#initial_date').data('datepicker').getDate() == null){
                $('#initial_date').data('datepicker').setDate(new Date());
            }
            var initial_date  = moment($('#initial_date').data('datepicker').getFormattedDate('yyyymmdd'));
            var final_date    = moment($('#final_date').data('datepicker').getFormattedDate('yyyymmdd'));
            $(".date-range").addClass("d-none");
            if(final_date.diff(initial_date, "days")<0){
                // Initial Date is greater than final
                //$('#initial_date').val(final_date.format('DD/MM/YYYY'));
                $(".date-range").removeClass("d-none");
            }else if(initial_date.diff(final_date, "days")>0 ){
                //$('#final_date').val(initial_date.format('DD/MM/YYYY'));
                $(".date-range").removeClass("d-none");
            }
            //do something, like clearing an input
            dataTable.draw();
        }

		function confirmDelete(id)
		{
            $.ajax({
                url: "{{ route('admin.control_points.destroy','') }}/"+id,
                type: 'DELETE',
                data: {'_token':CSRF_TOKEN},
                success: function(response) {
                    if(response.success == true) {
                        var url = "{{ route('admin.control_points.index')}}";
                        window.location.href = url;
                    }else {
                        alert("Falha ao remover o Registro")
                    }
                },
                error: function(response){
                    alert("Falha ao remover o Registro")
                }
            });
		}


	</script>
@stop
