@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Centro de Custos')

@section('content_header')
    <h1>Novo</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="box box-success">
      <form action="{{ route('admin.cost_centers.store') }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          @include('admin.cost_centers._form')
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Criar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="/js/daterangepicker/daterangepicker.css">
@stop

@section('js')
    <script src="/js/daterangepicker/moment.min.js"></script>
    <script src="/js/daterangepicker/daterangepicker.js"></script>
    <script>
    $('#start_date').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD/MM/YYYY HH:mm A'
        }
    });
    $('#end_date').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD/MM/YYYY HH:mm A'
        }
    });
    </script>
@stop
