@extends('admin.dashboard.page')

@inject('layoutHelper', "\JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper")

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('title', config('adminlte.logo'). ' - Departamentos')

@section('content_header')
    <h1>Editar</h1>
@stop

@section('content')
  <div class="col-md-12">
    <div class="box box-success">
      <form action="{{ route('admin.departaments.update', $entity->id) }}" method="post">
        <div class="box-body">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="put">
          <input type="hidden" name="departaments[id]" value="{{$entity->id}}">
          @include('admin.departaments._form')
        </div>
        <div class="box-footer">
          <button class="btn btn-success">Atualizar</button>
        </div>
      </form>
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop
