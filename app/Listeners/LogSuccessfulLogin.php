<?php
namespace App\Listeners;



use App\Http\Requests\Admin\LoginAdminRequest;
use App\Repositories\General\UserLoginLogRepository;
use Illuminate\Auth\Events\Login;
//use Mail;

class LogSuccessfulLogin{
    public function __construct(LoginAdminRequest $request) {
        $this->request = $request;
        $this->repository = new UserLoginLogRepository();
    }

    public function handle(Login $event) {
        $user_login["user_login"]["user_id"] = $event->user->id;
        $user_login["user_login"]["type_access"] = (preg_match("/admin/",\Request::route()->getName())?"web":"app");
        $user_login["user_login"]["ip"] = $this->request->ip();
        $this->repository->create($user_login);

    }
}

