<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\Subsidiaries;
use Illuminate\Support\Facades\DB;

class SubsidiariesRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new Subsidiaries();
    }

    public function getModel()
    {
        $this->model                = new Subsidiaries();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['subsidiaries']['cnpj'] = isset($data['subsidiaries']['cnpj'])?$data['subsidiaries']['cnpj']:null;
            $data['subsidiaries']['cnpj'] = str_replace("/", "", $data['subsidiaries']['cnpj']);
            $data['subsidiaries']['cnpj'] = str_replace("-", "", $data['subsidiaries']['cnpj']);
            $data['subsidiaries']['cnpj'] = str_replace(".", "", $data['subsidiaries']['cnpj']);
            $data['subsidiaries']['active']       = (!isset($data['subsidiaries']['active']) || $data['subsidiaries']['active'] == null)?0:$data['subsidiaries']['active'];
            $data['subsidiaries']      = $this->setCreatedUpdatedUser($data['subsidiaries'], 1);
            
            $entity                     = $this->model->firstOrCreate($data['subsidiaries']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['subsidiaries']['cnpj'] = isset($data['subsidiaries']['cnpj'])?$data['subsidiaries']['cnpj']:null;
        $data['subsidiaries']['cnpj'] = str_replace("/", "", $data['subsidiaries']['cnpj']);
        $data['subsidiaries']['cnpj'] = str_replace("-", "", $data['subsidiaries']['cnpj']);
        $data['subsidiaries']['cnpj'] = str_replace(".", "", $data['subsidiaries']['cnpj']);
        $data['subsidiaries']['active']       = (!isset($data['subsidiaries']['active']) || $data['subsidiaries']['active'] == null)?0:$data['subsidiaries']['active'];
        $data['subsidiaries']       = $this->setCreatedUpdatedUser($data['subsidiaries'], 0);
       
        $object->fill($data['subsidiaries']);
        $object->save(); 

        return $object;
    }

}