<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\Departaments;
use Illuminate\Support\Facades\DB;

class DepartamentsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new Departaments();
    }

    public function getModel()
    {
        $this->model                = new Departaments();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['departaments']       = $this->setCreatedUpdatedUser($data['departaments'], 1);
            $entity                     = $this->model->firstOrCreate($data['departaments']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['departaments']       = $this->setCreatedUpdatedUser($data['departaments'], 0);
        $object->fill($data['departaments']);
        $object->save(); 

        return $object;
    }

}