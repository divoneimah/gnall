<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\Events;
use Illuminate\Support\Facades\DB;

class EventsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new Events();
    }

    public function getModel()
    {
        $this->model                = new Events();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['events']      = $this->setCreatedUpdatedUser($data['events'], 1);

            $entity                  = $this->model->firstOrCreate($data['events']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['events']      = $this->setCreatedUpdatedUser($data['events'], 0);
       
        $object->fill($data['events']);
        $object->save(); 

        return $object;
    }

}