<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\ControlPoints;
use Illuminate\Support\Facades\DB;

class ControlPointsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new ControlPoints();
    }

    public function getModel()
    {
        $this->model                = new ControlPoints();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['control_points']     = $this->setCreatedUpdatedUser($data['control_points'], 1);
            $entity                     = $this->model->firstOrCreate($data['control_points']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
       
        $object->fill($data['control_points']);
        $object->save(); 

        return $object;
    }

}