<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\Expenses;
use Illuminate\Support\Facades\DB;

class ExpensesRepository extends BaseRepository
{
    private $event_id = 57;

    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try {
            $model = $this->model->where(['id' => $id])->first();
            $model->delete();
        } catch (\Exception $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        } catch (\ErrorException $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model = new Expenses();
    }

    public function getModel()
    {
        $this->model = new Expenses();
    }

    public function findOrCreate($data = null)
    {
        $this->getModels();
        DB::beginTransaction();
        try {
            $data['expenses'] = $this->setCreatedUpdatedUser($data['expenses'], 1);
            $entity           = $this->model->firstOrCreate($data['expenses']);
            /*if ($data['expenses']['event_id'] == $this->event_id) {
                $data['expenses']['expense_id_from'] = $entity->id;
                $data['expenses']['event_id'] = 58;
                $data['expenses']                    = $this->setCreatedUpdatedUser($data['expenses'], 1);
                $entity                              = $this->model->firstOrCreate($data['expenses']);
            }*/
        } catch (\Exception $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if (!$object) {
            return null;
        }
        $data['expenses'] = $this->setCreatedUpdatedUser($data['expenses'], 0);

        $object->fill($data['expenses']);

        $object->save();

        return $object;
    }

}
