<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\ExpenseNatures;
use Illuminate\Support\Facades\DB;

class ExpenseNaturesRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new ExpenseNatures();
    }

    public function getModel()
    {
        $this->model                = new ExpenseNatures();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['expense_natures']      = $this->setCreatedUpdatedUser($data['expense_natures'], 1);

            $entity                  = $this->model->firstOrCreate($data['expense_natures']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['expense_natures']      = $this->setCreatedUpdatedUser($data['expense_natures'], 0);
       
        $object->fill($data['expense_natures']);
        $object->save(); 

        return $object;
    }

}