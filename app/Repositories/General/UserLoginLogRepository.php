<?php
namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\Admin\UserLoginLog;
use Illuminate\Support\Facades\DB;

class UserLoginLogRepository {

    public function __construct()
    {

    }

    public function create($data=null)
    {
        $this->getModels();
        DB::beginTransaction();

        try{

            $entity                 = $this->model->create($data["user_login"]);

        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $entity;
    }


    public function getModels()
    {
        $this->model                = new UserLoginLog();
    }

    public function getModel()
    {
        $this->model                = new UserLoginLog();
    }
}
