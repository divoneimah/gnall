<?php

namespace App\Repositories\General;


use App\Base\BaseRepository;
use App\Models\Admin\AdminUsers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AdminUsersRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            if(!isset($data['admin_users']['password'])){
                $data['admin_users']['password']    = Hash::make(123);
            }else{
                $data['admin_users']['password']    = Hash::make($data['admin_users']['password']);
            }
            unset($data['admin_users']['password_confirmation']);
            $entity                 = $this->model->firstOrCreate($data["admin_users"]);
            
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function delete($id)
    {
        if($id == 1){
            $this->_errors[] = "This entity could not be removed";
            return false;
        }
        $this->getModels();
        DB::beginTransaction();
        try{
            $model          = $this->model->find($id);
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new AdminUsers();
    }

    public function getModel()
    {
        $this->model                = new AdminUsers();
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        
        if(!isset($data['admin_users']['password'])){
            
        }else{
            $data['admin_users']['password']    = Hash::make($data['admin_users']['password']);
        }
        unset($data['admin_users']['password_confirmation']);

        $object->fill($data['admin_users']);
        $object->save(); 
        
        return $object;
    }

    public function update_password($_data){
        $this->getModels();

        $object_login = $this->model_login->where(['login' => $_data['email'], 'remember_token' => $_data['token']])->first();
        if($object_login){
            $object_login->password    = Hash::make($_data['password']);;
            $object_login->save();
        }else{
            return false;
        }
        return $object_login;
    }
}