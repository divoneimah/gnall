<?php

namespace App\Repositories\General;


use App\Base\BaseRepository;
use App\Models\General\Users;
use App\Models\General\UserSubsidiaries;
use App\Models\General\UserCostCenters;
use App\Models\General\UserPaymentForms;
use App\Models\General\UserAccessGroups;

use App\Models\General\UserPaymentFormsCenters;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        $this->getRepositories();
        DB::beginTransaction();
        try{
            $_cost_center_id = $_subsidiary_id = $access_group_module_id = null;
            if(!isset($data['users']['password'])){
                $data['users']['password']    = Hash::make(123);
            }else{
                $data['users']['password']    = Hash::make($data['users']['password']);
            }
            unset($data['users']['password_confirmation']);
            if(isset($data['users']['cost_center_id'])){
                $_cost_center_id = $data['users']['cost_center_id'];
                unset($data['users']['cost_center_id']);
            }
            if(isset($data['users']['subsidiary_id'])){
                $_subsidiary_id  = $data['users']['subsidiary_id'];
                unset($data['users']['subsidiary_id']);
            }
            if(isset($data['users']['access_group_module_id'])){
                $access_group_module_id  = $data['users']['access_group_module_id'];
                unset($data['users']['access_group_module_id']);
            }
            if(isset($data['user_payment_forms'])){
                $_user_payment_forms  = $data['user_payment_forms'];
                unset($data['user_payment_forms']);
            }
            $data['users']          = $this->setCreatedUpdatedUser($data['users'], 1);
            $entity                 = $this->model->firstOrCreate($data["users"]);
            if($_cost_center_id != null && is_array($_cost_center_id)){
                $this->repo_cost_centers->deleteByUserId($entity->id);
                foreach($_cost_center_id as $c){
                    $this->model_cost_center->firstOrCreate(['user_id' => $entity->id, 'cost_center_id'=> $c]);
                }
            }
            if($_subsidiary_id != null && is_array($_subsidiary_id)){
                $this->repo_subsidiaries->deleteByUserId($entity->id);
                foreach($_subsidiary_id as $s){
                    $this->model_subsidiary->firstOrCreate(['user_id' => $entity->id, 'subsidiary_id'=> $s]);
                }
            }
            if($_user_payment_forms != null && is_array($_user_payment_forms)){
                foreach($_user_payment_forms as $key => $upf){
                    $_datapf = null;
                    if(!isset($upf['payment_form_id'])) continue;
                    $_datapf = ['user_id' => $entity->id, 'payment_form_id' => $upf['payment_form_id'], 'card_number' => isset($upf['card_number'])?$upf['card_number']:null, 'card_limit' => isset($upf['card_limit'])?$upf['card_limit']:null];
                    $this->model_payment_forms->firstOrCreate($_datapf);
                }
            }
            if($access_group_module_id != null){
                $this->model_access_group->firstOrCreate(['user_id' => $entity->id, 'access_group_module_id'=> $access_group_module_id]);
            }
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function delete($id)
    {
        $this->getModels();
        $this->getRepositories();

        DB::beginTransaction();
        try{
            $model          = $this->model->find($id);
            $this->repo_cost_centers->deleteByUserId($id);
            $this->repo_subsidiaries->deleteByUserId($id);
            
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new Users();
        $this->model_cost_center    = new UserCostCenters();
        $this->model_subsidiary     = new UserSubsidiaries();
        $this->model_payment_forms  = new UserPaymentForms();
        $this->model_access_group   = new UserAccessGroups();
    }

    public function getModel()
    {
        $this->model = new Users();
    }

    public function getRepositories()
    {
        $this->repo_cost_centers    = new UserCostCentersRepository();
        $this->repo_subsidiaries    = new UserSubsidiariesRepository();
        $this->repo_payment_forms   = new UserPaymentFormsRepository();
        $this->repo_access_group    = new UserAccessGroupsRepository();
    }

    public function update($id, $data)
    {
        $this->getModels();
        $this->getRepositories();
        $_cost_center_id = $_subsidiary_id = $access_group_module_id = null;

        $entity = $this->model->find($id);
        if(!$entity){
            return null;
        }
        
        if(!isset($data['users']['password'])){
            
        }else{
            $data['users']['password']    = Hash::make($data['users']['password']);
        }
        unset($data['users']['password_confirmation']);
        if(isset($data['users']['cost_center_id'])){
            $_cost_center_id = $data['users']['cost_center_id'];
            unset($data['users']['cost_center_id']);
        }
        if(isset($data['users']['subsidiary_id'])){
            $_subsidiary_id  = $data['users']['subsidiary_id'];
            unset($data['users']['subsidiary_id']);
        }
        if(isset($data['users']['access_group_module_id'])){
            $access_group_module_id  = $data['users']['access_group_module_id'];
            unset($data['users']['access_group_module_id']);
        }
        $data['users']          = $this->setCreatedUpdatedUser($data['users'], 0);

        $entity->fill($data['users']);
        $entity->save(); 
        $this->repo_cost_centers->deleteByUserId($entity->id);
        $this->repo_subsidiaries->deleteByUserId($entity->id);
        $this->repo_access_group->deleteByUserId($entity->id);
        
        if($_cost_center_id != null && is_array($_cost_center_id)){
            foreach($_cost_center_id as $c){
                $this->model_cost_center->firstOrCreate(['user_id' => $entity->id, 'cost_center_id'=> $c]);
            }
        }
        if($_subsidiary_id != null && is_array($_subsidiary_id)){
            foreach($_subsidiary_id as $s){
                $this->model_subsidiary->firstOrCreate(['user_id' => $entity->id, 'subsidiary_id'=> $s]);
            }
        }

        if($access_group_module_id != null){
            $this->model_access_group->firstOrCreate(['user_id' => $entity->id, 'access_group_module_id'=> $access_group_module_id]);
        }

        return $entity;
    }

    public function update_password($_data){
        $this->getModels();

        $object_login = $this->model_login->where(['login' => $_data['email'], 'remember_token' => $_data['token']])->first();
        if($object_login){
            $object_login->password    = Hash::make($_data['password']);;
            $object_login->save();
        }else{
            return false;
        }
        return $object_login;
    }
}