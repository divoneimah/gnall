<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\ExpenseReceipts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class ExpenseReceiptsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
            $this->remove_images($model);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new ExpenseReceipts();
    }

    public function getModel()
    {
        $this->model                = new ExpenseReceipts();
    }

    public function findOrCreate($data=null, $request=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $image                          = isset($data['expense_receipts']['image'])?$data['expense_receipts']['image']:null;
            unset($data['expense_receipts']['image']);
            $data['expense_receipts']       = $this->setCreatedUpdatedUser($data['expense_receipts'], 1);
            $entity                         = $this->model->firstOrCreate($data['expense_receipts']);
            if($image != null && $image->isValid()){
                $data['id']         = $entity->id;
                $entity             = $this->upload_image($data, $request);
            }
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();

        return $entity;
    }

    public function update($id, $data, $request=null)
    {
        
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $image                  = isset($data['expense_receipts']['image'])?$data['expense_receipts']['image']:null;
        unset($data['expense_receipts']['image']);

        $data['expense_receipts']      = $this->setCreatedUpdatedUser($data['expense_receipts'], 0);

        $object->fill($data['expense_receipts']);
        $object->save(); 
        if($image != null && $image->isValid()){
            $data['id']         = $object->id;
            $this->remove_images($object);
            $object             = $this->upload_image($data, $request);
        }
        
        return $object;
    }

    private function remove_images($model=null){
        if($model){
            $path_parts = pathinfo($model->server_path);
            Storage::delete(str_replace("storage","public", $model->server_path));
            return true;
        }
        return false;
        
    }

    public function upload_image($data, $request){
        $model_id   = isset($data['id'])?$data['id']:-1;
        $object     = $this->model->firstWhere('id', $model_id);
        if(!$object){
            $object = new ExpenseReceipts();
        }
        
        if($request->hasFile('expense_receipts.image') && $request->file('expense_receipts.image')->isValid()){
            $file                   = $request->file('expense_receipts.image');
            // Return original name of the file
            $path_parts             = pathinfo($file->getClientOriginalName());
            $original_name          = $path_parts['filename'];
            $uniqid                 = uniqid(date('HisYmd'));
            $file_name              = 'b_'.$uniqid.'_'. Str::slug($original_name, "_") . ".". strtolower($path_parts['extension']);
            $path_name              = ExpenseReceipts::$EXPENSE_RECEIPTS_FILE_DIRECTORY;
            $full_path              = Storage::url($path_name ."/". $file_name);
            $upload                 = $file->storeAs($path_name, $file_name);

            // open an image file
            $img = Image::make($file->getRealPath());
            $this->remove_images($object);
            $object->server_path            = $full_path;
            $object->save();
            
            return $object;
        }
        return false;
    }

}