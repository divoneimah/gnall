<?php

namespace App\Repositories\General;


use App\Base\BaseRepository;
use App\Models\General\UserAccessGroups;

use Illuminate\Support\Facades\DB;

class UserAccessGroupsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        $this->getRepositories();
        DB::beginTransaction();
        try{
            $data['users']      = $this->setCreatedUpdatedUser($data['users'], 1);

            $entity                 = $this->model->firstOrCreate($data["users"]);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $entity;
    }

    public function delete($id)
    {
        $this->getModels();
        $this->getRepositories();

        DB::beginTransaction();
        try{
            $model          = $this->model->find($id);
            $this->repo_cost_centers->deleteByUserId($id);
            $this->repo_subsidiaries->deleteByUserId($id);
            
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }


    public function deleteByUserId($user_id = null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['user_id' => $user_id])->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new UserAccessGroups();
    }

    public function getModel()
    {
        $this->model                = new UserAccessGroups();
    }

    public function getRepositories()
    {
    }

    public function update($id, $data)
    {
        $this->getModels();
        $this->getRepositories();
        $_cost_center_id = $_subsidiary_id = null;

        $entity = $this->model->find($id);
        if(!$entity){
            return null;
        }
        $data['users']      = $this->setCreatedUpdatedUser($data['users'], 0);
        
        $entity->fill($data['users']);
        $entity->save(); 

        return $entity;
    }
}