<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\Transfer;
use Illuminate\Support\Facades\DB;

class TransferRepository extends BaseRepository
{

    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        //dd($id);die();
        DB::beginTransaction();
        try {
            $model = $this->model->where(['id' => $id])->first();
            $model->delete();
        } catch (\Exception $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        } catch (\ErrorException $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model = new Transfer();
    }

    public function getModel()
    {
        $this->model = new Transfer();
    }

    public function findOrCreate($data = null)
    {
        $this->getModels();
        DB::beginTransaction();
        try {
            $data['transfer'] = $this->setCreatedUpdatedUser($data['transfer'], 1);
            $entity           = $this->model->firstOrCreate($data['transfer']);

        } catch (\Exception $e) {
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if (!$object) {
            return null;
        }
        $data['transfer'] = $this->setCreatedUpdatedUser($data['transfer'], 0);

        $object->fill($data['transfer']);

        $object->save();

        return $object;
    }

}
