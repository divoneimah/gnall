<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\UserSubsidiaries;
use Illuminate\Support\Facades\DB;

class UserSubsidiariesRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function deleteByUserId($user_id = null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['user_id' => $user_id])->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new UserSubsidiaries();
    }

    public function getModel()
    {
        $this->model                = new UserSubsidiaries();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['user_subsidiaries']          = $this->setCreatedUpdatedUser($data['user_subsidiaries'], 1);

            $entity                         = $this->model->firstOrCreate($data['user_subsidiaries']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['user_subsidiaries']          = $this->setCreatedUpdatedUser($data['user_subsidiaries'], 0);
       
        $object->fill($data['user_subsidiaries']);
        $object->save(); 

        return $object;
    }

}