<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\PaymentForms;
use Illuminate\Support\Facades\DB;

class PaymentFormsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new PaymentForms();
    }

    public function getModel()
    {
        $this->model                = new PaymentForms();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['payment_forms']      = $this->setCreatedUpdatedUser($data['payment_forms'], 1);

            $entity                  = $this->model->firstOrCreate($data['payment_forms']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['payment_forms']      = $this->setCreatedUpdatedUser($data['payment_forms'], 0);
       
        $object->fill($data['payment_forms']);
        $object->save(); 

        return $object;
    }

}