<?php

namespace App\Repositories\General;

use App\Base\BaseRepository;
use App\Models\General\CostCenters;
use Illuminate\Support\Facades\DB;

class CostCentersRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $model  = $this->model->where(['id' => $id])->first();
            $model->delete();
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }catch(\ErrorException $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        DB::commit();
        return $model;
    }

    public function getModels()
    {
        $this->model                = new CostCenters();
    }

    public function getModel()
    {
        $this->model                = new CostCenters();
    }

    public function findOrCreate($data=null)
    {
        $this->getModels();
        DB::beginTransaction();
        try{
            $data['cost_centers']       = $this->setCreatedUpdatedUser($data['cost_centers'], 1);
            $entity                     = $this->model->firstOrCreate($data['cost_centers']);
        }catch(\Exception $e){
            $this->_errors[] = $e->getMessage();
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $entity;
    }

    public function update($id, $data)
    {
        $this->getModels();
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $data['cost_centers']       = $this->setCreatedUpdatedUser($data['cost_centers'], 0);
        $object->fill($data['cost_centers']);
        $object->save(); 

        return $object;
    }

}