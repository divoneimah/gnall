<?php
use App\Models\General\Users;
use App\Models\General\SystemModules;

if (!function_exists('numbr')) {
 
    function numbr($num, $decimal = 2) {
        if(!is_numeric($num)) return 0;
    
        return number_format($num, $decimal, ',' ,'.');
    }
}
if(! function_exists('toDateBr'))
{
    function toDateBr($date=null, $fulldatetime=true)
    {
        $datebr = new DateTime($date);
        if($fulldatetime){
            return $datebr->format('d/m/Y H:i:s');
        }
        return $datebr->format('d/m/Y');
    }
}

if(! function_exists('securityAccess'))
{
    
    function securityAccess($location=null, $force_redirect=0)
    {
        $user_id            = Auth::user()->id;
        $user               = Auth::user();
        $entity             = Users::find($user_id);
        if(isset($entity->userAccessGroups->accessGroupModules->system_modules)){
            $_data = json_decode($entity->userAccessGroups->accessGroupModules->system_modules, true);
            if(isset($_data['data']) && is_array($_data['data'])){
                if(in_array($location, $_data['data'])){
                    return true;
                }
            }
        }
        if($force_redirect){
            $url = route('admin.dashboard');
            header("Location: ".$url);
            exit();
        }
        return false;
    }
}