<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Http\Requests\Api\UserSubsidiariesStoreRequest;
use App\Http\Resources\UserSubsidiariesCollection;
use App\Http\Resources\UserSubsidiariesResource;
use App\Models\General\UserSubsidiaries;
use App\Repositories\General\UserSubsidiariesRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group UserSubsidiaries
 *
 * APIs for managing entitys
 */
class UserSubsidiariesController extends BaseController
{
    public function __construct()
    {
        $this->repository = new UserSubsidiariesRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');
        $where = array();
        foreach ($params  as $field => $values){
            if(!preg_match("/page/",$field)) {
                if ($field == 'user_id') {
                    $where[] = [$field, '=', $values];
                }
            }
        }
        $this->per_page = (int)(isset($params['per_page']) ? $params['per_page'] : $this->per_page);
        $entities   = $this->repository->where($where)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new UserSubsidiariesCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserSubsidiariesStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = (new UserSubsidiariesRepository())->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new UserSubsidiariesResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([], 'Failed to create entity');

    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\UserSubsidiaries  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(UserSubsidiaries $user_subsidiaries)
    {
        return $this->sendResponse(new UserSubsidiariesResource($user_subsidiaries), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\UserSubsidiaries  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UserSubsidiariesStoreRequest $request, UserSubsidiaries $user_subsidiaries)
    {
        $data   = $request->validated();
        $entity = (new UserSubsidiariesRepository())->update($user_subsidiaries->id, $data);

        return $this->sendResponse(new UserSubsidiariesResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\UserSubsidiaries  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSubsidiaries $user_subsidiaries)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($user_subsidiaries->id);
        }catch(\Exception $e){

        }
        if($entity)
            return $this->sendResponse([], 'Entity deleted successfully.');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
