<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\Expenses;
use App\Http\Resources\ExpensesResource as ExpensesResource;
use App\Http\Requests\Api\ExpensesStoreRequest;
use App\Http\Requests\Api\ExpensesUpdateRequest;
use App\Repositories\General\ExpensesRepository;
use App\Http\Resources\ExpensesCollection;
use Illuminate\Http\Request;

/**
 * @group Web - Expenses
 *
 * APIs for managing entities
 */
class ExpensesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository = (new ExpensesRepository());
        if ($req->input("per_page")) {
            $this->per_page = (int)$req->input("per_page");
        }
        if ($req->input("start")) {
            $this->start_page = (int)$req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params   = $request->except('_token');
        $entities = $this->repository->filter($params)->with(['userPaymentForms'])->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new ExpensesCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpensesStoreRequest $request)
    {
        $data = $request->validated();
        //print_r($data);die();

        $where[] = ["user_id","=",$data['expenses']['user_id']];
        $where[] = ["internal_app_id","=",$data['expenses']['internal_app_id']];
        $find = $this->repository->where($where);
        if($find->count()==0) {
            $entity = $this->repository->findOrCreate($data);
            if ($entity) {
                $entity = $this->repository->with(['userPaymentForms'])->find($entity->id);
                return $this->sendResponse(new ExpensesResource($entity), 'Entity created successfully.');
            }
        }elseif($find->count()==1){

            $find = $find->get();
            $expenses = new \stdClass();
            foreach ($find as $e){
                $expenses->id = $e->id;
            }
            $data['expenses']['id'] = $expenses->id;
            $entity = $this->repository->update($expenses->id, $data);
            return $this->sendResponse(new ExpensesResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function show(Expenses $expenses)
    {
        return $this->sendResponse(new ExpensesResource($expenses), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function update(ExpensesUpdateRequest $request, Expenses $expenses)
    {
        $data   = $request->validated();
        $entity = $this->repository->update($expenses->id, $data);
        $entity = $this->repository->with(['userPaymentForms'])->find($expenses->id);

        return $this->sendResponse(new ExpensesResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expenses $expenses)
    {
        $return = ($this->repository)->delete($expenses->id);

        if ($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
