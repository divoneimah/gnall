<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Http\Requests\Api\AdminUsersStoreRequest;
use App\Http\Resources\AdminUsersCollection;
use App\Http\Resources\AdminUsersResource;
use App\Models\Admin\AdminUsers;
use App\Repositories\General\AdminUsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group AdminUsers
 *
 * APIs for managing entitys
 */
class AdminUsersController extends BaseController
{
    public function __construct()
    {
        $this->repository = new AdminUsersRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new AdminUsersCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUsersStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = (new AdminUsersRepository())->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new AdminUsersResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([], 'Failed to create entity');
        
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(AdminUsers $admin_users)
    {
        return $this->sendResponse(new AdminUsersResource($admin_users), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUsersStoreRequest $request, AdminUsers $admin_users)
    {
        $data   = $request->validated();  
        $entity = (new AdminUsersRepository())->update($admin_users->id, $data);

        return $this->sendResponse(new AdminUsersResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminUsers $admin_users)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($admin_users->id);
        }catch(\Exception $e){

        }
        if($entity)
            return $this->sendResponse([], 'Entity deleted successfully.');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
