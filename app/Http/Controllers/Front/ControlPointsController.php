<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\ControlPoints;
use App\Http\Resources\ControlPointsResource as ControlPointsResource;
use App\Http\Requests\Api\ControlPointsStoreRequest;
use App\Http\Requests\Api\ControlPointsUpdateRequest;
use App\Repositories\General\ControlPointsRepository;
use App\Http\Resources\ControlPointsCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @group Web - ControlPoints
 *
 * APIs for managing entities
 */
class ControlPointsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new ControlPointsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new ControlPointsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ControlPointsStoreRequest $request)
    {
        $data       = $request->validated(); 
        $user_id    = Auth::user()->id;
        $data['control_points']['created_user_id'] = $user_id;   
        $data['control_points']['updated_user_id'] = $user_id;   
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new ControlPointsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function show(ControlPoints $control_points)
    {
        return $this->sendResponse(new ControlPointsResource($control_points), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function update(ControlPointsUpdateRequest $request, ControlPoints $control_points)
    {
        $data       = $request->validated(); 
        $user_id    = Auth::user()->id;
        $data['control_points']['updated_user_id'] = $user_id;   
        
        $entity         = $this->repository->update($control_points->id, $data);
        if($entity){
            return $this->sendResponse(new ControlPointsResource($entity), 'Entity updated successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function destroy(ControlPoints $control_points)
    {
        $return = ($this->repository)->delete($control_points->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
