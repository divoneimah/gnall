<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\ExpenseNatures;
use App\Http\Resources\ExpenseNaturesResource as ExpenseNaturesResource;
use App\Http\Requests\Api\ExpenseNaturesStoreRequest;
use App\Http\Requests\Api\ExpenseNaturesUpdateRequest;
use App\Repositories\General\ExpenseNaturesRepository;
use App\Http\Resources\ExpenseNaturesCollection;
use Illuminate\Http\Request;

/**
 * @group Web - ExpenseNatures
 *
 * APIs for managing entities
 */
class ExpenseNaturesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new ExpenseNaturesRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new ExpenseNaturesCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseNaturesStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new ExpenseNaturesResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\ExpenseNatures  $expense_natures
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseNatures $expense_natures)
    {
        return $this->sendResponse(new ExpenseNaturesResource($expense_natures), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\ExpenseNatures  $expense_natures
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseNaturesUpdateRequest $request, ExpenseNatures $expense_natures)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($expense_natures->id, $data);

        return $this->sendResponse(new ExpenseNaturesResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\ExpenseNatures  $expense_natures
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseNatures $expense_natures)
    {
        $return = ($this->repository)->delete($expense_natures->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
