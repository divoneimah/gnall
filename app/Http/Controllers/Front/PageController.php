<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use Illuminate\Http\Request;

/**
 * @group Web - Expenses
 *
 * APIs for managing entities
 */
class PageController extends BaseController
{
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function termo_privacidade(Request $request)
    {
        return view('front/page/termo_privacidade');
    }

    
}
