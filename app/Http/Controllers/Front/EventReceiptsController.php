<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\EventReceipts;
use App\Http\Resources\EventReceiptsResource as EventReceiptsResource;
use App\Http\Requests\Api\EventReceiptsStoreRequest;
use App\Http\Requests\Api\EventReceiptsUpdateRequest;
use App\Repositories\General\EventReceiptsRepository;
use App\Http\Resources\EventReceiptsCollection;
use Illuminate\Http\Request;

/**
 * @group Web - EventReceipts
 *
 * APIs for managing entities
 */
class EventReceiptsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new EventReceiptsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new EventReceiptsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventReceiptsStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new EventReceiptsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\EventReceipts  $event_receipts
     * @return \Illuminate\Http\Response
     */
    public function show(EventReceipts $event_receipts)
    {
        return $this->sendResponse(new EventReceiptsResource($event_receipts), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\EventReceipts  $event_receipts
     * @return \Illuminate\Http\Response
     */
    public function update(EventReceiptsUpdateRequest $request, EventReceipts $event_receipts)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($event_receipts->id, $data);

        return $this->sendResponse(new EventReceiptsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\EventReceipts  $event_receipts
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventReceipts $event_receipts)
    {
        $return = ($this->repository)->delete($event_receipts->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
