<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Http\Requests\Api\UsersStoreRequest;
use App\Http\Resources\UsersCollection;
use App\Http\Resources\UsersResource;
use App\Models\General\Users;
use App\Repositories\General\UsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group Users
 *
 * APIs for managing entitys
 */
class UsersController extends BaseController
{
    public function __construct()
    {
        $this->repository = new UsersRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new UsersCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new UsersResource($entity), 'Entity created successfully.');
        }
        return $this->sendError($this->repository->getErrors(), 'Failed to create entity');
        
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        return $this->sendResponse(new UsersResource($users), 'Registro Resgatado com sucesso');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function show_lean()
    {
        $user_id    = Auth::user()->id;
        $entity     = $this->repository->find($user_id);
        return $this->sendResponse(["name" => $entity->name, "email" => $entity->email, "id" => $entity->id], 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UsersStoreRequest $request, Users $users)
    {
        $data   = $request->validated();  
        $entity = (new UsersRepository())->update($users->id, $data);

        return $this->sendResponse(new UsersResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($users->id);
        }catch(\Exception $e){

        }
        if($entity)
            return $this->sendResponse([], 'Entity deleted successfully.');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
