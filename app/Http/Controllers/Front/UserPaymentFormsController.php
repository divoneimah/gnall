<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Http\Requests\Api\UserPaymentFormsStoreRequest;
use App\Http\Resources\UserPaymentFormsCollection;
use App\Http\Resources\UserPaymentFormsResource;
use App\Models\General\UserPaymentForms;
use App\Repositories\General\UserPaymentFormsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group UserPaymentForms
 *
 * APIs for managing entitys
 */
class UserPaymentFormsController extends BaseController
{
    public function __construct()
    {
        $this->repository = new UserPaymentFormsRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');
        $where = array();
        foreach ($params  as $field => $values){
            if(!preg_match("/page/",$field)) {
                if ($field == 'user_id') {
                    $where[] = [$field, '=', $values];
                }
            }
        }
        $this->per_page = (int)(isset($params['per_page']) ? $params['per_page'] : $this->per_page);
        $entities   = $this->repository->where($where)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new UserPaymentFormsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPaymentFormsStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = ($this->repository)->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new UserPaymentFormsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError($this->repository->getErrors(), 'Failed to create entity');

    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(UserPaymentForms $user_payment_forms)
    {
        return $this->sendResponse(new UserPaymentFormsResource($user_payment_forms), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UserPaymentFormsStoreRequest $request, UserPaymentForms $user_payment_forms)
    {
        $data   = $request->validated();
        $entity = (new UserPaymentFormsRepository())->update($user_payment_forms->id, $data);

        return $this->sendResponse(new UserPaymentFormsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPaymentForms $user_payment_forms)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($user_payment_forms->id);
        }catch(\Exception $e){

        }
        if($entity)
            return $this->sendResponse([], 'Entity deleted successfully.');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
