<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\PaymentForms;
use App\Http\Resources\PaymentFormsResource as PaymentFormsResource;
use App\Http\Requests\Api\PaymentFormsStoreRequest;
use App\Http\Requests\Api\PaymentFormsUpdateRequest;
use App\Repositories\General\PaymentFormsRepository;
use App\Http\Resources\PaymentFormsCollection;
use Illuminate\Http\Request;

/**
 * @group Web - PaymentForms
 *
 * APIs for managing entities
 */
class PaymentFormsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new PaymentFormsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new PaymentFormsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentFormsStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new PaymentFormsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentForms $payment_forms)
    {
        return $this->sendResponse(new PaymentFormsResource($payment_forms), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentFormsUpdateRequest $request, PaymentForms $payment_forms)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($payment_forms->id, $data);

        return $this->sendResponse(new PaymentFormsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentForms $payment_forms)
    {
        $return = ($this->repository)->delete($payment_forms->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
