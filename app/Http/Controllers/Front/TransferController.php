<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\Transfer;
use App\Http\Resources\TransferResource as TransferResource;
use App\Http\Requests\Api\TransferStoreRequest;
use App\Http\Requests\Api\TransferUpdateRequest;
use App\Repositories\General\TransferRepository;
use App\Http\Resources\TransferCollection;
use Illuminate\Http\Request;

/**
 * @group Web - Tranfer
 *
 * APIs for managing entities
 */
class TransferController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository = (new TransferRepository());
        if ($req->input("per_page")) {
            $this->per_page = (int)$req->input("per_page");
        }
        if ($req->input("start")) {
            $this->start_page = (int)$req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params   = $request->except('_token');
        $entities = $this->repository->filter($params)->with(['users'])->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new TransferCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransferStoreRequest $request)
    {
        $data = $request->validated();
        #dd($data);die();
        $entity = $this->repository->findOrCreate($data);

        if ($entity) {
            $entity = $this->repository->find($entity->id);
            return $this->sendResponse(new \Illuminate\Http\Resources\Json\JsonResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param \App\Models\General\Transfer $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Tranfer $transfer)
    {
        return $this->sendResponse(new TransferResource($transfer), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\General\Tranfer $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(TransferUpdateRequest $request, Transfer $transfer)
    {
        $data   = $request->validated();
        $entity = $this->repository->update($transfer->id, $data);
        $entity = $this->repository->with(['users'])->find($transfer->id);

        return $this->sendResponse(new TransferResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param \App\Models\General\Tranfer $tranfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer, Request $request)
    {


        $data['transfer']['deleted_user_id'] = $request->user()->id;
        $dados                               = $this->repository->update($transfer->id, $data);

        $return = ($this->repository)->delete($transfer->id);
        if ($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
