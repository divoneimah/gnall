<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\Subsidiaries;
use App\Http\Resources\SubsidiariesResource as SubsidiariesResource;
use App\Http\Requests\Api\SubsidiariesStoreRequest;
use App\Http\Requests\Api\SubsidiariesUpdateRequest;
use App\Repositories\General\SubsidiariesRepository;
use App\Http\Resources\SubsidiariesCollection;
use Illuminate\Http\Request;

/**
 * @group Web - Subsidiaries
 *
 * APIs for managing entities
 */
class SubsidiariesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new SubsidiariesRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new SubsidiariesCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubsidiariesStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new SubsidiariesResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function show(Subsidiaries $subsidiaries)
    {
        return $this->sendResponse(new SubsidiariesResource($subsidiaries), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function update(SubsidiariesUpdateRequest $request, Subsidiaries $subsidiaries)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($subsidiaries->id, $data);

        return $this->sendResponse(new SubsidiariesResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subsidiaries $subsidiaries)
    {
        $return = ($this->repository)->delete($subsidiaries->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
