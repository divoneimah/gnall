<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\UnitMeasurements;
use App\Http\Resources\UnitMeasurementsResource as UnitMeasurementsResource;
use App\Http\Requests\Api\UnitMeasurementsStoreRequest;
use App\Http\Requests\Api\UnitMeasurementsUpdateRequest;
use App\Repositories\General\UnitMeasurementsRepository;
use App\Http\Resources\UnitMeasurementsCollection;
use Illuminate\Http\Request;

/**
 * @group Web - UnitMeasurements
 *
 * APIs for managing entities
 */
class UnitMeasurementsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new UnitMeasurementsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new UnitMeasurementsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitMeasurementsStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new UnitMeasurementsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\UnitMeasurements  $unit_measurements
     * @return \Illuminate\Http\Response
     */
    public function show(UnitMeasurements $unit_measurements)
    {
        return $this->sendResponse(new UnitMeasurementsResource($unit_measurements), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\UnitMeasurements  $unit_measurements
     * @return \Illuminate\Http\Response
     */
    public function update(UnitMeasurementsUpdateRequest $request, UnitMeasurements $unit_measurements)
    {
        $data           = $request->validated(); 
        $entity         = $this->repository->update($unit_measurements->id, $data);

        return $this->sendResponse(new UnitMeasurementsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\UnitMeasurements  $unit_measurements
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitMeasurements $unit_measurements)
    {
        $return = ($this->repository)->delete($unit_measurements->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
