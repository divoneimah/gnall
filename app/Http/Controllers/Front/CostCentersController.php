<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\CostCenters;
use App\Http\Resources\CostCentersResource as CostCentersResource;
use App\Http\Requests\Api\CostCentersStoreRequest;
use App\Http\Requests\Api\CostCentersUpdateRequest;
use App\Repositories\General\CostCentersRepository;
use App\Http\Resources\CostCentersCollection;
use Illuminate\Http\Request;

/**
 * @group Web - CostCenters
 *
 * APIs for managing entities
 */
class CostCentersController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new CostCentersRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new CostCentersCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostCentersStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new CostCentersResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenters $cost_centers)
    {
        return $this->sendResponse(new CostCentersResource($cost_centers), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function update(CostCentersUpdateRequest $request, CostCenters $cost_centers)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($cost_centers->id, $data);

        return $this->sendResponse(new CostCentersResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostCenters $cost_centers)
    {
        $return = ($this->repository)->delete($cost_centers->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
