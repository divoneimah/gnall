<?php

namespace App\Http\Controllers\Front;

use App\Base\BaseController;
use App\Models\General\EventExpenseNatures;
use App\Http\Resources\EventExpenseNaturesResource as EventExpenseNaturesResource;
use App\Http\Requests\Api\EventExpenseNaturesStoreRequest;
use App\Http\Requests\Api\EventExpenseNaturesUpdateRequest;
use App\Repositories\General\EventExpenseNaturesRepository;
use App\Http\Resources\EventExpenseNaturesCollection;
use Illuminate\Http\Request;

/**
 * @group Web - EventExpenseNatures
 *
 * APIs for managing entities
 */
class EventExpenseNaturesController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new EventExpenseNaturesRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->withTrashed()->paginate($this->per_page);
        return $this->sendResponse(new EventExpenseNaturesCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventExpenseNaturesStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new EventExpenseNaturesResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\EventExpenseNatures  $event_expense_natures
     * @return \Illuminate\Http\Response
     */
    public function show(EventExpenseNatures $event_expense_natures)
    {
        return $this->sendResponse(new EventExpenseNaturesResource($event_expense_natures), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\EventExpenseNatures  $event_expense_natures
     * @return \Illuminate\Http\Response
     */
    public function update(EventExpenseNaturesUpdateRequest $request, EventExpenseNatures $event_expense_natures)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($event_expense_natures->id, $data);

        return $this->sendResponse(new EventExpenseNaturesResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\EventExpenseNatures  $event_expense_natures
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventExpenseNatures $event_expense_natures)
    {
        $return = ($this->repository)->delete($event_expense_natures->id);

        if($return)
            return $this->sendResponse([], 'Registro removido com sucesso');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
