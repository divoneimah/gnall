<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Base\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Requests\Api\LoginUsersRequest;
use App\Models\General\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use Carbon\Carbon;

use Illuminate\Support\Str;

class LoginUsersController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'login_json']);
    }

    public function login_json(LoginUsersRequest $request)
    {
        $_data  = $request->validated();
        
        $success = false;
        $status  = 401;
        $data = $request->all();

        // Get user by email
        $entity = Users::where('email', $_data['login']['email'])->first();

        // Validate Company
        if(!$entity) {
            return $this->sendError([], 'Invalid credentials');
        }

        // Validate Password
        if (!Hash::check($_data['login']['password'], $entity->password)) {
            return $this->sendError([], 'Invalid credentials');
        }

        $_token_data = $this->generate_token($entity);

        return $this->sendResponse(['result' =>$_token_data], 'Logged Users.');

    }

    private function generate_token($entity = null){
        $token      = Str::random(60);
        $token      = $entity->createToken($token);
        $api_token  = $token->accessToken;

        //$api_token = hash('sha256', $token);
        $entity->api_token = $api_token;
        $entity->save();
        $expire = $token->token->expires_at->diffInSeconds(Carbon::now());
        return [
            'access_token' => $api_token,
            'token_type' => 'bearer',
            'id' => $entity->id,
            'expires_in' => $token->token->expires_at,
            'name' => $entity->name
        ];
    }
    
    public function logout(){
        try{
            $user = Auth::user();
            $user->api_token = null;
            $user->save();
        }catch (Exception $e){
            $message = trans('auth.token_inactive');
            return $this->sendResponse(false, 'Not logged out.');;
        }
        return $this->sendResponse(true, 'Logged out.');;
    }
}
