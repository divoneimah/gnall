<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Base\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Requests\Admin\LoginAdminRequest;
use App\Models\Admin\AdminUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

use Illuminate\Support\Str;

class LoginAdminController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'index', 'login_json']);
    }

    public function index()
    {
        $errors = [];
        return view('admin/login/form_login', ['errors']);
    }

    public function login(LoginAdminRequest $request)
    {
        $_data      = $request->validated();
        
        $success    = false;
        $status     = 401;
        $data       = $request->all();

        $return =  Auth::guard('admin')->attempt(
            ['email' => $_data['login']['email'], 'password' => $_data['login']['password']], $request->filled('remember')
        );
        if($return){
            return redirect()->route('admin.dashboard');;
        }
        \Session::flash('message',['message'=>'Falha ao acessar o sistema.']);

        return redirect()->route('admin.login.form_login');;
    }

    public function login_json(LoginAdminRequest $request)
    {
        $_data  = $request->validated();
        
        $success = false;
        $status  = 401;
        $data = $request->all();

        // Get user by email
        $entity = AdminUsers::where('email', $_data['login']['email'])->first();

        // Validate Company
        if(!$entity) {
            return $this->sendError([], 'Invalid credentials');
        }

        // Validate Password
        if (!Hash::check($_data['login']['password'], $entity->password)) {
            return $this->sendError([], 'Invalid credentials');
        }

        $_token_data = $this->generate_token($entity);

        return $this->sendResponse(['result' =>$_token_data], 'Logged Admin.');
    }

    private function generate_token($entity = null){
        $token = Str::random(60);

        $api_token = $entity->createToken($token)->accessToken;

        //$api_token = hash('sha256', $token);
        $entity->api_token = $api_token;
        $entity->save();
        return [
            'access_token' => $api_token,
            'token_type' => 'bearer',
            'id' => $entity->id,
            'name' => $entity->name
        ];
    }
    
    public function logout(Request $request){
        try{
            Auth::logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route('admin.index');
        }catch (Exception $e){
            $message = trans('auth.token_inactive');
            return $this->sendResponse(false, 'Not logged out.');;
        }
        return $this->sendResponse(true, 'Logged out.');;
    }
}
