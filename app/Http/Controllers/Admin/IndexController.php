<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Repositories\General\IndexRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Index
 *
 * APIs for managing entitys
 */
class IndexController extends BaseController
{
    /**
     * @var AdminLte
     */
    private $adminlte;

    public function __construct(AdminLte $adminlte)
    {
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return redirect()->route('admin.index');
    }

}
