<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Http\Requests\Api\UserCostCentersStoreRequest;
use App\Http\Resources\UserCostCentersCollection;
use App\Http\Resources\UserCostCentersResource;
use App\Models\General\UserCostCenters;
use App\Repositories\General\UserCostCentersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group UserCostCenters
 *
 * APIs for managing entitys
 */
class UserCostCentersController extends BaseController
{
    public function __construct()
    {
        $this->repository = new UserCostCentersRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new UserCostCentersCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCostCentersStoreRequest $request)
    {
        $data   = $request->validated();  
        $entity = ($this->repository)->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new UserCostCentersResource($entity), 'Entity created successfully.');
        }
        return $this->sendError($this->repository->getErrors(), 'Failed to create entity');
        
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\UserCostCenters  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(UserCostCenters $user_cost_centers)
    {
        return $this->sendResponse(new UserCostCentersResource($user_cost_centers), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\UserCostCenters  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UserCostCentersStoreRequest $request, UserCostCenters $user_cost_centers)
    {
        $data   = $request->validated();  
        $entity = (new UserCostCentersRepository())->update($user_cost_centers->id, $data);

        return $this->sendResponse(new UserCostCentersResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\UserCostCenters  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCostCenters $user_cost_centers)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($user_cost_centers->id);
        }catch(\Exception $e){

        }
        if($entity)
            return $this->sendResponse([], 'Entity deleted successfully.');
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
