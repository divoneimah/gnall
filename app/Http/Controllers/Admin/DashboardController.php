<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Http\Requests\Api\AdminUsersStoreRequest;
use App\Http\Resources\AdminUsersCollection;
use App\Http\Resources\AdminUsersResource;
use App\Models\Admin\AdminUsers;
use App\Repositories\General\AdminUsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group AdminUsers
 *
 * APIs for managing entitys
 */
class DashboardController extends BaseController
{
    /**
     * @var AdminLte
     */
    private $adminlte;

    public function __construct(AdminLte $adminlte)
    {
        $this->adminlte = $adminlte;
        $this->middleware('auth');
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin/dashboard/dashboard')->with('adminlte', $this->adminlte);
    }

}
