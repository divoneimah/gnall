<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Http\Requests\Admin\AdminUsersStoreRequest;
use App\Http\Resources\AdminUsersCollection;
use App\Http\Resources\AdminUsersResource;
use App\Models\Admin\AdminUsers;
use App\Repositories\General\AdminUsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group AdminUsers
 *
 * APIs for managing entitys
 */
class AdminUsersController extends BaseController
{
    /**
     * @var AdminLte
     */
    private $adminlte;

    public function __construct(AdminLte $adminlte)
    {
        $this->adminlte = $adminlte;
        $this->middleware('auth');
        $this->repository = new AdminUsersRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');
        return redirect()->route('admin.users.index');
        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return view('admin/admin_users/index', ['errors'])
                ->with('adminlte', $this->adminlte)
                ->with('entities', $entities);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.admin_users.create', compact('page_title', 'adminlte'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUsersStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = (new AdminUsersRepository())->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Entity created successfully.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Entidade não criada.']);
        }
        return redirect()->route('admin_users.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(AdminUsers $admin_users)
    {
        return $this->sendResponse(new AdminUsersResource($admin_users), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminUsers $admin_users)
    {
        $entity                 = $admin_users;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.admin_users.edit', compact('entity', 'page_title', 'adminlte', 'admin_users'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUsersStoreRequest $request, AdminUsers $admin_users)
    {
        $data   = $request->validated();  
        $entity = (new AdminUsersRepository())->update($admin_users->id, $data);

        return $this->sendResponse(new AdminUsersResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\AdminUsers  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminUsers $admin_users)
    {
        $entity = null;
        if($admin_users->id == 1){
            \Session::flash('message',['class' => 'danger', 'message'=>'Este registro não pode ser removido.']);
            return $this->sendError([], 'Este registro não pode ser removido.');
        }
        try{
            $entity = ($this->repository)->delete($admin_users->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
