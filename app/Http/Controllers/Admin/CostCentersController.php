<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\CostCenters;
use App\Http\Resources\CostCentersResource as CostCentersResource;
use App\Http\Requests\Api\CostCentersStoreRequest;
use App\Http\Requests\Api\CostCentersUpdateRequest;
use App\Repositories\General\CostCentersRepository;
use App\Http\Resources\CostCentersCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Web - CostCenters
 *
 * APIs for managing entities
 */
class CostCentersController extends BaseController
{
    private $_order_columns = ['id','cost_center_id','description'
    ];

    /**
     * @var AdminLte
     */
    private $adminlte;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte     = $adminlte;
        $this->repository           = (new CostCentersRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');
        if(isset($params['return_json']) && $params['return_json'] == 1){
            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');
            if(isset($search['value'])){
                $params['cost_center_id']           = $search['value'];
                $params['description']              = $search['value'];
            }
            $entities               = $this->repository->filter($params);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            };
            $entities               = $entities->take($this->per_page)->skip($this->start_page)->get();
            $totalRecordswithFilter = $this->repository->filter($params)->count();
            $totalRecords           = $this->repository->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.cost_centers.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';

                    $_data[] = [$e->id,
                        $e->cost_center_id,
                        $e->description,
                        $actions 
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->filter($params)->paginate($this->per_page);
        }
        return view('admin.cost_centers.index', ['errors'])
        ->with('adminlte', $this->adminlte)
        ->with('entities', $entities);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.cost_centers.create', compact('page_title', 'adminlte'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostCentersStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.cost_centers.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function show(CostCenters $cost_centers)
    {
        return $this->sendResponse(new CostCentersResource($cost_centers), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CostCenters $cost_centers)
    {
        $entity                 = $cost_centers;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.cost_centers.edit', compact('entity', 'page_title', 'adminlte', 'cost_centers'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function update(CostCentersUpdateRequest $request, CostCenters $cost_centers)
    {
        $data           = $request->validated(); 
        $entity         = $this->repository->update($cost_centers->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.cost_centers.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\CostCenters  $cost_centers
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostCenters $cost_centers)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($cost_centers->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
