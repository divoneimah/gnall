<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Http\Requests\Admin\UserPaymentFormsStoreRequest;
use App\Http\Resources\UserPaymentFormsCollection;
use App\Http\Resources\UserPaymentFormsResource;
use App\Models\General\UserPaymentForms;
use App\Repositories\General\UserPaymentFormsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * @group UserPaymentForms
 *
 * APIs for managing entitys
 */
class UserPaymentFormsController extends BaseController
{
    public function __construct()
    {
        $this->repository = new UserPaymentFormsRepository();
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new UserPaymentFormsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPaymentFormsStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = ($this->repository)->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.users.edit', $data['user_payment_forms']['user_id']);

    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(UserPaymentForms $user_payment_forms)
    {
        return $this->sendResponse(new UserPaymentFormsResource($user_payment_forms), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UserPaymentFormsStoreRequest $request, UserPaymentForms $user_payment_forms)
    {
        $data   = $request->validated();
        $entity = (new UserPaymentFormsRepository())->update($user_payment_forms->id, $data);

        return $this->sendResponse(new UserPaymentFormsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\UserPaymentForms  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPaymentForms $user_payment_forms)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($user_payment_forms->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }

        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
