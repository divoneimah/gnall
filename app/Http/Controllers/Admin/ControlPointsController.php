<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\AccessGroupModules;
use App\Models\General\ControlPoints;
use App\Http\Resources\ControlPointsResource as ControlPointsResource;
use App\Http\Requests\Admin\ControlPointsStoreRequest;
use App\Http\Requests\Admin\ControlPointsUpdateRequest;
use App\Repositories\General\ControlPointsRepository;
use App\Http\Resources\ControlPointsCollection;
use App\Repositories\General\UsersRepository;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


/**
 * @group Web - ControlPoints
 *
 * APIs for managing entities
 */
class ControlPointsController extends BaseController
{
    private $_order_columns = ['id','name','latitude', 'longitude', 'note',
    'created_at','updated_at'];

    /**
     * @var AdminLte
     */
    private $adminlte;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte             = $adminlte;
        $this->repository           = (new ControlPointsRepository());
        $this->repo_users           = (new UsersRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        if(isset($params['return_json']) && $params['return_json'] == 1){

            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');

            if (isset($params['user_id'])) {
                $params['control_points.user_id'] = $params['user_id'];
                unset($params['user_id']);
            }



            $entities               = $this->repository->with(['users', 'createdUsers', 'updatedUsers'])
                    ->leftJoin('users', 'users.id', '=', 'control_points.user_id')
                    ->leftJoin('users as c', 'c.id', '=', 'control_points.created_user_id')
                    ->leftJoin('users as uu', 'uu.id', '=', 'control_points.updated_user_id');

            if (isset($params['control_points.user_id'])) {
                $entities->where("control_points.user_id", "=", $params['control_points.user_id']);
            }
            if(isset($params['initial_date']) && isset($params['final_date'])){
                $entities->whereBetween('control_points.created_at', array($params['initial_date']."00:00:00",$params['final_date']."23:59:59"));
            }
            /*echo '<pre>';
            echo $entities->toSql();
            //var_dump($params['user_id']);
            print_r($params);
            echo '</pre>';
            die();*/
            if(isset($search['value'])){
                $entities           = $entities->where('users.name','like',"%{$search['value']}%");
            }
            $entities               = $entities->select(["control_points.*", "users.name", "uu.name as updated_name", "c.name as created_name"]);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            };

            $entities               = $entities
            ->take($this->per_page)->skip($this->start_page)->get();

            $totalRecordswithFilter = $this->repository->with(['users'])->filter($params)->count();
            $totalRecords           = $this->repository->with(['users'])->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.control_points.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';
                    $created_at = ($e->created_at != null)?$e->created_at->format('d/m/Y'):null;
                    $updated_at = ($e->updated_at != null)?$e->updated_at->format('d/m/Y'):null;

                    $created_at = isset($e->created_name)?$e->created_name. "-". $created_at:'-';
                    $updated_at = isset($e->updated_name)?$e->updated_name."-". $updated_at: "-";

                    $_data[] = [$e->id,
                        $e->name,
                        $e->latitude,
                        $e->longitude,
                        $e->note,
                        $created_at,
                        $updated_at,
                        $actions
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->with(['users'])->paginate($params);
        }



        $users = $this->repo_users
            ->with(['userAccessGroups'])
            ->join('user_access_groups', 'users.id', '=', 'user_access_groups.user_id')
            ->where('user_access_groups.access_group_module_id', '=', AccessGroupModules::$ACCESS_GROUP_MANAGER)
            ->orderBy("name", "ASC")
            ->select(['users.id', 'users.name']);
        if (isset($entity_user->userAccessGroups->access_group_module_id) &&
            $entity_user->userAccessGroups->access_group_module_id == AccessGroupModules::$ACCESS_GROUP_MANAGER) {
            $users = $users->where('users.id', '=', $entity_user->id);
        }
        $users = $users
            ->get();

        return view('admin.control_points.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities)
            ->with('users', $users);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $users                  = $this->repo_users->all();
        return view('admin.control_points.create', compact('page_title', 'adminlte', 'users'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ControlPointsStoreRequest $request)
    {
        $data       = $request->validated();
        $user_id    = Auth::user()->id;
        $data['control_points']['created_user_id'] = $user_id;
        $data['control_points']['updated_user_id'] = $user_id;
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.control_points.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function show(ControlPoints $control_points)
    {
        return $this->sendResponse(new ControlPointsResource($control_points), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ControlPoints $control_points)
    {
        $entity                 = $control_points;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $users                  = $this->repo_users->all();
        return view('admin.control_points.edit', compact('entity', 'page_title', 'adminlte', 'control_points', 'users'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function update(ControlPointsUpdateRequest $request, ControlPoints $control_points)
    {
        $data           = $request->validated();
        $user_id        = Auth::user()->id;
        $data['control_points']['updated_user_id'] = $user_id;
        $entity         = $this->repository->update($control_points->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.control_points.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\ControlPoints  $control_points
     * @return \Illuminate\Http\Response
     */
    public function destroy(ControlPoints $control_points)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($control_points->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }

        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
