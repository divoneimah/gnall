<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Http\Requests\Admin\UsersStoreRequest;
use App\Http\Resources\UsersCollection;
use App\Http\Resources\UsersResource;
use App\Models\General\Users;
use App\Repositories\General\CostCentersRepository;
use App\Repositories\General\DepartamentsRepository;
use App\Repositories\General\SubsidiariesRepository;
use App\Repositories\General\UsersRepository;
use App\Repositories\General\PaymentFormsRepository;
use App\Repositories\General\AccessGroupModulesRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Users
 *
 * APIs for managing entitys
 */
class UsersController extends BaseController
{
    private $_order_columns = ['id','name','email', 'id'];
    /**
     * @var AdminLte
     */
    private $adminlte;
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte                 = $adminlte;
        $this->repository               = new UsersRepository();
        $this->repo_cost_centers        = (new CostCentersRepository());
        $this->repo_departaments        = (new DepartamentsRepository());
        $this->repo_subsidiaries        = (new SubsidiariesRepository());
        $this->repo_payment_forms       = (new PaymentFormsRepository());
        $this->repo_access_group_module  = (new AccessGroupModulesRepository());
        
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        if(isset($params['return_json']) && $params['return_json'] == 1){
            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');

            if(isset($search['value'])){
                $params['name']                 = $search['value'];
                $params['email']                = $search['value'];
            }
            $entities               = $this->repository->filter($params);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            }
            $entities               = $entities->take($this->per_page)->skip($this->start_page)->get();
            $totalRecordswithFilter = $this->repository->filter($params)->count();
            $totalRecords           = $this->repository->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.users.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';
                    $departament = (isset($e->departaments->description)?$e->departaments->description:"-");
                    $permission = isset($e->userAccessGroups->accessGroupModules->system_modules)?$e->userAccessGroups->accessGroupModules->name:'-';
                    $_data[] = [$e->id,
                        $e->name,
                        $e->email,
                        $departament,
                        $permission,
                        $actions 
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->filter($params)->paginate($this->per_page);
        }
        return view('admin.users.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;

        $departaments           = $this->repo_departaments->get();
        $subsidiaries           = $this->repo_subsidiaries->where('active','=',true)->get();
        $cost_centers           = $this->repo_cost_centers->all();
        $payment_forms          = $this->repo_payment_forms->all();
        $access_group_module    = $this->repo_access_group_module->all();

        return view('admin.users.create', compact('page_title', 'departaments', 'adminlte', 'subsidiaries', 'cost_centers', 'payment_forms', 'access_group_module'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersStoreRequest $request)
    {
        $data   = $request->validated();   
        $entity = (new UsersRepository())->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.users.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        return $this->sendResponse(new UsersResource($users), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $users)
    {
        $entity                 = $users;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $departaments           = $this->repo_departaments->get();
        $subsidiaries           = $this->repo_subsidiaries->where('active','=',true)->get();
        $cost_centers           = $this->repo_cost_centers->all();
        $payment_forms          = $this->repo_payment_forms->all();
        $access_group_module    = $this->repo_access_group_module->all();

        return view('admin.users.edit', compact('entity', 'page_title', 'departaments','adminlte', 'users', 'subsidiaries', 'cost_centers', 'payment_forms', 'access_group_module'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(UsersStoreRequest $request, Users $users)
    {
        $data   = $request->validated();  
        $entity = (new UsersRepository())->update($users->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.users.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\Users  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($users->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
