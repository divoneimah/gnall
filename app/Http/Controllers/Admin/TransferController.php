<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\Expenses;
use App\Http\Resources\ExpensesResource as ExpensesResource;
use App\Http\Requests\Admin\ExpensesStoreRequest;
use App\Http\Requests\Admin\ExpensesUpdateRequest;
use App\Repositories\General\AccessGroupModulesRepository;
use App\Repositories\General\CostCentersRepository;
use App\Repositories\General\ExpensesRepository;
use App\Repositories\General\EventsRepository;
use App\Repositories\General\ExpenseNaturesRepository;
use App\Repositories\General\PaymentFormsRepository;
use App\Repositories\General\UsersRepository;
use App\Repositories\General\DepartamentsRepository;
use App\Repositories\General\SubsidiariesRepository;
use App\Repositories\General\UserAccessGroupsRepository;
use App\Repositories\General\UserPaymentFormsRepository;
use App\Http\Resources\ExpensesCollection;
use App\Http\Resources\UserPaymentFormsCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\General\AccessGroupModules;
use App\Models\General\Events;

class TransferController extends BaseControllerController
{
    //
}
