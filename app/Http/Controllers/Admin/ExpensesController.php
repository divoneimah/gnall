<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\Expenses;
use App\Http\Resources\ExpensesResource as ExpensesResource;
use App\Http\Requests\Admin\ExpensesStoreRequest;
use App\Http\Requests\Admin\ExpensesUpdateRequest;
use App\Repositories\General\AccessGroupModulesRepository;
use App\Repositories\General\CostCentersRepository;
use App\Repositories\General\ExpensesRepository;
use App\Repositories\General\EventsRepository;
use App\Repositories\General\ExpenseNaturesRepository;
use App\Repositories\General\PaymentFormsRepository;
use App\Repositories\General\UsersRepository;
use App\Repositories\General\DepartamentsRepository;
use App\Repositories\General\SubsidiariesRepository;
use App\Repositories\General\UserAccessGroupsRepository;
use App\Repositories\General\UserPaymentFormsRepository;
use App\Repositories\General\TransferRepository;
use App\Http\Resources\ExpensesCollection;
use App\Http\Resources\UserPaymentFormsCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\General\AccessGroupModules;
use App\Models\General\Events;

/**
 * @group Web - Expenses
 *
 * APIs for managing entities
 */
class ExpensesController extends BaseController
{
    private $_order_columns = ['expenses.id', 'event_id', 'cost_center_id', 'id', 'payment_form_id',
        'expense_date', 'id', 'note', 'expenses.user_id', 'user_from_id'];

    private $_exclude_sum_debit = ["'D'", "'X'", "'S'"];

    private $_collumns = ['expenses.id',
        'expenses.event_id',
        'expenses.cost_center_id',
        'expenses.expense_date',
        'expenses.quantity',
        'expenses.unitary',
        'expenses.addition',
        'expenses.discount',
        'expenses.total',
        'expenses.note',
        'expenses.user_id',
        'expenses.longitude',
        'expenses.latitude',
        'expenses.user_from_id',
        'expenses.internal_app_id',
        'expenses.subsidiary_id',
        'expenses.payment_form_id',
        'expenses.created_at',
        'expenses.updated_at',
        'expenses.deleted_at',
        'expenses.user_payment_form_id',
        'expenses.created_user_id',
        'expenses.updated_user_id'];
    /**
     * @var AdminLte
     */
    private $adminlte;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte = $adminlte;
        $this->repository = (new ExpensesRepository());
        $this->repo_events = (new EventsRepository());
        $this->repo_cost_centers = (new CostCentersRepository());
        $this->repo_expense_natures = (new ExpenseNaturesRepository());
        $this->repo_payment_forms = (new PaymentFormsRepository());
        $this->repo_user_payment_forms = (new UserPaymentFormsRepository());
        $this->repo_users = (new UsersRepository());
        $this->repo_departaments = (new DepartamentsRepository());
        $this->repo_subsidiaries = (new SubsidiariesRepository());
        $this->repo_access_group_module = (new AccessGroupModulesRepository());
        $this->repo_user_access_group = (new UserAccessGroupsRepository());
        $this->tranfer_repository = (new TransferRepository());
        if ($req->input("per_page")) {
            $this->per_page = (int)$req->input("per_page");
        }
        if ($req->input("start")) {
            $this->start_page = (int)$req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->except('_token');
        $user = Auth::user();

        if (isset($params['return_json']) && $params['return_json'] == 1) {
            $draw = $request->input('draw');
            $search = $request->input('search');
            $order = $request->input('order');
            $totalSumTypes = 0;
            if (isset($search['value'])) {
                $params['unit_measure_id'] = $search['value'];
                $params['description'] = $search['value'];
            }

            $user_access_group = $this->repo_user_access_group->where('user_id', '=', $user->id)->first();
            if ($user_access_group && $user_access_group->access_group_module_id == AccessGroupModules::$ACCESS_GROUP_MANAGER) {
                $params['expenses.user_id'] = $user->id;
            }
            if (isset($params['user_id'])) {
                $params['expenses.user_id'] = $params['user_id'];
                unset($params['user_id']);
            }
            $entities = $this->repository->with(['users'])->join("events", 'events.id', '=', 'expenses.event_id')->join("users", "users.id", "=", "expenses.user_id")
                ->filter($params)->select($this->_collumns)->selectRaw(
                    "CASE WHEN events.movement_type IN
                (" . implode(",", $this->_exclude_sum_debit) . ")
                THEN
                    expenses.total * -1
                ELSE expenses.total END AS SomaSubtrai"
                );

            if (isset($params['expenses.user_id'])) {
                $entities->where("expenses.user_id", "=", $params['expenses.user_id']);
            }

            // die($entities->toSql() . " " . json_encode($entities->getBindings()));
            if (isset($order[0]['column'])) {
                if (isset($this->_order_columns[$order[0]['column']])) {
                    $order_by = $this->_order_columns[$order[0]['column']];
                    $order_dir = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            }
            if (isset($params['departament_id']) && $params['departament_id'] != "") {
                $entities = $entities->where("users.departament_id", "=", $params['departament_id']);
            }
            $totalSum = 0;

            $entitiesTotal = $entities;
            $entitiesTotal = $entitiesTotal->get();
            if ($entitiesTotal) {
                foreach ($entitiesTotal as $e) {
                    $totalSum += $e->SomaSubtrai;
                }
            }

            $whereTrasferBetween = array();
            $whereTransfer = array();
            $whereTransferDebit = array();
            $whereTransferCredit = array();
            if ($params['initial_date'] && $params['final_date']) {
                $whereTrasferBetween[] = [$params['initial_date']." 00:00:00", $params['final_date']." 23:59:59"];
            }

            if (isset($params['expenses.user_id'])) {
                $whereTransfer[] = ['transfers.user_id', '=', $params['expenses.user_id']];
            }

            if ($params['departament_id']) {
                $whereTransfer[] = ['users.departament_id', '=', $params['departament_id']];
            }

            if(isset($params['user_payment_form_id'])){
                $whereTransferDebit[] = ['id_debit_account','=',$params['user_payment_form_id']];
                $whereTransferDebit[] = ['id_credit_account','!=',$params['user_payment_form_id']];
                $whereTransferCredit[] = ['id_credit_account','=',$params['user_payment_form_id']];
                $whereTransferCredit[] = ['id_debit_account','!=',$params['user_payment_form_id']];
            }
            $entitiesTransferDebito = $this->tranfer_repository
                ->with('users')
                ->selectRaw("`transfers`.`id`, '' as `event_id`, '' as `cost_center_id`,
                                                              `transfers`.`created_at`, '' as `quantity`, '' as `unitary`, '' as `addition`, '' as `discount`,
                                                              `transfers`.`valor` as `total`, '' as `note`, `transfers`.`user_id`, '' as `longitude`, '' as `latitude`,
                                                              '' as `user_from_id`, `transfers`.`internal_app_id`, '' `subsidiary_id`, '' as `payment_form_id`, `transfers`.`created_at`,
                                                              `transfers`.`updated_at`, `transfers`.`deleted_at`, '' as `user_payment_form_id`, `transfers`.`created_user_id`, `transfers`.`updated_user_id`,
                                                               transfers.valor * -1  AS SomaSubtrai, `payment_forms`.`description`,`user_payment_forms`.`card_number`,`transfers`.`subsidiary_id`,`subsidiaries`.`name` AS subsidiariesName")
                ->join("users", "users.id", "=", "transfers.user_id")
                ->join("subsidiaries", "subsidiaries.id", "=", "transfers.subsidiary_id")
                ->join("user_payment_forms", function ($join){
                    $join->on("user_payment_forms.id", "=", "transfers.id_debit_account");
                    $join->on("user_payment_forms.user_id" , "=","transfers.user_id");
                    })
                ->join("payment_forms", "payment_forms.id", "=", "user_payment_forms.payment_form_id");

            $entitiesTransferCredit = $this->tranfer_repository
                ->with('users')
                ->selectRaw("`transfers`.`id`, '' as `event_id`, '' as `cost_center_id`,
                                                              `transfers`.`created_at`, '' as `quantity`, '' as `unitary`, '' as `addition`, '' as `discount`,
                                                              `transfers`.`valor` as `total`, '' as `note`, `transfers`.`user_id`, '' as `longitude`, '' as `latitude`,
                                                              '' as `user_from_id`, `transfers`.`internal_app_id`, '' `subsidiary_id`, '' as `payment_form_id`, `transfers`.`created_at`,
                                                              `transfers`.`updated_at`, `transfers`.`deleted_at`, '' as `user_payment_form_id`, `transfers`.`created_user_id`, `transfers`.`updated_user_id`,
                                                               transfers.valor * -1  AS SomaSubtrai, `payment_forms`.`description`,`user_payment_forms`.`card_number`,`transfers`.`subsidiary_id`,`subsidiaries`.`name` AS subsidiariesName")
                ->join("users", "users.id", "=", "transfers.user_id")
                ->join("subsidiaries", "subsidiaries.id", "=", "transfers.subsidiary_id")
                ->join("user_payment_forms", function ($join){
                    $join->on("user_payment_forms.id", "=", "transfers.id_credit_account");
                    $join->on("user_payment_forms.user_id" , "=","transfers.user_id");
                } )
                ->join("payment_forms", "payment_forms.id", "=", "user_payment_forms.payment_form_id");

            if (count($whereTrasferBetween) > 0) {
                $entitiesTransferDebito->whereBetween('transfers.created_at', $whereTrasferBetween);
                $entitiesTransferCredit->whereBetween('transfers.created_at', $whereTrasferBetween);
            }

            if (count($whereTransfer) > 0) {
                $entitiesTransferDebito->where($whereTransfer);
                $entitiesTransferCredit->where($whereTransfer);
            }

            if(count($whereTransferDebit)){
                $entitiesTransferDebito->where($whereTransferDebit);
            }

            if(count($whereTransferCredit)){
                $entitiesTransferCredit->where($whereTransferCredit);
            }
            $entitiesTransferDebito->distinct()->groupBy('transfers.id');
            $entitiesTransferCredit->distinct()->groupBy('transfers.id');
            $totalPageDebit = 0;
            if ($entitiesTransferDebito) {
                $entitiesTransferDebitoTotal = $entitiesTransferDebito->get();
                foreach ($entitiesTransferDebitoTotal as $ef) {
                    $totalSum -= $ef->total;
                    $totalPageDebit++;
                }
            }
            $totalPageCredit = 0;
            if ($entitiesTransferCredit) {
                $entitiesTransferCreditTotal = $entitiesTransferCredit->get();
                foreach ($entitiesTransferCreditTotal as $ef) {
                    $totalSum += $ef->total;
                    $totalPageCredit++;
                }
            }

            //echo $entitiesTransferCredit->count()."<br>";
            /*echo '<pre>';
            echo $entitiesTransferDebito->toSql()."\n";

            echo $entitiesTransferCredit->toSql()."\n";
            die();*/
            $totalRecords = $entities->count() + $totalPageDebit + $totalPageCredit;
            $totalRecordswithFilter = $entities->count() + $totalPageDebit + $totalPageCredit;
            //echo $entities->toSql();
            $entitiesTransferDebito = $entitiesTransferDebito->take($this->per_page)->skip($this->start_page)->get();
            $entitiesTransferCredit = $entitiesTransferCredit->take($this->per_page)->skip($this->start_page)->get();
            $entities = $entities->take($this->per_page)->skip($this->start_page)->get();

            $_data = [];
            #$totalSum = 0;
            //dd($entities);
            //die();

            if ($entities) {
                foreach ($entities as $e) {

                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="' . route("admin.expenses.edit", $e->id) . '"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete(' . $e->id . ')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';
                    $_values = numbr($e->SomaSubtrai);
                    $subsidiary = isset($e->subsidiaries->name) ? $e->subsidiaries->name : '-';
                    $subsidiary_id = isset($e->subsidiaries->id) ? $e->subsidiaries->id : '-';
                    $nature_expense = isset($e->events->expenseNatures->description) ? $e->events->expenseNatures->description : '-';

                    // Conta
                    $payment_form = isset($e->userPaymentForms->paymentForms->description) ? $e->userPaymentForms->paymentForms->description . " ********" . substr($e->userPaymentForms->card_number, -4) : '-';
                    #$totalSum     += $e->SomaSubtrai;

                    $_data[] = [$e->id,
                        $e->events->description,
                        $e->costCenters->description,
                        $subsidiary_id,
                        $subsidiary,
                        $nature_expense,
                        $payment_form,
                        toDateBr($e->expense_date, false),
                        $_values,
                        $e->note,
                        isset($e->users->name) ? $e->users->name : "-",
                        $actions
                    ];
                }
            }

            if ($entitiesTransferDebito) {
                foreach ($entitiesTransferDebito as $t) {
                    $actions = '';
                    $_values = numbr(-$t->total);
                    $subsidiary = $t->subsidiariesName;
                    $subsidiary_id = $t->subsidiary_id;
                    $nature_expense = isset($t->events->expenseNatures->description) ? $t->events->expenseNatures->description : '-';
                    // Conta
                    /*echo '<pre>';
                    print_r($t);
                    echo '</pre>';*/
                    $payment_form = isset($t->description) ? $t->description . " ********" . substr($t->card_number, -4) : '-';
                    #$totalSum     += $e->SomaSubtrai;
                    $_data[] = [$t->id,
                        "Tranferência débito",
                        "",
                        $subsidiary_id,
                        $subsidiary,
                        $nature_expense,
                        $payment_form,
                        toDateBr($e->expense_date, false),
                        $_values,
                        $t->note,
                        isset($t->users->name) ? $e->users->name : "-",
                        $actions
                    ];
                }
            }

            if ($entitiesTransferCredit) {
                foreach ($entitiesTransferCredit as $t) {
                    $actions = '';
                    $_values = numbr($t->total);
                    $subsidiary = $t->subsidiariesName;
                    $subsidiary_id = $t->subsidiary_id;
                    $nature_expense = isset($t->events->expenseNatures->description) ? $t->events->expenseNatures->description : '-';
                    // Conta
                    $payment_form = isset($t->description) ? $t->description . " ********" . substr($t->card_number, -4) : '-';
                    /*echo '<pre>';
                    print_r($t);
                    echo '</pre>';*/
                    #$totalSum     += $e->SomaSubtrai;
                    $_data[] = [$t->id,
                        "Tranferência crédito",
                        "",
                        $subsidiary_id,
                        $subsidiary,
                        $nature_expense,
                        $payment_form,
                        toDateBr($e->expense_date, false),
                        $_values,
                        $t->note,
                        isset($t->users->name) ? $e->users->name : "-",
                        $actions
                    ];
                }
            }
            //die();
            $additional['totalSum'] = numbr($totalSum);
            return $this->sendResponseDataTable($_data, $draw, $totalRecords, $totalRecordswithFilter, $additional);
        } else if (isset($params['download_csv'])) {
            if ($params['download_csv'] == 1) {
                if (isset($search['value'])) {
                    $params['unit_measure_id'] = $search['value'];
                    $params['description'] = $search['value'];
                }

                $user_access_group = $this->repo_user_access_group->where('user_id', '=', $user->id)->first();
                if ($user_access_group && $user_access_group->access_group_module_id == AccessGroupModules::$ACCESS_GROUP_MANAGER) {
                    $params['expenses.user_id'] = $user->id;
                }
                if (isset($params['user_id'])) {
                    $params['expenses.user_id'] = $params['user_id'];
                    unset($params['user_id']);
                }
                $entities = $this->repository->with(['users'])->join("events", 'events.id', '=', 'expenses.event_id')->join("users", "users.id", "=", "expenses.user_id")
                    ->filter($params)->select($this->_collumns)->selectRaw(
                        "CASE WHEN events.movement_type IN
                (" . implode(",", $this->_exclude_sum_debit) . ")
                THEN
                    expenses.total * -1
                ELSE expenses.total END AS SomaSubtrai"
                    );

                if (isset($params['expenses.user_id'])) {
                    $entities->where("expenses.user_id", "=", $params['expenses.user_id']);
                }


                if (isset($params['departament_id']) && $params['departament_id'] != "") {
                    $entities = $entities->where("users.departament_id", "=", $params['departament_id']);
                }

                $whereTrasferBetween = array();
                $whereTransfer = array();
                $whereTransferDebit = array();
                $whereTransferCredit = array();
                if ($params['initial_date'] && $params['final_date']) {
                    $whereTrasferBetween[] = [$params['initial_date']." 00:00:00", $params['final_date']." 23:59:59"];
                }

                if (isset($params['expenses.user_id'])) {
                    $whereTransfer[] = ['transfers.user_id', '=', $params['expenses.user_id']];
                }

                if ($params['departament_id']) {
                    $whereTransfer[] = ['users.departament_id', '=', $params['departament_id']];
                }

                if(isset($params['user_payment_form_id'])){
                    $whereTransferDebit[] = ['id_debit_account','=',$params['user_payment_form_id']];
                    $whereTransferDebit[] = ['id_credit_account','!=',$params['user_payment_form_id']];
                    $whereTransferCredit[] = ['id_credit_account','=',$params['user_payment_form_id']];
                    $whereTransferCredit[] = ['id_debit_account','!=',$params['user_payment_form_id']];
                }
                $entitiesTransferDebito = $this->tranfer_repository
                    ->with('users')
                    ->selectRaw("`transfers`.`id`, '' as `event_id`, '' as `cost_center_id`,
                                                              `transfers`.`created_at`, '' as `quantity`, '' as `unitary`, '' as `addition`, '' as `discount`,
                                                              `transfers`.`valor` as `total`, '' as `note`, `transfers`.`user_id`, '' as `longitude`, '' as `latitude`,
                                                              '' as `user_from_id`, `transfers`.`internal_app_id`, '' `subsidiary_id`, '' as `payment_form_id`, `transfers`.`created_at`,
                                                              `transfers`.`updated_at`, `transfers`.`deleted_at`, '' as `user_payment_form_id`, `transfers`.`created_user_id`, `transfers`.`updated_user_id`,
                                                               transfers.valor * -1  AS SomaSubtrai, `payment_forms`.`description`,`user_payment_forms`.`card_number`,`transfers`.`subsidiary_id`,`subsidiaries`.`name` AS subsidiariesName")
                    ->join("users", "users.id", "=", "transfers.user_id")
                    ->join("subsidiaries", "subsidiaries.id", "=", "transfers.subsidiary_id")
                    ->join("user_payment_forms", function ($join){
                        $join->on("user_payment_forms.id", "=", "transfers.id_debit_account");
                        $join->on("user_payment_forms.user_id" , "=","transfers.user_id");
                    })
                    ->join("payment_forms", "payment_forms.id", "=", "user_payment_forms.payment_form_id");

                $entitiesTransferCredit = $this->tranfer_repository
                    ->with('users')
                    ->selectRaw("`transfers`.`id`, '' as `event_id`, '' as `cost_center_id`,
                                                              `transfers`.`created_at`, '' as `quantity`, '' as `unitary`, '' as `addition`, '' as `discount`,
                                                              `transfers`.`valor` as `total`, '' as `note`, `transfers`.`user_id`, '' as `longitude`, '' as `latitude`,
                                                              '' as `user_from_id`, `transfers`.`internal_app_id`, '' `subsidiary_id`, '' as `payment_form_id`, `transfers`.`created_at`,
                                                              `transfers`.`updated_at`, `transfers`.`deleted_at`, '' as `user_payment_form_id`, `transfers`.`created_user_id`, `transfers`.`updated_user_id`,
                                                               transfers.valor * -1  AS SomaSubtrai, `payment_forms`.`description`,`user_payment_forms`.`card_number`,`transfers`.`subsidiary_id`,`subsidiaries`.`name` AS subsidiariesName")
                    ->join("users", "users.id", "=", "transfers.user_id")
                    ->join("subsidiaries", "subsidiaries.id", "=", "transfers.subsidiary_id")
                    ->join("user_payment_forms", function ($join){
                        $join->on("user_payment_forms.id", "=", "transfers.id_credit_account");
                        $join->on("user_payment_forms.user_id" , "=","transfers.user_id");
                    } )
                    ->join("payment_forms", "payment_forms.id", "=", "user_payment_forms.payment_form_id");

                if (count($whereTrasferBetween) > 0) {
                    $entitiesTransferDebito->whereBetween('transfers.created_at', $whereTrasferBetween);
                    $entitiesTransferCredit->whereBetween('transfers.created_at', $whereTrasferBetween);
                }

                if (count($whereTransfer) > 0) {
                    $entitiesTransferDebito->where($whereTransfer);
                    $entitiesTransferCredit->where($whereTransfer);
                }

                if(count($whereTransferDebit)){
                    $entitiesTransferDebito->where($whereTransferDebit);
                }

                if(count($whereTransferCredit)){
                    $entitiesTransferCredit->where($whereTransferCredit);
                }
                $entitiesTransferDebito->distinct()->groupBy('transfers.id');
                $entitiesTransferCredit->distinct()->groupBy('transfers.id');

                $entities = $entities->get();
                $entitiesTransferDebito = $entitiesTransferDebito->get();
                $entitiesTransferCredit = $entitiesTransferCredit->get();
                return $this->exportCsv($entities,$entitiesTransferDebito,$entitiesTransferCredit);
            }
        } else {
            $entities = $this->repository->with(['events', 'users', 'costCenters'])->filter($params)->paginate($this->per_page);
        }
        $entity_user = $this->repo_users->with(['userAccessGroups'])->find($user->id);

        $users = $this->repo_users
            ->with(['userAccessGroups'])
            ->join('user_access_groups', 'users.id', '=', 'user_access_groups.user_id')
            ->where('user_access_groups.access_group_module_id', '=', AccessGroupModules::$ACCESS_GROUP_MANAGER)
            ->orderBy("name", "ASC")
            ->select(['users.id', 'users.name']);
        if (isset($entity_user->userAccessGroups->access_group_module_id) &&
            $entity_user->userAccessGroups->access_group_module_id == AccessGroupModules::$ACCESS_GROUP_MANAGER) {
            $users = $users->where('users.id', '=', $entity_user->id);
        }
        $users = $users
            ->get();
        $user_payment_forms = $this->repo_user_payment_forms
            ->with(['paymentForms']);
        if ($entity_user->userAccessGroups->access_group_module_id != AccessGroupModules::$ACCESS_GROUP_ADMIN) {
            $user_payment_forms = $user_payment_forms->where('user_id', '=', $user->id);
        }
        $user_payment_forms = $user_payment_forms->get();
        $departaments = $this->repo_departaments->orderBy("description", "ASC")->get();
        $payment_forms = $this->repo_payment_forms->all();

        return view('admin.expenses.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities)
            ->with('user_payment_forms', $user_payment_forms)
            ->with('payment_forms', $payment_forms)
            ->with('departaments', $departaments)
            ->with('users', $users);
    }

    public function exportCsv($entities,$entitiesTransferDebito = null,$entitiesTransferCredit = null)
    {
        $fileName = "Despesas-" . date("d-m-Y") . ".csv";


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $dataCsv = array();
        $i = 0;
        foreach ($entities as $e) {

            $dataCsv[$i]['Evento'] = utf8_decode($e->events->description);
            $dataCsv[$i]['Centro de custo'] = utf8_decode($e->costCenters->description);
            $dataCsv[$i]['Unidade ERP'] = isset($e->subsidiaries->id) ? $e->subsidiaries->id : '-';
            $dataCsv[$i]['Nome Unidade'] = isset($e->subsidiaries->name) ? $e->subsidiaries->name : '-';
            $dataCsv[$i]['Natureza Gasto ERP'] = utf8_decode(isset($e->events->expenseNatures->description) ? $e->events->expenseNatures->description : '-');
            $dataCsv[$i]['Conta'] = utf8_decode(isset($e->userPaymentForms->paymentForms->description) ? $e->userPaymentForms->paymentForms->description . " ********" . substr($e->userPaymentForms->card_number, -4) : '-');
            $dataCsv[$i]['Data da despesa'] = toDateBr($e->expense_date, false);
            $dataCsv[$i]['Valores'] = numbr($e->SomaSubtrai);
            $dataCsv[$i]['Observacao'] = utf8_decode($e->note);
            $dataCsv[$i]['Gestor'] = utf8_decode(isset($e->users->name) ? $e->users->name : "-");

            $i++;
        }

        if ($entitiesTransferDebito) {
            foreach ($entitiesTransferDebito as $t) {
                $subsidiary = $t->subsidiariesName;
                $subsidiary_id = $t->subsidiary_id;
                // Conta

                $payment_form = isset($t->description) ? $t->description . " ********" . substr($t->card_number, -4) : '-';
                #$totalSum     += $e->SomaSubtrai;
                $dataCsv[$i]['Evento'] = utf8_decode("Tranferência débito");
                $dataCsv[$i]['Centro de custo'] = utf8_decode("");
                $dataCsv[$i]['Unidade ERP'] = $subsidiary_id;
                $dataCsv[$i]['Nome Unidade'] = $subsidiary;
                $dataCsv[$i]['Natureza Gasto ERP'] = utf8_decode(isset($e->events->expenseNatures->description) ? $e->events->expenseNatures->description : '-');
                $dataCsv[$i]['Conta'] = utf8_decode($payment_form);
                $dataCsv[$i]['Data da despesa'] = toDateBr($t->created_at, false);
                $dataCsv[$i]['Valores'] = numbr(-$t->total);
                $dataCsv[$i]['Observacao'] = utf8_decode($t->note);
                $dataCsv[$i]['Gestor'] = utf8_decode(isset($t->users->name) ? $t->users->name : "-");
                $i++;
            }
        }


        if ($entitiesTransferCredit) {
            foreach ($entitiesTransferCredit as $t) {

                $subsidiary = $t->subsidiariesName;
                $subsidiary_id = $t->subsidiary_id;

                $payment_form = isset($t->description) ? $t->description . " ********" . substr($t->card_number, -4) : '-';

                $dataCsv[$i]['Evento'] = utf8_decode("Tranferência Crédito");
                $dataCsv[$i]['Centro de custo'] = utf8_decode("");
                $dataCsv[$i]['Unidade ERP'] = $subsidiary_id;
                $dataCsv[$i]['Nome Unidade'] = $subsidiary;
                $dataCsv[$i]['Natureza Gasto ERP'] = utf8_decode(isset($e->events->expenseNatures->description) ? $e->events->expenseNatures->description : '-');
                $dataCsv[$i]['Conta'] = utf8_decode($payment_form);
                $dataCsv[$i]['Data da despesa'] = toDateBr($t->created_at, false);
                $dataCsv[$i]['Valores'] = numbr($t->total);
                $dataCsv[$i]['Observacao'] = utf8_decode($t->note);
                $dataCsv[$i]['Gestor'] = utf8_decode(isset($t->users->name) ? $t->users->name : "-");
                $i++;
            }
        }

        $columns = array('Evento', 'Centro de custo', 'Unidade ERP', 'Nome Unidade', 'Natureza Gasto ERP', 'Conta', 'Data da despesa', 'Valores', 'Observacao', 'Gestor');
        #dd($entities);
        $callback = function () use ($dataCsv, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ";", '"');

            foreach ($dataCsv as $row) {

                /*$row['Evento'] = utf8_decode($e->events->description);
                $row['Centro de custo'] = utf8_decode($e->costCenters->description);
                $row['Unidade ERP'] = isset($e->subsidiaries->id) ? $e->subsidiaries->id : '-';
                $row['Nome Unidade'] = isset($e->subsidiaries->name) ? $e->subsidiaries->name : '-';
                $row['Natureza Gasto ERP'] = utf8_decode(isset($e->events->expenseNatures->description) ? $e->events->expenseNatures->description : '-');
                $row['Conta'] = utf8_decode(isset($e->userPaymentForms->paymentForms->description) ? $e->userPaymentForms->paymentForms->description . " ********" . substr($e->userPaymentForms->card_number, -4) : '-');
                $row['Data da despesa'] = toDateBr($e->expense_date, false);
                $row['Valores'] = numbr($e->total);
                $row['Observacao'] = utf8_decode($e->note);
                $row['Gestor'] = utf8_decode(isset($e->users->name) ? $e->users->name : "-");*/

                fputcsv($file, array($row['Evento'], $row['Centro de custo'], $row['Unidade ERP'], $row['Nome Unidade'], $row['Natureza Gasto ERP'], $row['Conta'], $row['Data da despesa'], $row['Valores'], $row['Observacao'], $row['Gestor']), ";", '"');
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $page_title = $this->page_title;
        $adminlte = $this->adminlte;
        $events = $this->repo_events->orderBy("description", "ASC")->get();
        $expense_natures = $this->repo_expense_natures->all();
        $users = $this->repo_users
            ->with(['userAccessGroups'])
            ->select(['users.*'])
            ->join('user_access_groups', 'users.id', '=', 'user_access_groups.user_id')
            ->where('user_access_groups.access_group_module_id', '=', 3)
            ->orderBy("name", "ASC")->get();
        $cost_centers = $this->repo_cost_centers->orderBy("description", "ASC")->get();
        $subsidiaries = $this->repo_subsidiaries->orderBy("name", "ASC")->get();
        $payment_forms = $this->repo_payment_forms->all();
        $entity_user = $this->repo_users->with(['userAccessGroups'])->find($user->id);

        $user_payment_forms = $this->repo_user_payment_forms;
        if ($entity_user->userAccessGroups->access_group_module_id == AccessGroupModules::$ACCESS_GROUP_ADMIN) {
            $user_payment_forms = $user_payment_forms->where('user_id', '=', $user->id);
        } else {
            $user_payment_forms = $user_payment_forms
                ->with(['paymentForms'])
                ->where('user_id', '=', $user->id)
                ->get();
        }

        $user_from = $this->repo_users->orderBy("name", "ASC")->get();
        $user_id = Auth::user()->id;
        return view('admin.expenses.create', compact('page_title', 'adminlte', 'events',
            'expense_natures', 'users', 'user_from', 'cost_centers', 'user_id', 'subsidiaries',
            'payment_forms', 'user_payment_forms'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpensesStoreRequest $request)
    {
        $data = $request->validated();

        /*
        $data['expenses']['unitary']        = str_replace(",",".", str_replace(".","",$data['expenses']['unitary']));
        $data['expenses']['addition']       = str_replace(",",".", str_replace(".","",$data['expenses']['addition']));
        $data['expenses']['discount']       = str_replace(",",".", str_replace(".","",$data['expenses']['discount']));
        $data['expenses']['total']          = str_replace(",",".", str_replace(".","",$data['expenses']['total']));
        $data['expenses']['latitude']       = str_replace(",",".", str_replace(".","",$data['expenses']['latitude']));
        $data['expenses']['longitude']      = str_replace(",",".", str_replace(".","",$data['expenses']['longitude']));
        */
        $data['expenses']['expense_date'] = Carbon::createFromFormat('d/m/Y', $data['expenses']['expense_date']);

        $entity = $this->repository->findOrCreate($data);
        if ($entity) {
            \Session::flash('message', ['class' => 'success', 'message' => 'Registro criado com sucesso.']);
        } else {
            \Session::flash('message', ['class' => 'danger', 'message' => 'Registro não criado.']);
        }
        return redirect()->route('admin.expenses.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function show(Expenses $expenses)
    {
        return $this->sendResponse(new ExpensesResource($expenses), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Expenses $expenses)
    {
        $user = Auth::user();
        $entity = $expenses;
        $page_title = $this->page_title;
        $adminlte = $this->adminlte;
        $events = $this->repo_events->orderBy("description", "ASC")->get();;
        $expense_natures = $this->repo_expense_natures->all();
        $users = $this->repo_users
            ->with(['userAccessGroups'])
            ->join('user_access_groups', 'users.id', '=', 'user_access_groups.user_id')
            ->where('user_access_groups.access_group_module_id', '=', 3)->select(['users.id', 'users.name'])
            ->orderBy("name", "ASC")
            ->get();
        $cost_centers = $this->repo_cost_centers->orderBy("description", "ASC")->get();
        $subsidiaries = $this->repo_subsidiaries->orderBy("name", "ASC")->get();
        $user_payment_forms = $this->repo_user_payment_forms
            ->with(['paymentForms'])
            ->where('user_id', '=', $user->id)
            ->get();
        $payment_forms = $this->repo_payment_forms->all();
        $user_from = $this->repo_users->orderBy("name", "ASC")->get();
        $user_id = Auth::user()->id;

        return view('admin.expenses.edit', compact('entity', 'page_title', 'adminlte', 'expenses', 'events',
            'expense_natures', 'users', 'cost_centers', 'user_id', 'user_from', 'subsidiaries',
            'payment_forms', 'user_payment_forms'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function update(ExpensesUpdateRequest $request, Expenses $expenses)
    {
        $data = $request->validated();
        /*
        $data['expenses']['unitary']        = str_replace(",",".", str_replace(".","",$data['expenses']['unitary']));
        $data['expenses']['addition']       = str_replace(",",".", str_replace(".","",$data['expenses']['addition']));
        $data['expenses']['discount']       = str_replace(",",".", str_replace(".","",$data['expenses']['discount']));
        $data['expenses']['total']          = str_replace(",",".", str_replace(".","",$data['expenses']['total']));
        $data['expenses']['latitude']       = str_replace(",",".", str_replace(".","",$data['expenses']['latitude']));
        $data['expenses']['longitude']      = str_replace(",",".", str_replace(".","",$data['expenses']['longitude']));
        */
        $data['expenses']['expense_date'] = Carbon::createFromFormat('d/m/Y', $data['expenses']['expense_date']);

        $entity = $this->repository->update($expenses->id, $data);

        if ($entity) {
            \Session::flash('message', ['class' => 'success', 'message' => 'Registro atualizado com sucesso.']);
        } else {
            \Session::flash('message', ['class' => 'danger', 'message' => 'Registro não atualizado.']);
        }
        return redirect()->route('admin.expenses.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param \App\Models\General\Expenses $expenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expenses $expenses)
    {
        $entity = null;
        try {
            $entity = ($this->repository)->delete($expenses->id);
        } catch (\Exception $e) {

        }
        if ($entity) {
            \Session::flash('message', ['class' => 'success', 'message' => 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }

        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }

}
