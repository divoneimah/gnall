<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\Subsidiaries;
use App\Http\Resources\SubsidiariesResource as SubsidiariesResource;
use App\Http\Requests\Admin\SubsidiariesStoreRequest;
use App\Http\Requests\Admin\SubsidiariesUpdateRequest;
use App\Repositories\General\SubsidiariesRepository;
use App\Http\Resources\SubsidiariesCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Web - Subsidiaries
 *
 * APIs for managing entities
 */
class SubsidiariesController extends BaseController
{
    private $_order_columns          = ['id', 'name', 'cnpj', 'internal_app_id', 'active'
];

    /**
     * @var AdminLte
     */
    private $adminlte;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte             = $adminlte;
        $this->repository           = (new SubsidiariesRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        if(isset($params['return_json']) && $params['return_json'] == 1){
            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');
            if(isset($search['value'])){
                $params['name']                     = $search['value'];
                $params['active']                   = $search['value'];
                $params['cnpj']                     = $search['value'];
                $params['internal_app_id']          = $search['value'];
            }
            $entities               = $this->repository->filter($params);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            };
            $entities               = $entities->take($this->per_page)->skip($this->start_page)->get();
            $totalRecordswithFilter = $this->repository->filter($params)->count();
            $totalRecords           = $this->repository->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.subsidiaries.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';

                    $_data[] = [$e->id,
                        $e->name,
                        $e->cnpj,
                        $e->internal_app_id,
                        ($e->active)==1?'Ativo':'Inativo',
                        $actions 
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->filter($params)->paginate($this->per_page);
        }
        return view('admin.subsidiaries.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.subsidiaries.create', compact('page_title', 'adminlte'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubsidiariesStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.subsidiaries.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function show(Subsidiaries $subsidiaries)
    {
        return $this->sendResponse(new SubsidiariesResource($subsidiaries), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subsidiaries $subsidiaries)
    {
        $entity                 = $subsidiaries;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        return view('admin.subsidiaries.edit', compact('entity', 'page_title', 'adminlte', 'subsidiaries'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function update(SubsidiariesUpdateRequest $request, Subsidiaries $subsidiaries)
    {
        $data           = $request->validated(); 
        $entity         = $this->repository->update($subsidiaries->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.subsidiaries.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\Subsidiaries  $subsidiaries
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subsidiaries $subsidiaries)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($subsidiaries->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
