<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\Events;
use App\Http\Resources\EventsResource as EventsResource;
use App\Http\Requests\Admin\EventsStoreRequest;
use App\Http\Requests\Admin\EventsUpdateRequest;
use App\Repositories\General\EventsRepository;
use App\Repositories\General\ExpenseNaturesRepository;
use App\Repositories\General\UnitMeasurementsRepository;
use App\Http\Resources\EventsCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Web - Events
 *
 * APIs for managing entities
 */
class EventsController extends BaseController
{
    private $_order_columns = ['id', 'id', 'id', 'description'
    ];
    /**
     * @var AdminLte
     */
    private $adminlte;

    private $_movement_types          = [/*'C' => "Crédito", "D" => "Débitos",*/"E" => "Crédito financeiro","S" => "Débito financeiro", "X" => "Transferência (débito)", "Y" => "Transferência (crédito)"];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        $this->adminlte                         = $adminlte;
        $this->repository                       = (new EventsRepository());
        $this->repo_unit_measurements           = (new UnitMeasurementsRepository());
        $this->repo_expense_natures             = (new ExpenseNaturesRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        if(isset($params['return_json']) && $params['return_json'] == 1){
            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');
            if(isset($search['value'])){
                $params['expense_nature_id']        = $search['value'];
                $params['description']              = $search['value'];
            }
            $entities               = $this->repository->filter($params);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            };
            $entities               = $entities->take($this->per_page)->skip($this->start_page)->get();
            $totalRecordswithFilter = $this->repository->filter($params)->count();
            $totalRecords           = $this->repository->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.events.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';
                    $tipo_movimento = "-";
                    foreach($this->_movement_types as $key => $m){
                            if($key == $e->movement_type){
                                $tipo_movimento = $m;
                            break;
                            }
                    }
                    $_data[] = [$e->id,
                        $e->unitMeasurements->unit_measure_id,
                        $e->expenseNatures->description,
                        $e->description,
                        $tipo_movimento,
                        $actions
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->with(['unitMeasurements'])->filter($params)->paginate($this->per_page);
        }
        return view('admin.events.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities)
            ->with('movement_types', $this->_movement_types);
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title                 = $this->page_title;
        $adminlte                   = $this->adminlte;
        $unit_measurements          = $this->repo_unit_measurements->all();
        $expense_natures            = $this->repo_expense_natures->all();
        $movement_types             = $this->_movement_types;
        return view('admin.events.create', compact('page_title', 'adminlte', 'unit_measurements', 'expense_natures', 'movement_types'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventsStoreRequest $request)
    {
        $data   = $request->validated();
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.events.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function show(Events $events)
    {
        return $this->sendResponse(new EventsResource($events), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Events $events)
    {
        $entity                 = $events;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $unit_measurements          = $this->repo_unit_measurements->all();
        $expense_natures            = $this->repo_expense_natures->all();
        $movement_types             = $this->_movement_types;

        return view('admin.events.edit', compact('entity', 'page_title', 'adminlte', 'events', 'unit_measurements', 'expense_natures', 'movement_types'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function update(EventsUpdateRequest $request, Events $events)
    {
        $data           = $request->validated();
        $entity         = $this->repository->update($events->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.events.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy(Events $events)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($events->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }

        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
