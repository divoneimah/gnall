<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\PaymentForms;
use App\Http\Resources\PaymentFormsResource as PaymentFormsResource;
use App\Http\Requests\Admin\PaymentFormsStoreRequest;
use App\Http\Requests\Admin\PaymentFormsUpdateRequest;
use App\Repositories\General\PaymentFormsRepository;
use App\Repositories\General\UserPaymentFormsRepository;
use App\Http\Resources\PaymentFormsCollection;
use App\Http\Resources\UserPaymentFormsCollection;
use Illuminate\Http\Request;
use JeroenNoten\LaravelAdminLte\AdminLte;

/**
 * @group Web - PaymentForms
 *
 * APIs for managing entities
 */
class PaymentFormsController extends BaseController
{
    private $_account_types          = ['A' => "Adiantamento", "C" => "Cartão"];
    private $_order_columns          = ['id', 'account_type', 'description'
];

    /**
     * @var AdminLte
     */
    private $adminlte;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req, AdminLte $adminlte)
    {
        
        $this->adminlte             = $adminlte;
        $this->repository           = (new PaymentFormsRepository());
        $this->user_payment_forms   = (new UserPaymentFormsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
        if($req->input("start")){
            $this->start_page = (int) $req->input("start");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        if(isset($params['return_json']) && $params['return_json'] == 1){
            $draw                   = $request->input('draw');
            $search                 = $request->input('search');
            $order                  = $request->input('order');
            if(isset($search['value'])){
                $params['cost_center_id']           = $search['value'];
                $params['description']              = $search['value'];
            }
            $entities               = $this->repository->filter($params);
            if(isset($order[0]['column'])){
                if(isset($this->_order_columns[$order[0]['column']])){
                    $order_by   = $this->_order_columns[$order[0]['column']];
                    $order_dir  = $order[0]['dir'];
                    $entities = $entities->orderBy($order_by, $order_dir);
                }
            };
            $entities               = $entities->take($this->per_page)->skip($this->start_page)->get();
            $totalRecordswithFilter = $this->repository->filter($params)->count();
            $totalRecords           = $this->repository->count();
            $_data = [];
            if($entities){
                foreach($entities as $e){
                    $actions = '
                    <div class="btn-group">
                                <a class="btn btn-default" href="'. route("admin.payment_forms.edit", $e->id) .'"><i class="fa fa-fw fa-edit"> </i>Editar</a>
                                <a class="btn btn-default" data-confirmation-event="confirmDelete" data-toggle="confirmation" href="javascript: confirmDelete('. $e->id .')"><i class="fa fa-fw fa-trash"></i>Remover</a>
                            </div>';
                    $account_type = "-";
                    if(isset($this->_account_types[$e->account_type]))
                        $account_type =  $this->_account_types[$e->account_type];
                    
                    $_data[] = [$e->id,
                        $account_type,
                        $e->description,
                        $actions 
                ];
                }
            }
            return $this->sendResponseDataTable($_data, $draw, $totalRecords , $totalRecordswithFilter);
        }else{
            $entities   = $this->repository->filter($params)->paginate($this->per_page);
        }
        return view('admin.payment_forms.index', ['errors'])
            ->with('adminlte', $this->adminlte)
            ->with('entities', $entities)
            ->with('_account_types', $this->_account_types);
    }

    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_manager(Request $request)
    {
        $params     = $request->except('_token');

        $user_id    = 0;
        if(isset($params['user_id'])){
            $user_id = $params['user_id'];
        }
        $entities   = $this->user_payment_forms
                ->with(['paymentForms'])
                ->where('user_id','=', $user_id)
                ->paginate(1000);
        return $this->sendResponse(new UserPaymentFormsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $_account_types         = $this->_account_types;
        return view('admin.payment_forms.create', compact('page_title', 'adminlte', '_account_types'));
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentFormsStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro criado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não criado.']);
        }
        return redirect()->route('admin.payment_forms.index');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentForms $payment_forms)
    {
        return $this->sendResponse(new PaymentFormsResource($payment_forms), 'Registro Resgatado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentForms $payment_forms)
    {
        $entity                 = $payment_forms;
        $page_title             = $this->page_title;
        $adminlte               = $this->adminlte;
        $_account_types         = $this->_account_types;
        return view('admin.payment_forms.edit', compact('entity', 'page_title', 'adminlte', 'payment_forms', '_account_types'));
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentFormsUpdateRequest $request, PaymentForms $payment_forms)
    {
        $data           = $request->validated(); 
        $entity         = $this->repository->update($payment_forms->id, $data);

        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=>'Registro atualizado com sucesso.']);
        }else{
            \Session::flash('message',['class' => 'danger', 'message'=>'Registro não atualizado.']);
        }
        return redirect()->route('admin.payment_forms.index');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\PaymentForms  $payment_forms
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentForms $payment_forms)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($payment_forms->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
