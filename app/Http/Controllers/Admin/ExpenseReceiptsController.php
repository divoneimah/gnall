<?php

namespace App\Http\Controllers\Admin;

use App\Base\BaseController;
use App\Models\General\ExpenseReceipts;
use App\Http\Resources\ExpenseReceiptsResource as ExpenseReceiptsResource;
use App\Http\Requests\Api\ExpenseReceiptsStoreRequest;
use App\Http\Requests\Api\ExpenseReceiptsUpdateRequest;
use App\Repositories\General\ExpenseReceiptsRepository;
use App\Http\Resources\ExpenseReceiptsCollection;
use Illuminate\Http\Request;

/**
 * @group Web - ExpenseReceipts
 *
 * APIs for managing entities
 */
class ExpenseReceiptsController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        $this->repository           = (new ExpenseReceiptsRepository());
        if($req->input("per_page")){
            $this->per_page = (int) $req->input("per_page");
        }
    }
    
    /**
     * List - Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params     = $request->except('_token');

        $entities   = $this->repository->filter($params)->paginate($this->per_page);
        return $this->sendResponse(new ExpenseReceiptsCollection($entities), 'Entities retrieved successfully.');
    }

    /**
     * POST - Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseReceiptsStoreRequest $request)
    {
        $data   = $request->validated();    
        $entity = $this->repository->findOrCreate($data);
        if($entity){
            return $this->sendResponse(new ExpenseReceiptsResource($entity), 'Entity created successfully.');
        }
        return $this->sendError([$this->repository->getStringErrors()], 'Failed to create entity');
    }

    /**
     * GET - Display the specified resource.
     *
     * @param  \App\Models\General\ExpenseReceipts  $expense_receipts
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseReceipts $expense_receipts)
    {
        return $this->sendResponse(new ExpenseReceiptsResource($expense_receipts), 'Registro Resgatado com sucesso');
    }

    /**
     * PUT - Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\General\ExpenseReceipts  $expense_receipts
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseReceiptsUpdateRequest $request, ExpenseReceipts $expense_receipts)
    {
        $data           = $request->validated(); 
        $entity         = $this->repository->update($expense_receipts->id, $data);

        return $this->sendResponse(new ExpenseReceiptsResource($entity), 'Entity updated successfully.');
    }

    /**
     * DELETE - Remove the specified resource from storage.
     *
     * @param  \App\Models\General\ExpenseReceipts  $expense_receipts
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseReceipts $expense_receipts)
    {
        $entity = null;
        try{
            $entity = ($this->repository)->delete($expense_receipts->id);
        }catch(\Exception $e){

        }
        if($entity){
            \Session::flash('message',['class' => 'success', 'message'=> 'Registro removido com sucesso']);
            return $this->sendResponse([], 'Registro removido com sucesso');
        }
            
        return $this->sendError($this->repository->getErrors(), 'Falha ao remover o registro');
    }
}
