<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class UnitMeasurementsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;

        $_data = [
                'id'                    => $this->id,
                'unit_measure_id'       => $this->unit_measure_id,
                'description'           => $this->description,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
            ];

        return $_data;

    }

}
