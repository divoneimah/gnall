<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class EventReceiptsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;

        $_data = [
                'id'                    => $this->id,
                'event_id'              => $this->event_id,
                'description'           => $this->description,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id,
                'deleted_at'            => $this->deleted_at,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
        ];

        return $_data;

    }

}
