<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class UserPaymentFormsLeanResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;
        
        $_data = [
                'id'                => $this->id,
                'card_number_last'  => "******". substr($this->card_number, -4),
                'card_limit'        => $this->card_limit,
        ];
        if(isset($this->paymentForms->description)){
            $_data['payment_form_name'] = $this->paymentForms->description;
        }
        
        return $_data;

    }

}