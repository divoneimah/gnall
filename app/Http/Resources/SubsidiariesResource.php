<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class SubsidiariesResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;

        $_data = [
                'id'                    => $this->id,
                'name'                  => $this->name,
                'cnpj'                  => $this->cnpj,
                'internal_app_id'       => $this->internal_app_id,
                'active'                => $this->active,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id,
                'deleted_at'            => $this->deleted_at,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
            ];

        return $_data;

    }

}
