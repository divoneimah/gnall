<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseReceiptsResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;

        $_data = [
                'id'                    => $this->id,
                'expense_id'            => $this->expense_id,
                'event_id'              => $this->event_id,
                'receipt_id'            => $this->receipt_id,
                'document_path'         => $this->document_path,
                'server_path'           => $this->server_path,
                'internal_app_id'       => $this->internal_app_id,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
                'deleted_at'            => $this->deleted_at,
        ];
        return $_data;

    }

}
