<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpensesResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;
        $deleted_at = ($this->deleted_at != null)?$this->deleted_at->format('d/m/Y'):null;
        
        $_data = [
                'id'                    => $this->id,
                'user_id'               => $this->user_id,
                'event_id'              => $this->event_id,
                'cost_center_id'        => $this->cost_center_id,
                'expense_date'          => $this->expense_date,
                'quantity'              => $this->quantity,
                'unitary'               => $this->unitary,
                'addition'              => $this->addition,
                'discount'              => $this->discount,
                'total'                 => $this->total,
                'note'                  => $this->note,
                'longitude'             => $this->longitude,
                'latitude'              => $this->latitude,
                'user_from_id'          => $this->user_from_id,
                'subsidiary_id'         => $this->subsidiary_id,
                'payment_form_id'       => $this->payment_form_id,
                'internal_app_id'       => $this->internal_app_id,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
                'deleted_at'            => $this->deleted_at,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id,
                'user_payment_form'     => null,
        ];
        if(isset($this->userPaymentForms)){
            $_data['user_payment_form'] = new UserPaymentFormsLeanResource($this->userPaymentForms);
        }
        return $_data;

    }

}