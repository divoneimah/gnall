<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class TransferResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->id)){ return null; }
        $created_at = ($this->created_at != null)?$this->created_at->format('d/m/Y'):null;
        $updated_at = ($this->updated_at != null)?$this->updated_at->format('d/m/Y'):null;
        $deleted_at = ($this->deleted_at != null)?$this->deleted_at->format('d/m/Y'):null;

        $_data = [
                'id'                    => $this->id,
                'user_id'               => $this->user_id,
                'id_debit_account'      => $this->id_debit_account,
                'id_credit_account'     => $this->id_credit_account,
                'valor'                 => $this->valor,
                'subsidiary_id'         => $this->subsidiary_id,
                'internal_app_id'       => $this->internal_app_id,
                'created_at'            => $this->created_at,
                'updated_at'            => $this->updated_at,
                'deleted_at'            => $this->deleted_at,
                'created_user_id'       => $this->created_user_id,
                'updated_user_id'       => $this->updated_user_id
        ];
        return $_data;

    }

}
