<?php

namespace App\Http\Requests\Admin;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;
use App\Http\Requests\Api\ControlPointsStoreRequest as ApiControlPointsStoreRequest;

class PaymentFormsStoreRequest extends ApiControlPointsStoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'payment_forms.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'account_type'              => ['nullable', 'string', 'size:1'],
            $entity.'description'               => ['nullable', 'string']
        ];
        return $_validation;
    }
}
