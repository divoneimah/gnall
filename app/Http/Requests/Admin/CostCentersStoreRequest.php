<?php

namespace App\Http\Requests\Admin;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;
use App\Http\Requests\Api\CostCentersStoreRequest as ApiCostCentersStoreRequest;

class CostCentersStoreRequest extends ApiCostCentersStoreRequest
{
    
    /**
    * Handle a failed validation attempt.
    *
    * @param  \Illuminate\Contracts\Validation\Validator  $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    function failedValidation(Validator $validator)
    {
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                $_messages[$key] = $e[0];
                
            }
        }

        Session::flash('message', $_messages); 
        Session::flash('alert-class', 'alert-danger'); 
        
        throw (new ValidationException($validator))
                    //->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }

}
