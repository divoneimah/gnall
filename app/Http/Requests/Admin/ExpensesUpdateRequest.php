<?php

namespace App\Http\Requests\Admin;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;
use App\Http\Requests\Api\ExpensesStoreRequest as ApiExpensesStoreRequest;

class ExpensesUpdateRequest extends ApiExpensesStoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'expenses.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                       => ['nullable', 'exists:users,id'],
            $entity.'event_id'                      => ['nullable', 'exists:events,id'],
            $entity.'cost_center_id'                => ['nullable', 'exists:cost_centers,id'],
            $entity.'expense_date'                  => ['nullable', 'date_format:d/m/Y'],
            $entity.'quantity'                      => ['nullable', 'numeric'],
            $entity.'unitary'                       => ['nullable', 'numeric'],
            $entity.'addition'                      => ['nullable', 'numeric'],
            $entity.'discount'                      => ['nullable', 'numeric'],
            $entity.'total'                         => ['nullable', 'numeric'],
            $entity.'longitude'                     => ['nullable', 'string'],
            $entity.'latitude'                      => ['nullable', 'string'],
            $entity.'user_from_id'                  => ['nullable', 'exists:users,id'],
            $entity.'note'                          => ['nullable', 'string'],
            $entity.'internal_app_id'               => ['nullable', 'string'],
            $entity.'user_payment_form_id'          => ['nullable', 'exists:user_payment_forms,id'],
            $entity.'subsidiary_id'                 => ['nullable', 'exists:subsidiaries,id'],

        ];
        return $_validation;
    }

    /**
    * Handle a failed validation attempt.
    *
    * @param  \Illuminate\Contracts\Validation\Validator  $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    function failedValidation(Validator $validator)
    {
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                $_messages[$key] = $e[0];
                
            }
        }

        Session::flash('message', $_messages); 
        Session::flash('alert-class', 'alert-danger'); 
        
        throw (new ValidationException($validator))
                    //->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }

}
