<?php

namespace App\Http\Requests\Admin;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;
use App\Http\Requests\Api\UsersStoreRequest as ApiUsersStoreRequest;

class UsersStoreRequest extends ApiUsersStoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'users.';
        $entity_pf          = 'user_payment_forms.*.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();
        $ignore_login       = (int) (isset($_data['users']['id'])?$_data['users']['id']:-1);

        $_validation        = [
            $entity.'name'                  => ['required', 'string' ],
            $entity.'user_id'               => ['nullable', 'numeric' ],
            $entity.'email'                 => ['required', 'email', 'unique:users,email,'.$ignore_login],
            $entity.'cost_center_id'        => ['exists:cost_centers,id'],
            $entity.'subsidiary_id'         => ['exists:subsidiaries,id'],
            $entity.'departament_id'        => ['nullable', 'exists:departaments,id'],
            $entity.'access_group_module_id'=> ['exists:access_group_modules,id'],

            // Login
            $entity.'id'                    => ['integer'],

            $entity_pf ."payment_form_id"   => ['exists:payment_forms,id'],
            $entity_pf ."card_number"       => ['nullable'],
            $entity_pf ."card_limit"        => ['nullable']

        ];
        if(isset($_data['users']['password']) && $_data['users']['password'] != ""){
            $_validation    = array_merge($_validation, [$entity.'password'        => ['string', 'confirmed']]);
        }
        return $_validation;
    }

    /**
     * Handle a failed validation attempt.
    *
    * @param  \Illuminate\Contracts\Validation\Validator  $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    function failedValidation(Validator $validator)
    {
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                $_messages[$key] = $e[0];
                
            }
        }
        Session::flash('message', $_messages); 
        Session::flash('alert-class', 'alert-danger'); 
        
        throw (new ValidationException($validator))
                    //->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }

}
