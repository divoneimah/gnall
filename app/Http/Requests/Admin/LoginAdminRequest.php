<?php

namespace App\Http\Requests\Admin;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;

class LoginAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity = 'login.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();
        
        return [
            $entity.'email'     => ['required', 'string'],
            $entity.'password'  => ['required', 'string'],
        ];
    }

    /**
     * To receive a json data from POST/PUT
     * Ref: https://gist.github.com/vistik/0da195da9b4441401c15
     */
    public function validationData()
    {   
        return count($this->json()->all()) ? $this->json()->all() : ($this->all());
    }

    /**
     * Handle a failed validation attempt.
    *
    * @param  \Illuminate\Contracts\Validation\Validator  $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    function failedValidation(Validator $validator)
    {
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                $_messages[$key] = $e[0];
                
            }
        }

        Session::flash('message', $_messages); 
        Session::flash('alert-class', 'alert-danger'); 
        
        throw (new ValidationException($validator))
                    //->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }
}
