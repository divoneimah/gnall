<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Session;

class AdminUsersStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'admin_users.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();
        $ignore_login       = (int) (isset($_data['admin_users']['id'])?$_data['admin_users']['id']:-1);

        $_validation        = [
            $entity.'name'                  => ['required', 'string' ],
            $entity.'email'                 => ['required', 'email', 'unique:admin_users,email,'.$ignore_login],

            // Login
            $entity.'id'                    => ['integer'],


        ];
        if(isset($_data['admin_users']['password']) && $_data['admin_users']['password'] != ""){
            $_validation    = array_merge($_validation, [$entity.'password'        => ['string', 'confirmed']]);
        }
        return $_validation;
    }
}
