<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class PaymentFormsUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'payment_forms.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'account_type'              => ['nullable', 'string', 'size:1'],
            $entity.'description'               => ['nullable', 'string']
        ];
        return $_validation;
    }

}
