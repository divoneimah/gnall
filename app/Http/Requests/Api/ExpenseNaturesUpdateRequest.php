<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class ExpenseNaturesUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'expense_natures.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'expense_nature_id'             => ['nullable', 'string','max:9'],
            $entity.'description'                   => ['nullable', 'string']
        ];
        return $_validation;
    }

}
