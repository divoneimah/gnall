<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class UserPaymentFormsUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'user_payment_forms.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                  => ['required', 'exists:users,id' ],
            $entity.'payment_form_id'          => ['required', 'exists:payment_forms,id'],
            $entity.'card_number'              => ['nullable', 'string'],
            $entity.'card_limit'               => ['nullable', 'numeric']

        ];
        return $_validation;
    }

}
