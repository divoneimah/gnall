<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class ExpenseReceiptsStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'expense_receipts.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'expense_id'                    => ['nullable', 'exists:expenses,id'],
            $entity.'event_id'                      => ['nullable', 'exists:events,id'],
            $entity.'receipt_id'                    => ['nullable', 'numeric'],
            $entity.'document_path'                 => ['nullable', 'string'],
            //$entity.'server_path'                   => ['nullable', 'string'],
            $entity.'image'                         => ['file'],
            $entity.'internal_app_id'               => ['nullable', 'string'],

        ];
        return $_validation;
    }

}
