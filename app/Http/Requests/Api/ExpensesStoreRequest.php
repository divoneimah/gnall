<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class ExpensesStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'expenses.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                   => ['nullable', 'exists:users,id'],
            $entity.'event_id'                      => ['nullable', 'exists:events,id'],
            $entity.'cost_center_id'                => ['nullable', 'exists:cost_centers,id'],
            $entity.'expense_date'                  => ['nullable', 'date_format:Y-m-d'],
            $entity.'quantity'                      => ['nullable', 'numeric'],
            $entity.'unitary'                       => ['nullable', 'numeric'],
            $entity.'addition'                      => ['nullable', 'numeric'],
            $entity.'discount'                      => ['nullable', 'numeric'],
            $entity.'total'                         => ['nullable', 'numeric'],
            $entity.'longitude'                     => ['nullable', 'string'],
            $entity.'latitude'                      => ['nullable', 'string'],
            $entity.'user_from_id'                  => ['nullable', 'exists:users,id'],
            $entity.'note'                          => ['nullable', 'string'],
            $entity.'internal_app_id'               => ['nullable', 'string'],
            $entity.'payment_form_id'               => ['nullable', 'exists:payment_forms,id'],
            $entity.'user_payment_form_id'          => ['nullable', 'exists:user_payment_forms,id'],
            $entity.'subsidiary_id'                 => ['nullable', 'exists:subsidiaries,id']
        ];
        return $_validation;
    }
}
