<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class EventReceiptsStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'event_receipts.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'event_id'                      => ['nullable', 'exists:events,id'],
            $entity.'description'                   => ['nullable', 'string']
        ];
        return $_validation;
    }

}
