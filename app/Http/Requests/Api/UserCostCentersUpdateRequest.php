<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class UserCostCentersUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'user_cost_centers.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();
        $ignore_login       = (int) (isset($_data['users']['id'])?$_data['users']['id']:-1);

        $_validation        = [
            $entity.'user_id'                  => ['required', 'exists:users,id' ],
            $entity.'cost_center_id'           => ['required', 'exists:cost_centers,id']

        ];
        return $_validation;
    }

}
