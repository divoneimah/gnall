<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class CostCentersUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'cost_centers.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'cost_center_id'                => ['nullable', 'string'],
            $entity.'description'                   => ['nullable', 'string']
        ];
        return $_validation;
    }

}
