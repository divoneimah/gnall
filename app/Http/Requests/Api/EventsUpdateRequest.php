<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class EventsUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'events.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'unit_measure_id'               => ['nullable', 'exists:unit_measurements,id'],
            $entity.'description'                   => ['nullable', 'string'],
            $entity.'expense_nature_id'             => ['nullable', 'exists:expense_natures,expense_nature_id'],
            $entity.'movement_type'                 => ['nullable', 'string', 'size:1']
        ];
        return $_validation;
    }

}
