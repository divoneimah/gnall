<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class TransferUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'transfer.';

        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                   => ['nullable', 'exists:users,id'],
            $entity.'id_debit_account'          => ['nullable', 'numeric'],
            $entity.'id_credit_account'         => ['nullable', 'numeric'],
            $entity.'valor'                     => ['nullable', 'numeric'],
            $entity.'subsidiary_id'             => ['nullable', 'numeric'],
            $entity.'internal_app_id'           => ['nullable', 'string']
        ];
        return $_validation;
    }

}
