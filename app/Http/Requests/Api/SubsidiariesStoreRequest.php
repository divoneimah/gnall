<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;
use App\Rules\CnpjRule;

class SubsidiariesStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'subsidiaries.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'name'                          => ['nullable', 'string'],
            $entity.'cnpj'                          => ['required', 'string', new CnpjRule],
            $entity.'internal_app_id'               => ['nullable', 'string'],
            $entity.'active'                        => ['nullable', 'integer']
        ];
        return $_validation;
    }

}
