<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;
use App\Rules\CnpjRule;

class DepartamentsStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'departaments.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'description'                   => ['nullable', 'string'],
        ];
        return $_validation;
    }

}
