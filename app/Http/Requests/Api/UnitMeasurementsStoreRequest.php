<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class UnitMeasurementsStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'unit_measurements.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'unit_measure_id'               => ['nullable', 'string'],
            $entity.'description'                   => ['nullable', 'string']
        ];
        return $_validation;
    }

}
