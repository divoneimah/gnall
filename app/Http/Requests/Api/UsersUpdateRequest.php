<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class UsersUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'users.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();
        $ignore_login       = (int) (isset($_data['users']['id'])?$_data['users']['id']:-1);

        $_validation        = [
            $entity.'name'                  => ['required', 'string' ],
            $entity.'user_id'               => ['nullable', 'numeric' ],
            $entity.'email'                 => ['required', 'email', 'unique:users,email,'.$ignore_login],

            // Login
            $entity.'id'                    => ['integer'],


        ];
        if(isset($_data['users']['password']) && $_data['users']['password'] != ""){
            $_validation    = array_merge($_validation, [$entity.'password'        => ['string', 'confirmed']]);
        }
        return $_validation;
    }

}
