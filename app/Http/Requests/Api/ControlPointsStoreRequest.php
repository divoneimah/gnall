<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class ControlPointsStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity                                 = 'control_points.';
        
        $_data                                  = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                   => ['nullable', 'exists:users,id'],
            $entity.'longitude'                 => ['nullable', 'string'],
            $entity.'latitude'                  => ['nullable', 'string'],
            //$entity.'created_user_id'           => ['nullable', 'exists:users,id'],
            //$entity.'updated_user_id'           => ['nullable', 'exists:users,id'],
            $entity.'note'                      => ['nullable', 'string'],
            
        ];
        return $_validation;
    }

}
