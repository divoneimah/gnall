<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity = 'login.';

        return [
            $entity.'email'     => ['required', 'string'],
            $entity.'password'  => ['required', 'string'],

        ];
    }

    /**
     * To receive a json data from POST/PUT
     * Ref: https://gist.github.com/vistik/0da195da9b4441401c15
     */
    public function validationData()
    {   
        return $this->json()->all();
    }

    function failedValidation(Validator $validator) { 
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                        $_messages[$key] = $e;
                
            }
        }
        throw new HttpResponseException(
        response()->json($response = [
            'success' => false,
            'message' => $_messages,
            'code' => BaseRequest::$STATUS_RESPONSE_CODE_BAD_REQUEST
        ], BaseRequest::$STATUS_RESPONSE_CODE_BAD_REQUEST));
    }
}
