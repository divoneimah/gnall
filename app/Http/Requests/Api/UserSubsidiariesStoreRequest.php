<?php

namespace App\Http\Requests\Api;

use App\Base\BaseRequest;

class UserSubsidiariesStoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $entity             = 'user_subsidiaries.';
        $_data              = count($this->json()->all()) ? $this->json()->all() : $this->all();

        $_validation        = [
            $entity.'user_id'                  => ['required', 'exists:users,id' ],
            $entity.'subsidiary_id'            => ['required', 'exists:subsidiaries,id']

        ];
        return $_validation;
    }

}
