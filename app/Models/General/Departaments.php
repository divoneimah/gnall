<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property string $description
 * @property User[] $users
 */
class Departaments extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['description', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Models\General\Users');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', '%'. trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
