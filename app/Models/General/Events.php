<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $unit_measure_id
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property UnitMeasurement $unitMeasurement
 * @property EventExpenseNature[] $eventExpenseNatures
 * @property EventReceipt[] $eventReceipts
 * @property ExpenseReceipt[] $expenseReceipts
 * @property Expense[] $expenses
 */
class Events extends BaseModel
{
    use SoftDeletes;
    public static $EXPENSE_MOVEMENT_TYPE_CREDIT             = 'C';
    public static $EXPENSE_MOVEMENT_TYPE_DEBIT              = 'D';

    /**
     * @var array
     */
    protected $fillable = ['id', 'unit_measure_id', 'created_user_id', 'updated_user_id', 'expense_nature_id', 'movement_type', 'description', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitMeasurements()
    {
        return $this->belongsTo('App\Models\General\UnitMeasurements', 'unit_measure_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenseNatures()
    {
        return $this->belongsTo('App\Models\General\ExpenseNatures', 'expense_nature_id', 'expense_nature_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventReceipts()
    {
        return $this->hasMany('App\Models\General\EventReceipts', 'event_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenseReceipts()
    {
        return $this->hasMany('App\Models\General\ExpenseReceipts');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses()
    {
        return $this->hasMany('App\Models\General\Expenses');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', '%'. trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
