<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $event_id
 * @property int $expense_nature_id
 * @property Event $event
 * @property ExpenseNature $expenseNature
 */
class EventExpenseNatures extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['event_id', 'expense_nature_id', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Models\General\Event');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expenseNature()
    {
        return $this->belongsTo('App\Models\General\ExpenseNature');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
