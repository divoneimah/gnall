<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property int $user_id
 * @property string $start_date
 * @property string $end_date
 * @property User $user
 */
class ControlPoints extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'users.name', 'longitude', 'latitude', 'created_user_id', 'updated_user_id', 'note', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Models\General\Users', "user_id", "id");
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdUsers()
    {
        return $this->belongsTo('App\Models\General\Users', "created_user_id", "id");
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedUsers()
    {
        return $this->belongsTo('App\Models\General\Users', "updated_user_id", "id");
    }
}
