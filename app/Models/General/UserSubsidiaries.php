<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $user_id
 * @property int $subsidiary_id
 * @property Subsidiary $subsidiary
 * @property User $user
 */
class UserSubsidiaries extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'subsidiary_id', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subsidiary()
    {
        return $this->belongsTo('App\Models\General\Subsidiary');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\General\User');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
