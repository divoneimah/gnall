<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property int $event_id
 * @property int $cost_center_id
 * @property string $expense_date
 * @property float $quantity
 * @property float $unitary
 * @property float $addition
 * @property float $discount
 * @property float $total
 * @property string $note
 * @property CostCenter $costCenter
 * @property Event $event
 * @property ExpenseReceipt[] $expenseReceipts
 */
class Expenses extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['id', 'event_id', 'user_id', 'initial_date', 'final_date', 'cost_center_id', 'expense_date', 'quantity', 'unitary', 'addition', 'discount', 'total', 'note', 'longitude', 'latitude', 'user_from_id', 'internal_app_id', 'subsidiary_id', 'payment_form_id', 'user_payment_form_id', 'created_user_id', 'updated_user_id', 'created_at','updated_at','deleted_at','expense_id_from'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function costCenters()
    {
        return $this->belongsTo('App\Models\General\CostCenters', 'cost_center_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function events()
    {
        return $this->belongsTo('App\Models\General\Events', 'event_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Models\General\Users', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usersFrom()
    {
        return $this->belongsTo('App\Models\General\Users', 'user_from_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPaymentForms()
    {
        return $this->belongsTo('App\Models\General\UserPaymentForms', 'user_payment_form_id','id');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if($key == "initial_date"){
                    if(isset($params['final_date'])){
                        $initial_date = $params['initial_date'];
                        $final_date   = $params['final_date'];
                        $query->whereBetween("expense_date", [$initial_date, $final_date]
                        );
                    }else{
                        $query->where("expense_date", '>=', $data);
                    }

                    continue;
                }
                if($key == "final_date"){
                    if(!isset($params['initial_date'])){
                    //    $query->where("expense_date", '<=', $data);
                    }
                    continue;
                }
                if($key == "user_id"){
                    if($data != null){
                        $query->where("user_id", '=', $data);

                    }
                    continue;
                }
                if($key == "departament_id"){
                    if($data != null){
                        //$query->where("users.departament_id", '=', $data);

                    }
                    continue;
                }
                if($key == 'user_payment_form_id'){
                    if($data != null){
                        $query->where("user_payment_form_id", '=', $data);
                    }
                    continue;
                }
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenseReceipts()
    {
        return $this->hasMany('App\Models\General\ExpenseReceipts', 'expense_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentForms()
    {
        return $this->belongsTo('App\Models\General\PaymentForms', 'payment_form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subsidiaries()
    {
        return $this->belongsTo('App\Models\General\Subsidiaries', 'subsidiary_id');
    }

}
