<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Event[] $events
 */
class UnitMeasurements extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'description', 'unit_measure_id', 'created_user_id', 'updated_user_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('App\Models\General\Event', 'unit_measure_id');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', '%'. trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
