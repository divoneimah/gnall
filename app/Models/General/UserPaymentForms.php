<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $user_id
 * @property int $payment_form_id
 * @property string $card_number
 * @property PaymentForm $paymentForm
 * @property User $user
 */
class UserPaymentForms extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'payment_form_id', 'card_number', 'card_limit', 'created_user_id', 'updated_user_id' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentForms()
    {
        return $this->belongsTo('App\Models\General\PaymentForms', 'payment_form_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Models\General\Users');
    }
    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
