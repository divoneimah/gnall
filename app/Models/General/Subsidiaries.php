<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $cnpj
 * @property boolean $active
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property UserSubsidiary[] $userSubsidiaries
 */
class Subsidiaries extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'cnpj', 'active', 'internal_app_id', 'created_user_id', 'updated_user_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userSubsidiaries()
    {
        return $this->hasMany('App\Models\General\UserSubsidiary');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%".trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
