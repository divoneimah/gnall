<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property EventExpenseNature[] $eventExpenseNatures
 */
class ExpenseNatures extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['id', 'description', 'created_user_id', 'updated_user_id', 'expense_nature_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventExpenseNatures()
    {
        return $this->hasMany('App\Models\General\EventExpenseNature');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
