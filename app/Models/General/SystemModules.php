<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 */
class SystemModules extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['code'];
    public $timestamps = false;

    public static $SM_ADMIN_USERS_INDEX         = 'admin_users';
    public static $SM_CONTROL_POINTS_INDEX      = 'control_points';
    public static $SM_COST_CENTERS_INDEX        = 'cost_centers';
    public static $SM_DEPARTAMENTS_INDEX        = 'departaments';
    public static $SM_EVENTS_INDEX              = 'events';
    public static $SM_UNIT_MEASUREMENTS_INDEX   = 'unit_measurements';
    public static $SM_EXPENSE_NATURES_INDEX     = 'expense_natures';
    public static $SM_PAYMENT_FORMS_INDEX       = 'payment_forms';
    public static $SM_SUBSIDIARIES_INDEX        = 'subsidiaries';
    public static $SM_EXPENSES_INDEX            = 'expenses';
    public static $SM_USERS_INDEX               = 'users';

}
