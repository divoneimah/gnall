<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $expense_id
 * @property int $event_id
 * @property int $cost_center_id
 * @property string $expense_date
 * @property int $receipt_id
 * @property int $sequencial_receipt
 * @property string $document_path
 * @property CostCenter $costCenter
 * @property Event $event
 * @property Expense $expense
 */
class ExpenseReceipts extends BaseModel
{
    use SoftDeletes;
    public static $EXPENSE_RECEIPTS_FILE_DIRECTORY      = 'public/expense_receipts';
    public static $EXPENSE_RECEIPTS_RESIZE_W            = 200;
    public static $EXPENSE_RECEIPTS_RESIZE_H            = 400;

    /**
     * @var array
     */
    protected $fillable = ['expense_id', 'event_id', 'receipt_id', 'document_path', 'server_path', 'internal_app_id', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Models\General\Event');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expense()
    {
        return $this->belongsTo('App\Models\General\Expense');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
