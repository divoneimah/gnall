<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $email
 * @property string $token
 * @property string $created_at
 */
class PasswordResets extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['email', 'token', 'created_at'];

}
