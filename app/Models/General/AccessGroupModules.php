<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property string $name
 * @property string $system_modules
 * @property UserGroup[] $userGroups
 */
class AccessGroupModules extends BaseModel
{
    public static $ACCESS_GROUP_ADMIN               = 1;
    public static $ACCESS_GROUP_MANAGER             = 3;

    /**
     * @var array
     */
    protected $fillable = ['name', 'system_modules', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userGroups()
    {
        return $this->hasMany('App\Models\General\UserGroup');
    }
}
