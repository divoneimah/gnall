<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

/**

 */
class Transfer extends BaseModel
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'id_debit_account', 'id_credit_account', 'valor', 'internal_app_id', 'created_user_id', 'subsidiary_id', 'updated_user_id', 'deleted_user_id', 'created_at','updated_at','deleted_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Models\General\Users', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usersFrom()
    {
        return $this->belongsTo('App\Models\General\Users', 'user_from_id');
    }


    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){

                if($key == "user_id"){
                    if($data != null){
                        $query->where("user_id", '=', $data);

                    }
                    continue;
                }

                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }



}
