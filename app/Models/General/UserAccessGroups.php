<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property int $user_id
 * @property int $access_group_module_id
 * @property AccessGroupModule $accessGroupModule
 * @property User $user
 */
class UserAccessGroups extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'access_group_module_id', 'created_at', 'updated_at', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accessGroupModules()
    {
        return $this->belongsTo('App\Models\General\AccessGroupModules', 'access_group_module_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\General\User');
    }
}
