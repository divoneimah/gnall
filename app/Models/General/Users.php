<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Base\BaseModel;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $api_token
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property ControlPoint[] $controlPoints
 * @property UserCostCenter[] $userCostCenters
 * @property UserSubsidiary[] $userSubsidiaries
 */
class Users  extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'name', 'email', 'email_verified_at', 'password', 'api_token', 'remember_token'
        , 'created_at', 'updated_at', 'departament_id', 'created_user_id', 'updated_user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function controlPoints()
    {
        return $this->hasMany('App\Models\General\ControlPoint');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCostCenters()
    {
        return $this->hasMany('App\Models\General\UserCostCenters', 'user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userSubsidiaries()
    {
        return $this->hasMany('App\Models\General\UserSubsidiaries', 'user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPaymentForms()
    {
        return $this->hasMany('App\Models\General\UserPaymentForms', 'user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userAccessGroups()
    {
        return $this->hasOne('App\Models\General\UserAccessGroups', 'user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function departaments()
    {
        return $this->belongsTo('App\Models\General\Departaments', 'departament_id', 'id');
    }

    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->orWhere($key, 'LIKE', "%". trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
