<?php

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $user_id
 * @property string $type_access
 * @property string $ip
 * @property string $login_at
 */


class UserLoginLog extends Model
{
    public $timestamps = false;
    use HasFactory;
    protected $table = 'user_login_logs';
    protected $fillable = ['user_id','type_access','ip'];
}

