<?php

namespace App\Base;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    public static $IMAGE_ORIGINAL_DIRECTORY = '/original';
    public static $IMAGE_RESIZED_DIRECTORY  = '/resized';


    public function scopeFilter($query, $params)
    {
        if(is_array($params)){
            foreach($params as $key => $data){
                if(in_array($key, $this->fillable) && trim($params[$key] !== '')){
                    $query->where($key, 'LIKE', trim($params[$key]) . '%');
                }
            }
        }
        return $query;
    }
}
