<?php

namespace App\Base;
use Illuminate\Support\Facades\Auth;

abstract class BaseRepository
{
    public $model;
    protected $_errors;

    public $order_by            = null;
    public $order_by_direction  = 'asc';

    public function __construct()
    {
        $this->_errors = null;
        $this->getModel();
    }

    public function addErrors($error=null){
        $this->_errors[] = $error;
    }

    public function getErrors(){
        return $this->_errors;
    }

    public function hasErrors(){
        if($this->_errors == null){ return 0;}
        return count($this->_errors);
    }

    public function getStringErrors(){
        $message = null;
        if($this->_errors !=null && is_array($this->_errors)){
            foreach($this->_errors as $e){
                $message .= $e."\r\n";
            }
        }
        return $message;
    }

    abstract public function getModel();

    public function all($data = [])
    {
        return $this->model->with($data)->get();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->model->where($column, $operator, $value, $boolean);
    }

    public function orderBy($column, $direction = 'asc')
    {
        return $this->model->orderby($column, $direction);
    }

    public function get($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    public function update($id, $data)
    {
        $object = $this->model->find($id);
        if(!$object){
            return null;
        }
        $object->fill($data);
        $object->save();
        return $object;
    }

    public function delete($id)
    {
        $model = $this->model->find($id);
        $model->delete();
        return $model;
    }

    public function filter($param=null){
        if($param == null) return $this->model;
        return $this->model->filter($param);
    }

        public function find($id, $with = [])
    {
        return $this->model->with($with)->findOrFail($id);
    }

    public function first()
    {
        return $this->model->first();
    }

    public function findBy($column, $value, $with = [], $operator = '=')
    {
        return $this->model->with($with)->where($column, $operator, $value);
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function paginate($value)
    {
        if($this->order_by != null){
            $this->model = $this->orderBy($this->order_by, $this->order_by_direction);
        }
        return $this->model->paginate($value);
    }

    public function count()
    {
        return $this->model->count();
    }

    public function findOrCreate($data=null)
    {
        return $this->model->firstOrCreate($data);
    }

    public function setCreatedUpdatedUser($_data=null, $created=0){
        $user                       = Auth::user();
        if($created == 1){
            $_data['created_user_id']   = $user->id;
        }
        $_data['updated_user_id']   = $user->id;
        return $_data;
    }
}
