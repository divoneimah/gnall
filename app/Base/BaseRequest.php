<?php

namespace App\Base;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator; 
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{

    public static $STATUS_RESPONSE_CODE_OK                          = 200;
    public static $STATUS_RESPONSE_CODE_NOT_FOUND                   = 404;
    public static $STATUS_RESPONSE_CODE_BAD_REQUEST                 = 400;
    public static $STATUS_RESPONSE_CODE_UNPROCESSABLE_ENTITY        = 422;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       
    }

    /**
     * To receive a json data from POST/PUT
     * Ref: https://gist.github.com/vistik/0da195da9b4441401c15
     */
    public function validationData()
    {   
        return count($this->json()->all()) ? $this->json()->all() : $this->all();
    }

    function failedValidation(Validator $validator) { 
        $errors         = $validator->errors();
        
        
        $_msg_errors    = $errors->getMessages();
        $_messages = null;
        if(is_array($_msg_errors)){
            foreach($_msg_errors as $key => $e){
                        $_messages[$key] = $e;
                
            }
        }
        throw new HttpResponseException(
        response()->json($response = [
            'success' => false,
            'message' => $_messages,
            'code' => BaseRequest::$STATUS_RESPONSE_CODE_UNPROCESSABLE_ENTITY
        ], BaseRequest::$STATUS_RESPONSE_CODE_UNPROCESSABLE_ENTITY));
    }
}
