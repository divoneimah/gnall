<?php

namespace App\Base;

use App\Http\Controllers\Controller;

abstract class BaseController extends Controller
{
    protected $per_page     = 10;    // Default per page pagination
    protected $start_page   = 0;    // Default per page pagination
    public $repository      = null;
    protected $page_title   = "Page Title";
    public function __construct()
    { }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponseDataTable($result, $draw=1, $recordsTotal=0, $recordsFiltered=0, $additional = [])
    {
        $response = [
            'draw'          => $draw,
            'data'          => $result,
            "recordsTotal"  => $recordsTotal,
            "recordsFiltered"=> $recordsFiltered,
            "additional"    => $additional
        ];
        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
