<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        /**
         * Below we try to find if a ModelNotFoundException||NotFoundHttpException exception occurred, then we throw an
         * error message in json.
         */
        if (($exception instanceof ModelNotFoundException || $exception instanceof NotFoundHttpException) && $request->wantsJson()) {
            $ids    = null;
            $route  = null;
            if($exception instanceof ModelNotFoundException){
                $ids    = json_encode($exception->getIds());
                $error  = "Entity not found {$ids}";
            }else{
                $error  = "Entity or route not found";
            }
            
            
            return response()->json($response = [
                'success' => false,
                'message' => $error,
            ], 404);
        }
        if (($exception instanceof AuthenticationException)) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'message' => 'Unauthenticated',
                    ]
                ], 401
            );
        }
        return parent::render($request, $exception);

    }
}
